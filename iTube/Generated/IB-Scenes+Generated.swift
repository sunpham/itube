// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum AppInfo: StoryboardType {
    internal static let storyboardName = "AppInfo"

    internal static let appInfoViewController = SceneType<AppInfoViewController>(storyboard: AppInfo.self, identifier: "AppInfoViewController")
  }
  internal enum BannerTemplate: StoryboardType {
    internal static let storyboardName = "BannerTemplate"

    internal static let bannerTemplateViewController = SceneType<BannerTemplateViewController>(storyboard: BannerTemplate.self, identifier: "BannerTemplateViewController")
  }
  internal enum CarouselTemplate: StoryboardType {
    internal static let storyboardName = "CarouselTemplate"

    internal static let carouselTemplateViewController = SceneType<CarouselTemplateViewController>(storyboard: CarouselTemplate.self, identifier: "CarouselTemplateViewController")
  }
  internal enum ChannelInfoPage: StoryboardType {
    internal static let storyboardName = "ChannelInfoPage"

    internal static let channelInfoPageViewController = SceneType<ChannelInfoPageViewController>(storyboard: ChannelInfoPage.self, identifier: "ChannelInfoPageViewController")
  }
  internal enum ChannelVideoTapStrip: StoryboardType {
    internal static let storyboardName = "ChannelVideoTapStrip"

    internal static let channelVideoTapStripViewController = SceneType<ChannelVideoTapStripViewController>(storyboard: ChannelVideoTapStrip.self, identifier: "ChannelVideoTapStripViewController")
  }
  internal enum CircleCarouselTemplate: StoryboardType {
    internal static let storyboardName = "CircleCarouselTemplate"

    internal static let circleCarouselTemplateViewController = SceneType<CircleCarouselTemplateViewController>(storyboard: CircleCarouselTemplate.self, identifier: "CircleCarouselTemplateViewController")
  }
  internal enum Collection: StoryboardType {
    internal static let storyboardName = "Collection"

    internal static let collectionViewController = SceneType<CollectionViewController>(storyboard: Collection.self, identifier: "CollectionViewController")
  }
  internal enum CollectionPage: StoryboardType {
    internal static let storyboardName = "CollectionPage"

    internal static let collectionPageViewController = SceneType<CollectionPageViewController>(storyboard: CollectionPage.self, identifier: "CollectionPageViewController")
  }
  internal enum FavouriteList: StoryboardType {
    internal static let storyboardName = "FavouriteList"

    internal static let favouriteListViewController = SceneType<FavouriteListViewController>(storyboard: FavouriteList.self, identifier: "FavouriteListViewController")
  }
  internal enum GridViewTemplate: StoryboardType {
    internal static let storyboardName = "GridViewTemplate"

    internal static let gridViewTemplateViewController = SceneType<GridViewTemplateViewController>(storyboard: GridViewTemplate.self, identifier: "GridViewTemplateViewController")
  }
  internal enum Home: StoryboardType {
    internal static let storyboardName = "Home"

    internal static let homeViewController = SceneType<HomeViewController>(storyboard: Home.self, identifier: "HomeViewController")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Main: StoryboardType {
    internal static let storyboardName = "Main"

    internal static let initialScene = InitialSceneType<MainViewController>(storyboard: Main.self)

    internal static let mainViewController = SceneType<MainViewController>(storyboard: Main.self, identifier: "MainViewController")
  }
  internal enum Search: StoryboardType {
    internal static let storyboardName = "Search"

    internal static let searchViewController = SceneType<SearchViewController>(storyboard: Search.self, identifier: "SearchViewController")
  }
  internal enum Settings: StoryboardType {
    internal static let storyboardName = "Settings"

    internal static let settingsViewController = SceneType<SettingsViewController>(storyboard: Settings.self, identifier: "SettingsViewController")
  }
  internal enum SuggestQueries: StoryboardType {
    internal static let storyboardName = "SuggestQueries"

    internal static let suggestQueriesViewController = SceneType<SuggestQueriesViewController>(storyboard: SuggestQueries.self, identifier: "SuggestQueriesViewController")
  }
  internal enum TubeList: StoryboardType {
    internal static let storyboardName = "TubeList"

    internal static let tubeListViewController = SceneType<TubeListViewController>(storyboard: TubeList.self, identifier: "TubeListViewController")
  }
  internal enum VideoPlayer: StoryboardType {
    internal static let storyboardName = "VideoPlayer"

    internal static let videoPlayerViewController = SceneType<VideoPlayerViewController>(storyboard: VideoPlayer.self, identifier: "VideoPlayerViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
