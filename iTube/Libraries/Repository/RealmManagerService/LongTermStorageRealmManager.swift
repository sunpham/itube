//
//  LongTermStorageRealmManager.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/25/21.
//

import Foundation
import RealmSwift
import RxCocoa
import RxSwift
import RxRealm

class LongTermStorageRealmManager: RealmManagerType {
    
    public static var `default`: RealmManagerType = {
        guard let defaultURL = Realm.Configuration.defaultConfiguration.fileURL else {
            fatalError("♨️♨️♨️ getting default realm configuration failed ♨️♨️♨️")
        }
        
        let bundleIdentifier = Bundle(for: BundleToken.self).bundleIdentifier.or("com.bigsunpham.itube")
        
        let objectTypes: [Object.Type] = [
            CodableYotubeVideo.self,
            CodableSnippet.self,
            CodableThumbnails.self,
            CodableThumbnail.self,
            CodableSnippetResourceId.self,
            CodableStatistics.self,
            CodableContentDetails.self,
            CodableContentRating.self,
            CodableRegoinRestriction.self,
            CodableYoutubeVideoSearchItem.self,
            CodableID.self,
            CodableHistoryItem.self,
            CodableFollowingItem.self,
            CodableYoutubeChannel.self
        ]
        
        let fileURL = defaultURL.deletingLastPathComponent().appendingPathComponent("\(bundleIdentifier).longterm.storage.realm")
        
        debugPrint("🔦🔦🔦 RealDatabase File URL: \(fileURL.absoluteURL) 🔦🔦🔦")
        let configuration = Realm.Configuration(fileURL: fileURL,
                                                encryptionKey: nil,
                                                schemaVersion: 1,
                                                objectTypes: objectTypes)
        
        return LongTermStorageRealmManager(configuration: configuration)
    }()
    
    public var realm: Realm?
    public var background: RealmThread?
    private let configuration: Realm.Configuration
    private let disposeBag = DisposeBag()
    private let backgroundQueue = DispatchQueue(label: "tube.background.queue", qos: .background)
    
    private lazy var notificationQueue: DispatchQueue = {
        let queueLabel = "realm.notification.queue." + String(ObjectIdentifier(self).hashValue)
        let queue = DispatchQueue(label: queueLabel, qos: .default)
        return queue
    }()
    
    public init(configuration: Realm.Configuration) {
        
        self.configuration = configuration
        background = RealmThread(start: true, queue: nil)
        background?.enqueue { [weak self] in
            guard let self = self else { return }
            self.realm = self.createRealm(configuration: configuration)
        }
    }
    
    private func createRealm(configuration: Realm.Configuration) -> Realm {
        do {
            return try Realm(configuration: configuration)
        } catch {
            fatalError("💔💔💔 Could not configuration Realm Swift with ERROR : \(error) 💔💔💔")
        }
    }
    
    private lazy var concurrentQueue = DispatchQueue.global(qos: .default)
    
    /**
     - Parameter type: The `type` of `Objects` which will get from `database`
     */
    func getAll<T>(_ type: T.Type) -> Results<T> where T : Object {
        let realm = createRealm(configuration: configuration)
        return realm.objects(type.self)
    }
    
    func get<T>(_ type: T.Type, filter predicate: NSPredicate? = nil) -> Results<T> where T : Object {
        
        let realm = createRealm(configuration: configuration)
        guard let predicate = predicate else {
            return realm.objects(type.self)
        }
        return realm.objects(type.self).filter(predicate)
    }
    
    
    func incrementID<T>(type: T.Type) -> Int where T: Object {
        
        guard let realm = self.realm else {
            return  0
        }
        let currentId: Int = ((realm.objects(type).max(ofProperty: "id") as Int?) ?? 0)
        return currentId + 1
    }
    
    func create<T>(object: T, update: Bool, writeBlock: @escaping ((T) -> Void)) -> Single<Void> where T : Object {
        return Single.create { [weak self] single -> Disposable in
            self?.backgroundQueue.async { [weak self] in
                guard let self = self else { return }
                let realm = self.createRealm(configuration: self.configuration)
                do {
                    try realm.write {
                        
                        writeBlock(object)
                        realm.add(object, update: update ? .all : .error)
                    }
                    single(.success(()))
                } catch {
                    debugPrint("Could not create object type: \(object.self) - ERROR: \(error)")
                    single(.failure(error))
                    
                }
            }
            return Disposables.create()
        }
    }
    
    func create<T>(_ type: T.Type, value: AnyObject, update: Bool) where T : Object {
    }
    
    func delete<T>(_ object: T) where T : Object {
        self.backgroundQueue.async { [weak self] in
            guard let self = self else { return }
            let realm = self.createRealm(configuration: self.configuration)
            do {
                try realm.write {
                    realm.delete(object)
                }
            } catch {
                debugPrint("Could not create object type: \(object.self) - ERROR: \(error)")
            }
        }
    }
    
    func delete<T>(type: T.Type, id: String) where T : Object {
        let realm = createRealm(configuration: configuration)
        do {
            try realm.write {
                realm.delete(realm.objects(type).filter(NSPredicate(format: "id == %@", id)))
            }
        } catch {
            debugPrint("Could not create object type: \(type) - ERROR: \(error)")
        }
    }
    
    func delete<T>(_ type: T.Type) where T : Object {
    }
    
    func clearData() {
    
        backgroundQueue.async { [weak self] in
            
            let block: (Realm) -> Void = { realm in
                realm.deleteAll()
            }
            guard let self = self else { return }
            let realm = self.createRealm(configuration: self.configuration)
            do {
                if realm.isInWriteTransaction {
                    block(realm)
                } else {
                    try realm.write { block(realm) }
                }
            } catch {
                debugPrint("Could not create object type: - ERROR: \(error)")
                
            }
        }
    }
    
    func update<T: Object>(_ type: T.Type, filter predicate: NSPredicate, updateBlock : @escaping ((T)  -> Void)) {
        if let object = getAll(type).filter(predicate).first {
            let savingObject = object.detached()
            updateBlock(savingObject)
            create(object: savingObject, update: true) { _ in }
                .subscribe()
                .disposed(by: disposeBag)
        }
    }
    
    func observableObjects<T: Object>(_ type: T.Type, filter predicate: NSPredicate) -> Observable<Results<T>> {
        
        let observeQueue: DispatchQueue = concurrentQueue
        return Observable.of(realm).filterNil().flatMap { realm -> Observable<Results<T>> in
            return Observable.collection(from: realm.objects(type).filter(predicate), on: observeQueue).map { $0.freeze() }
        }
        .observe(on: MainScheduler.instance)
        .asObservable()
    }
}

private class BundleToken {}
