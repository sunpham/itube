//
//  TargetType+Extensions.swift
//  Beepee
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Foundation
import Moya

extension TargetType {
    var headers: [String: String]? {
        return nil
    }
    
    var urlParameters: [String: Any]? {
        return nil
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var validate: Bool {
        return false
    }
}
