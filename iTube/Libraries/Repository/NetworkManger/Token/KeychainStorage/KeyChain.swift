//
//  KeyChain.swift
//  Beepee
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Foundation
import KeychainAccess

protocol AccessTokenStoragable {
    var currentToken: JWTToken { get }
    func store(token: JWTToken)
    func clearToken()
}

extension Keychain {
    static let `default`: Keychain = {
        guard let bundleId = Bundle.main.bundleIdentifier else {
            fatalError("Could not find bundle indentifier ")
        }
        
        return Keychain(service: bundleId)
    }()
}

class KeychainAccessTokenStorage: AccessTokenStoragable {
    
    static let `default` = KeychainAccessTokenStorage(key: "beepee_access_token_key")
    
    private let keyChain: Keychain
    private let accessTokenKey: String
    private let logger: Loggable = ColorfulLoger.logger
    
    init(keyChain: Keychain = Keychain.default, key: String) {
        self.keyChain = keyChain
        self.accessTokenKey = key
    }
    
    func store(token: JWTToken) {
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(token)
            try keyChain.set(data, key: accessTokenKey)
        } catch {
            logger.error("Could not encoder type : \(JWTToken.self)", file: "KeychainAccessTokenStorage.swift", line: nil)
        }
    }
    
    func clearToken() {
        store(token: .empty)
    }
    
    var currentToken: JWTToken {
        guard let tokenData = try? keyChain.getData(accessTokenKey) else { return JWTToken.empty }
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(JWTToken.self, from: tokenData)
        } catch {
            logger.error("Could not decode data with error: \(error)", file: "KeychainAccessTokenStorage.swift", line: nil)
            return JWTToken.empty
        }
    }
}
