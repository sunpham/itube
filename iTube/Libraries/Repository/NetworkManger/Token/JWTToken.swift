//
//  JWTToken.swift
//  Beepee
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Foundation
import JWTDecode

struct JWTToken: Codable, Equatable {
    
    static let empty: JWTToken = JWTToken(type: "", accessToken: "", refreshToken: "")
    let type: String
    let accessToken: String
    let refreshToken: String
    
    var isAccessTokenValid: Bool { return accessToken.isNotEmpty }
    var isRefreshTokenValid: Bool { return refreshToken.isNotEmpty }
    
    private enum CodingKeys: String, CodingKey {
        case type
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
    }
    
    init(type: String, accessToken: String, refreshToken: String) {
        self.type = type
        self.accessToken = accessToken
        self.refreshToken = refreshToken
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        accessToken = try container.decode(String.self, forKey: .accessToken)
        refreshToken = try container.decode(String.self, forKey: .refreshToken)
    }
    
}

