//
//  NetworkManager.swift
//  Beepee
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Foundation
import Moya
import Alamofire
import RxSwift
import RxCocoa

protocol Networkable {
    func request(target: TargetType) -> Single<Response>
    func authenticateRequest(target: TargetType, refreshTokenOnFailure: Bool) -> Single<Response>
    func storage(tokenInString: String)
    func clearToken()
}

class APIProvider: MoyaProvider<MultiTarget> {
    static let `default` = APIProvider()
    
    public static var plugins: [PluginType] {
        
        let accessTokenProvider = AccessTokenProvider.default
        return [
            NetworkLoggerPlugin(configuration: .init(logOptions: .verbose)),
            AccessTokenPlugin(tokenClosure: { _ -> String in
                return accessTokenProvider.currentToken.accessToken
            })
        ]
    }
    
    init(endpointClosure: @escaping EndpointClosure = MoyaProvider<MultiTarget>.defaultEndpointMapping,
         requestClosure: @escaping RequestClosure = MoyaProvider<MultiTarget>.defaultRequestMapping,
         stubClosure: @escaping StubClosure = MoyaProvider.neverStub,
         callbackQueue: DispatchQueue? = nil,
         plugins: [PluginType] = APIProvider.plugins,
         trackInflights: Bool = false,
         allowCache: Bool = true) {
        
        super.init(
            endpointClosure: endpointClosure,
            requestClosure: requestClosure,
            stubClosure: stubClosure,
            callbackQueue: callbackQueue,
            session: MoyaProvider<MultiTarget>.defaultAlamofireSession(),
            plugins: plugins,
            trackInflights: trackInflights
        )
    }
}

class NetworkManager: Networkable {
    
    static let `default` = NetworkManager(provider: APIProvider.default, tokenProvider: AccessTokenProvider.default)
    
    private let provider: MoyaProvider<MultiTarget>
    private let tokenProvider: AccesTokenProviderType
    
    private let logger: Loggable = ColorfulLoger.logger
    
    init(provider: MoyaProvider<MultiTarget>, tokenProvider: AccesTokenProviderType = AccessTokenProvider.default) {
        self.provider = provider
        self.tokenProvider = tokenProvider
    }
    
    func request(target: TargetType) -> Single<Response> {
        return provider.rx.request(MultiTarget(target)).filterSuccessfulStatusCodes()
    }
    
    func authenticateRequest(target: TargetType, refreshTokenOnFailure: Bool) -> Single<Response> {
        fatalError("Not implementation method")
    }
    
    func storage(tokenInString: String) {
        tokenProvider.store(token: JWTToken(type: "Bearer", accessToken: tokenInString, refreshToken: tokenInString))
    }
    
    func clearToken() {
        tokenProvider.clearToken()
    }
}

