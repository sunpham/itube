//
//  ITubeError.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/9/21.
//

import Foundation
import Moya

struct ITubeError {
    var code: Int = 0
    var message: String = ""
    var errors: [ITubeErrorInfo] = []
    var status: String = ""
    
    init(error: Error) {
        guard let error = error as? MoyaError, let errorData = error.response?.data else {
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let itubeErrorWrapper = try decoder.decode(CodableITubeErrorWrapper.self, from: errorData)
            guard let codableError = itubeErrorWrapper.error else { return }
            
            code = codableError.code
            message = codableError.message
            errors = codableError.errors.map { $0.map() }
            status = codableError.status
        } catch {
            debugPrint("Can not parse error data from moya: \(errno)")
        }
        
        if isHaveToRefreshAPIKey {
            UserDefaults.standard.goToNextAPIKey()
        }
    }
    
    var errorTypes: [ITubeErrorType] {
        return errors.map { ITubeErrorType(rawValue: $0.reason).or(.badRequest) }
    }
    
    var isHaveToRefreshAPIKey: Bool {
        return errorTypes.contains(.quotaExceeded)
    }
}

struct ITubeErrorInfo {
    let message: String
    let reason: String
    let domain: String
}

struct CodabeITubeError: Codable {
    
    let code: Int
    let message: String
    let errors: [CodableITubeErrorInfo]
    let status: String
    
    enum CodingKeys: String, CodingKey {
        case  code
        case  message
        case  errors
        case  status
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = (try? container.decode(Int.self, forKey: .code)).or(.zero)
        message = (try? container.decode(String.self, forKey: .message)).or("")
        errors = (try? container.decode([CodableITubeErrorInfo].self, forKey: .errors)).or([])
        status = (try? container.decode(String.self, forKey: .status)).or("")
    }
}

struct CodableITubeErrorInfo: Codable {
    let message: String
    let domain: String
    let reason: String
    
    enum CodingKeys: String, CodingKey {
        case message
        case domain
        case reason
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        message = (try? container.decode(String.self, forKey: .message)).or("")
        domain = (try? container.decode(String.self, forKey: .domain)).or("")
        reason = (try? container.decode(String.self, forKey: .reason)).or("")
    }
    
    func map() -> ITubeErrorInfo {
        return .init(message: message, reason: reason, domain: domain)
    }
}

struct CodableITubeErrorWrapper: Codable {
    let error: CodabeITubeError?
    
    enum CodingKeys: String, CodingKey {
        case  error
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        error = (try? container.decode(CodabeITubeError.self, forKey: .error)).or(nil)
    }
}

enum ITubeErrorType: String {
    case forbidden
    case badRequest
    case quotaExceeded
    case incompatibleParameters
    case invalidFilters
    case invalidPageToken
    case missingRequiredParameter
    case unexpectedParameter
    case accountDelegationForbidden
    case authenticatedUserAccountClosed
    case authenticatedUserAccountSuspended
    case authenticatedUserNotChannel
    case channelClosed
    case channelNotFound
    case channelSuspended
    case cmsUserAccountNotFound
    case insufficientCapabilities
    case insufficientPermissions
    case contentOwnerAccountNotFound
    case invalidRegionCode
    case invalidPart
    case unsupportedLanguageCode
    case unsupportedRegionCode
    case authorizationRequired
    case youtubeSignupRequired
}
