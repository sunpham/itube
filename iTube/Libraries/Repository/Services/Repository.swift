//
//  Repository.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/10/21.
//

import Foundation
import RxSwift
import RxOptional
import RealmSwift
import Resolver

protocol DataModel: LocalDataModel, RemoteDataModel {}

enum FetchPolicy {
    case cacheFirst(remote: RemoteRequest, local: LocalRequest)
}

protocol Repository {
    
    var remoteRepository: RemoteRepository { get }
    var localRepository: LocalRepository { get }
    
    func get<T: DataModel>(fetchPolicy: FetchPolicy) -> Observable<T>
    func get<T: DataModel>(fetchPolicy: FetchPolicy) -> Observable<[T]>
    func get<S: FetchStrategyType>(strategy: S) -> Observable<S.DataType>
    func get<S: FetchStrategyType>(strategy: S) -> Observable<[S.DataType]>
}

extension Repository {
    func get<T: DataModel>(fetchPolicy: FetchPolicy) -> Observable<T> {
        let fetchingStrategy: FetchStrategy<T> = makeFetchingStrategy(from: fetchPolicy)
        return get(strategy: fetchingStrategy)
        
    }
    
    public func get<S: FetchStrategyType>(strategy: S) -> Observable<S.DataType> {
        return strategy.fetch()
    }
    
    func get<T: DataModel>(fetchPolicy: FetchPolicy) -> Observable<[T]> {
        let fetchingStrategy: FetchStrategy<T> = makeFetchingStrategy(from: fetchPolicy)
        return get(strategy: fetchingStrategy)
    }
    
    func get<S: FetchStrategyType>(strategy: S) -> Observable<[S.DataType]> {
        return strategy.fetch()
    }
    
    private func makeFetchingStrategy<T: DataModel>(from fetchPolicy: FetchPolicy) -> FetchStrategy<T> {
        switch fetchPolicy {
        case .cacheFirst(let remote, let local):
            return FetchCacheFirstStrategy(localRepository: localRepository,
                                           localRequest: local,
                                           remoteRepository: remoteRepository,
                                           remoteRequest: remote)
        }
    }
}

public protocol IdentifiableObject {
    
    var id: String { get set }
}
