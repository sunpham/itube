//
//  FetchStrategy.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import RxSwift

protocol FetchStrategyType: AnyObject {
    associatedtype DataType
    func fetch() -> Observable<DataType>
    func fetch() -> Observable<[DataType]>
}
class FetchStrategy<DataType>: FetchStrategyType {
    
    init() {}
    
    func fetch() -> Observable<DataType> {
        fatalError("Subclasss must override this function")
    }
    
    func fetch() -> Observable<[DataType]> {
        fatalError("Subclass must override this function")
    }
}
