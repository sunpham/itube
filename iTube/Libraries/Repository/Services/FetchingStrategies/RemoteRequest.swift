//
//  RemoteRequest.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import Moya

public struct RemoteRequest {
    
    public let target: TargetType
    public let keyPath: String?
    
    public init(target: TargetType,
                keyPath: String? = nil) {
        self.target = target
        self.keyPath = keyPath
    }
}
