//
//  LocalRequest.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation

struct LocalRequest {
    
    public enum Query: Equatable {
        case objectId(String)
        case objectPredicate(NSPredicate, id: String = "")
        case listPredicate(NSPredicate)
        case list
        
        var objectId: String {
            switch self {
            case .objectId(let id):
                return id
            case .objectPredicate(_, let id):
                return id
            default:
                return ""
            }
        }
    }
    
    public enum UpdatePolicy {
        /// New data will be added or updated, keep existing data
        case merge
        /// Delete all existing cache data before adding new data
        case replace
    }
    
    let query: Query
    let updatePolicy: UpdatePolicy
    
    public init(_ query: Query, updatePolicy: UpdatePolicy = .merge) {
        self.query = query
        self.updatePolicy = updatePolicy
    }
    
    public static func objectId(_ id: String) -> LocalRequest {
        LocalRequest(.objectId(id))
    }
    
    public static func objectPredicate(_ predicate: NSPredicate) -> LocalRequest {
        LocalRequest(.objectPredicate(predicate))
    }
    
    public static func listPredicate(_ predicate: NSPredicate) -> LocalRequest {
        LocalRequest(.listPredicate(predicate))
    }
    
    public static var list: LocalRequest {
        LocalRequest(.list)
    }
}
