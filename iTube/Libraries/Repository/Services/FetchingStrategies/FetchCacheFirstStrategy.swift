//
//  FetchCacheFirstStrategy.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import RxSwift

// MARK: LocalRepository
class FetchCacheFirstStrategy<DataType: DataModel>: FetchStrategy<DataType> {
    private let localRepository: LocalRepository
    private let localRequest: LocalRequest
    private let remoteRepository: RemoteRepository
    private let remoteRequest: RemoteRequest
    
    init(localRepository: LocalRepository, localRequest: LocalRequest, remoteRepository: RemoteRepository, remoteRequest: RemoteRequest) {
        self.localRepository = localRepository
        self.localRequest = localRequest
        self.remoteRepository = remoteRepository
        self.remoteRequest = remoteRequest
    }
    
    override func fetch() -> Observable<DataType> {
        let localStream: Observable<DataType> = localRepository
            .get(localRequest)
            .filterNil()
            .catch { _ in return .never() }
        
        let localRequest = self.localRequest
        let remoteStream: Observable<DataType> = remoteRepository
            .request(remoteRequest).take(until: localStream.take(1))
            .doOnNext { [weak localRepository] data in
                localRepository?.save(data, request: localRequest)
            }
        
        return Observable.merge(localStream.take(1), remoteStream)
//        return localStream.take(1)
    }
    
    override func fetch() -> Observable<[DataType]> {
        let localStream: Observable<[DataType]> = localRepository
            .get(localRequest)
            .flatMap { (data: [DataType]) -> Observable<[DataType]> in
                if data.isEmpty {
                    return .never()
                }
                
                return .just(data)
            }
            .catch { _ in return .never() }
        
        let localRequest = self.localRequest
        let remoteStream: Observable<[DataType]> = remoteRepository
            .request(remoteRequest)
            .doOnNext { [weak localRepository] data in
                localRepository?.save(data, request: localRequest)
            }
        
        return Observable.merge(localStream.take(1), remoteStream)
    }
}
