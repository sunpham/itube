//
//  LocalRepository.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import RxSwift
import RealmSwift

protocol LocalRepository: AnyObject {
    
    func save<T: LocalDataModel>(_ data: T, request: LocalRequest)
    func observeSaving<T: LocalDataModel>(_ data: T, request: LocalRequest) -> Observable<Void>
    func get<T: LocalDataModel>(_ request: LocalRequest) -> Observable<T?>
    func getOnce<T: LocalDataModel>(_ request: LocalRequest) -> Observable<T>
    
    func save<T: LocalDataModel>(_ data: [T], request: LocalRequest)
    func observeSaving<T: LocalDataModel>(_ data: [T], request: LocalRequest) -> Observable<Void>
    func get<T: LocalDataModel>(_ request: LocalRequest) -> Observable<[T]>
    func getOnce<T: LocalDataModel>(_ request: LocalRequest) -> Observable<[T]>
    
    func delete<T: Object>(_ type: T.Type)
    func deleteObserving<T: Object>(_ type: T.Type) -> Observable<Void>
    func clearAll()
}
