//
//  RemoteRepository.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import RxSwift
import Moya

public protocol RemoteRepository: AnyObject {
    
    func request<T: RemoteDataModel>(_ remoteRequest: RemoteRequest) -> Observable<T>
    func request<T: RemoteDataModel>(_ remoteRequest: RemoteRequest) -> Observable<[T]>
    
    /// Convenience methods
    func request<T: RemoteDataModel>(_ target: TargetType, keyPath: String) -> Observable<T>
    func request<T: RemoteDataModel>(_ target: TargetType, keyPath: String) -> Observable<[T]>
}

extension RemoteRepository {
    
    public func request<T: RemoteDataModel>(_ target: TargetType, keyPath: String) -> Observable<T> {
        let remoteRequest = RemoteRequest(target: target, keyPath: keyPath)
        return self.request(remoteRequest)
    }
    
    public func request<T: RemoteDataModel>(_ target: TargetType, keyPath: String) -> Observable<[T]> {
        let remoteRequest = RemoteRequest(target: target, keyPath: keyPath)
        return self.request(remoteRequest)
    }
}
