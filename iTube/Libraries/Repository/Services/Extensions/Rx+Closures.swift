//
//  Rx+Closures.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import RxSwift


typealias Completion = () -> Void
typealias InputCompletion<T> = (T?) -> Void

extension Observable {
    
    static func closure<T>(_ closure: @escaping () -> T?) -> Observable<T> {
        return Observable<T>.create { observer in
            if let data = closure() {
                observer.onNext(data)
            }
            observer.onCompleted()
            return Disposables.create {}
        }
    }
}

extension Observable where Element == Void {
    
    static func closure(completion closure: @escaping (@escaping Completion) -> Void) -> Observable<Element> {
        return Observable<Element>.create { observer in
            closure {
                observer.onNext(())
                observer.onCompleted()
            }
            return Disposables.create {}
        }
    }
}

extension Observable {
    
    static func closure<Element>(completion closure: @escaping (@escaping InputCompletion<Element>) -> Void) -> Observable<Element> {
        return Observable<Element>.create { observer in
            closure { data in
                if let data = data {
                    observer.onNext(data)
                }
                observer.onCompleted()
            }
            return Disposables.create {}
        }
    }
}
