//
//  DefaultLocalRepository.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import RxSwift
import RealmSwift


public protocol LocalDataModel: RealmSwift.Object, IdentifiableObject {}

class DefaultLocalRepository: LocalRepository {
    private let realmManager: RealmManagerType
    private let disposeBag = DisposeBag()
    
    init(realmManager: RealmManagerType) {
        self.realmManager = realmManager
    }
}

extension DefaultLocalRepository {

    private func savePage<T: Pagable>(data: T, request: LocalRequest) {
        let objectId: String = request.query.objectId
        
        //if contains then append items and update info
        if let expectedObject = realmManager.getAll(T.self).first(where: { $0.id == objectId }) {
            let result = DeepCopier.Copy(of: expectedObject)
            result?.nextPageToken = data.nextPageToken
            for item in data.items {
                result?.items.append(item)
            }
            
            if let result = result {
                DispatchQueue.main.async {
                    self.realmManager.create(object: result, update: true, writeBlock: { _ in })
                        .subscribe()
                        .disposed(by: self.disposeBag)
                }
            }
        } else {
            DispatchQueue.main.async {
                self.realmManager.create(object: data, update: true, writeBlock: { updatedObject in
                    updatedObject.id = objectId
                })
                .subscribe()
                .disposed(by: self.disposeBag)
            }
        }
    }
    
    func save<T>(_ data: T, request: LocalRequest) where T : LocalDataModel {
        if T.self is CodableYoutubeVideoPagination.Type, let newData = data as? CodableYoutubeVideoPagination {
            savePage(data: newData, request: request)
            return
        }
        
        if T.self is CodableYoutubeVideoSearchItemPagination .Type,
           let newData = data as? CodableYoutubeVideoSearchItemPagination {
            savePage(data: newData, request: request)
            return
        }
        
        var savingObject = data
        switch request.query {
        case .objectId(let id) where id.isNotEmpty:
            savingObject.id = id
        default:
            break
        }
        
        DispatchQueue.main.async {
            self.realmManager.create(object: savingObject, update: true, writeBlock: { _ in })
                .subscribe()
                .disposed(by: self.disposeBag)
        }
    }
    
    func observeSaving<T>(_ data: T, request: LocalRequest) -> Observable<Void> where T : LocalDataModel {
        return Observable.empty()
    }
    
    func get<T:LocalDataModel>(_ request: LocalRequest) -> Observable<T?> {
        
        switch request.query {
        
        case let .objectId(id):
            let expectedItem = realmManager.getAll(T.self).toArray(type: T.self).first(where: { $0.id == id})
            return Observable.just(expectedItem)
            
        case let .objectPredicate(predicate, _):
            return Observable.just(realmManager.getAll(T.self).filter(predicate).first).observe(on: MainScheduler.instance)
            
        default:
            return .empty()
        }
    }
    
    func getOnce<T>(_ request: LocalRequest) -> Observable<T> where T : LocalDataModel {
        return Observable.empty()
    }
    
    func save<T>(_ data: [T], request: LocalRequest) where T : LocalDataModel {
        
    }
    
    func observeSaving<T>(_ data: [T], request: LocalRequest) -> Observable<Void> where T : LocalDataModel {
        return Observable.empty()
    }
    
    func get<T>(_ request: LocalRequest) -> Observable<[T]> where T : LocalDataModel {
        switch request.query {
        case .list:
            return Observable.just(realmManager.getAll(T.self).toArray(type: T.self)).observe(on: MainScheduler.instance)
            
        case let .listPredicate(predicate):
            return Observable.just(realmManager.get(T.self, filter: predicate).toArray(type: T.self)).observe(on: MainScheduler.instance)
        default:
            return .empty()
        }
    }
    
    func getOnce<T>(_ request: LocalRequest) -> Observable<[T]> where T : LocalDataModel {
        return Observable.empty()
    }
    
    func delete<T>(_ type: T.Type) where T : Object {
        
    }
    
    func deleteObserving<T>(_ type: T.Type) -> Observable<Void> where T : Object {
        return Observable.empty()
    }
    
    func clearAll() {
        realmManager.clearData()
    }
    
}

