//
//  DefaultRemoteRepository.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import RxSwift
import Resolver
import Moya

public protocol RemoteDataModel: Codable {}

public final class DefaultRemoteRepository: RemoteRepository {
    
    static let `default` = DefaultRemoteRepository()
    
    public static let instance: RemoteRepository = DefaultRemoteRepository()
    
    @LazyInjected var api: Networkable
    private let networkScheduler: SchedulerType
    
    public init() {
        self.networkScheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
    
    public func request<T: RemoteDataModel>(_ remoteRequest: RemoteRequest) -> Observable<T> {
        return api.request(target: remoteRequest.target)
            .observe(on: networkScheduler)
            .map(T.self, atKeyPath: remoteRequest.keyPath)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    public func request<T: RemoteDataModel>(_ remoteRequest: RemoteRequest) -> Observable<[T]> {
        return api.request(target: remoteRequest.target)
            .observe(on: networkScheduler)
            .map([T].self, atKeyPath: remoteRequest.keyPath)
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
}
