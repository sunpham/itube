//
//  BoolEnum.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/19/21.
//

import Foundation


public enum BoolEnum {

    /// all cases of Bool Enum
    /// `on` == `true`
    /// ` off` == `false`
    case on
    case off
    
    init(from value: Bool) {
        self = value ? .on : .off
    }

    /// isOn  for check `on` is current value
    public var isOn: Bool {
        return self == .on
    }

    /// isOff for check `off`
    public var isOff: Bool {
        return self == .off
    }

    /// transform from `current value` to `another`
    @discardableResult
    public mutating func `switch`() -> BoolEnum {
        switch self {
        case .on:
            self = .off
        case .off:
            self = .on
        }
        
        return self
    }

    /// get `current value`
    public var value: Bool {
        switch self {
        case .on:
            return true
        case .off:
            return false
        }
    }
}
