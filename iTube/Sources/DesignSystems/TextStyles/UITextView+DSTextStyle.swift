//
//  UITextView+DSTextStyle.swift
//  DesignSystem
//
//  Created by TAMPT12 on 4/14/20.
//

import UIKit

extension UITextView {
    
    public func setStyle(_ style: DSTextStyle) {
        font = UIFont.systemFont(ofSize: style.fontSize, weight: style.weight)
        textColor = style.color
    }
}
