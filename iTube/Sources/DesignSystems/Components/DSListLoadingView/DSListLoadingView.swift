//
//  DSListLoadingView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/28/21.
//

import UIKit
import Reusable

class DSListLoadingView: UIView, NibOwnerLoadable {
    @IBOutlet weak var skeletonContainer: UIStackView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        skeletonContainer.showAnimatedGradientSkeleton()
    }
}
