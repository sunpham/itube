//
//  GADITubeSmallNativeView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/26/21.
//

import UIKit
import GoogleMobileAds
import Reusable

class GADITubeSmallNativeView: GADNativeAdView, NibOwnerLoadable {
    
    @IBOutlet weak var _headLineLabel: UILabel!
    @IBOutlet weak var _mediaView: GADMediaView!
    @IBOutlet weak var _advertiserLabel: UILabel!
    @IBOutlet weak var _callToActionButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        _callToActionButton.layer.cornerRadius = 4.0
        _callToActionButton.layer.masksToBounds = true
        _callToActionButton.isUserInteractionEnabled = false
    }
    
    func setupView(_ nativeAd: GADNativeAd) {
        _headLineLabel.text = nativeAd.headline
        
        _mediaView.mediaContent = nativeAd.mediaContent
        
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            mediaContent.videoController.delegate = self
        }
        
        _advertiserLabel.text = nativeAd.advertiser
        _callToActionButton.setTitle(nativeAd.callToAction, for: .normal)
        
        self.headlineView = _headLineLabel
        self.mediaView = _mediaView
        self.advertiserView = _advertiserLabel
        self.callToActionView = _callToActionButton
        
        self.nativeAd = nativeAd
    }
}

extension GADITubeSmallNativeView: GADVideoControllerDelegate {
    
}


