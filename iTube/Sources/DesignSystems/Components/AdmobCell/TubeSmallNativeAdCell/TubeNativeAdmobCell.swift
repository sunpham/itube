//
//  TubeNativeAdmobCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/26/21.
//

import UIKit
import Reusable
import GoogleMobileAds

class TubeNativeAdmobCell: UITableViewCell, NibReusable, GADNativeAdDelegate {
    
    var parrentViewController: UIViewController!
    
    var isLoaded: Bool = false
    private lazy var adLoader = GADAdLoader(adUnitID: Natrium.admobConfiguration.nativeAdvancedAdId,
                                                 rootViewController: parrentViewController,
                                                 adTypes: [ .native ],
                                                 options: [])
    private let gadRequest = GADRequest()

    @IBOutlet weak var smallNativeAdView: GADITubeSmallNativeView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        adLoader.delegate = self
    }
    
    func configureCell(parrentViewController: UIViewController) {
        if isLoaded { return }
        
        self.parrentViewController = parrentViewController
        isLoaded = true
        adLoader.load(gadRequest)
    }
}

extension TubeNativeAdmobCell: GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAd.delegate = self
        smallNativeAdView.setupView(nativeAd)
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
    }
}
