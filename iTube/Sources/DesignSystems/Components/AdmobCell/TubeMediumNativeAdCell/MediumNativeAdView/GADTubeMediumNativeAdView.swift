//
//  GADTubeMediumNativeAdView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/26/21.
//

import UIKit
import Reusable
import GoogleMobileAds


class GADTubeMediumNativeAdView: GADNativeAdView, NibOwnerLoadable {

    @IBOutlet weak var _starRatingView: UIImageView!
    @IBOutlet weak var _iconView: UIImageView!
    @IBOutlet weak var _mediaView: GADMediaView!
    @IBOutlet weak var _advertiserLabel: UILabel!
    @IBOutlet weak var _bodyLabel: UILabel!
    @IBOutlet weak var _callToActionButton: UIButton!
    @IBOutlet weak var _priceLabel: UILabel!
    @IBOutlet weak var _storeLabel: UILabel!
    @IBOutlet weak var _headlineLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
    }
    
    func setupView(_ nativeAd: GADNativeAd) {
        _headlineLabel.text = nativeAd.headline
        _mediaView.mediaContent = nativeAd.mediaContent
        
        let mediaContent = nativeAd.mediaContent
        if mediaContent.hasVideoContent {
            mediaContent.videoController.delegate = self
        }
        
        _bodyLabel.text = nativeAd.body
        _callToActionButton.isUserInteractionEnabled = false
        _callToActionButton.setTitle(nativeAd.callToAction, for: .normal)
        _iconView.image = nativeAd.icon?.image
        
        _storeLabel.text = nativeAd.store
        
        _priceLabel.text = nativeAd.price
        
        _advertiserLabel.text = nativeAd.advertiser
        
        self.headlineView = _headlineLabel
        self.mediaView = _mediaView
        self.bodyView = _bodyLabel
        self.iconView = _iconView
        self.callToActionView = _callToActionButton
        self.priceView = _priceLabel
        self.storeView = _storeLabel
        self.advertiserView = _advertiserLabel
        self.nativeAd = nativeAd
    }
}

extension GADTubeMediumNativeAdView: GADVideoControllerDelegate {
    
}

