//
//  TubeMediumNativeAdCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/26/21.
//

import UIKit
import Reusable
import GoogleMobileAds

class TubeMediumNativeAdCell: UITableViewCell, NibReusable {

    var parrentViewController: UIViewController!
    
    var isLoaded: Bool = false
    private lazy var adLoader = GADAdLoader(adUnitID: Natrium.admobConfiguration.nativeAdvancedAdId,
                                                 rootViewController: parrentViewController,
                                                 adTypes: [ .native ],
                                                 options: [])
    private let gadRequest = GADRequest()
    @IBOutlet weak var nativeAdView: GADTubeMediumNativeAdView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        adLoader.delegate = self
    }
    
    
    func configureCell(parrentViewController: UIViewController) {
        if isLoaded { return }
        
        self.parrentViewController = parrentViewController
        isLoaded = true
        adLoader.load(gadRequest)
    }
}

extension TubeMediumNativeAdCell: GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        nativeAd.delegate = self
        nativeAdView.setupView(nativeAd)
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
    }
}

extension TubeMediumNativeAdCell: GADNativeAdDelegate {
    func nativeAdDidRecordImpression(_ nativeAd: GADNativeAd) {
        
    }
    
    func nativeAdDidRecordClick(_ nativeAd: GADNativeAd) {
        
    }
    
    func nativeAdIsMuted(_ nativeAd: GADNativeAd) {
        
    }
    
    func nativeAdDidDismissScreen(_ nativeAd: GADNativeAd) {
        
    }
    
    func nativeAdWillDismissScreen(_ nativeAd: GADNativeAd) {
        
    }
    
    func nativeAdWillPresentScreen(_ nativeAd: GADNativeAd) {
        
    }
}
