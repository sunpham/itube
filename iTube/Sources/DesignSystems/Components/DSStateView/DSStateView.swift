//
//  DSStateView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/24/21.
//

import UIKit
import Reusable

extension UIImage {
    func compressImage(maxSize: CGSize, compressionQuality: CGFloat) -> Data? {
        var actualHeight = Double(self.size.height)
        var actualWidth = Double(self.size.width)
        let maxHeight = Double(maxSize.height)
        let maxWidth = Double(maxSize.width)
        var imgRatio: Double = actualWidth / actualHeight
        let maxRatio: Double = maxWidth / maxHeight
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                // adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if imgRatio > maxRatio {
                // adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0, y: 0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: compressionQuality)
        UIGraphicsEndImageContext()
        
        return imageData
    }
    
    final var template: UIImage {
        return self.withRenderingMode(.alwaysTemplate)
    }
    
    final var original: UIImage {
        self.withRenderingMode(.alwaysOriginal)
    }
    
    func color(_ color: UIColor) -> UIImage {
        let templateImage = self.template
        let imageRenderer = UIGraphicsImageRenderer(size: templateImage.size, format: template.imageRendererFormat)
        let tintedImage = imageRenderer.image { _ in
            color.set()
            templateImage.draw(at: .zero)
        }
        return tintedImage.original
    }
}

class DSStateView: UIView, NibOwnerLoadable {
    
    var style: Style = .emptyData {
        didSet {
            updateViews()
        }
    }
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var actionButtonContainerView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        style = .emptyData
        
        titleLabel.setStyle(DS.T16M(color: Colors.ink500))
        messageLabel.setStyle(DS.T14R(color: Colors.ink400s))
        
        style = .emptyData
    }
    
    private func updateViews() {
        
        imageView.image = style.image
        titleLabel.text = style.title
        messageLabel.text = style.message
    }
}

extension DSStateView {
    enum Style {
        case emptyData
        case networkError
        case notFound
        case commingSoon
        
        var image: UIImage? {
            return  Asset.Image.Common.imEmptyPlaceholder.image.color(Colors.ink400s)
        }
        
        var title: String {
            switch self {
            case .emptyData:
                return "Chưa có dữ liệu nào"
                
            case .networkError:
                return "Lỗi mạng rồi bạn hiền ơi"
                
            case .notFound:
                return "Không tìm thấy mục này"
                
            case .commingSoon:
                return "Đang hoàn thành"
            }
        }
        
        var message: String {
            
            switch self {
            case .commingSoon:
                return "Tính năng đang trong quá trình phát triển, vui lòng quay lại sau bạn nhé ❤️"
            default:
                return
                    """
Hiện bạn chưa có dữ liệu nào cho mục này 😔.
Tiếp tục trải nghiệp app và tìm thấy điều bất ngời bạn nhé ❤️❤️❤️.
"""
            }
        }
        
        var actionTitle: String {
            return ""
        }
    }
}
