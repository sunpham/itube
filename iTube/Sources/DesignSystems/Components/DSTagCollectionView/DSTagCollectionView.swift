//
//  DSTagCollectionView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//
//
//  DSCollectionView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa
import RxDataSources

class DSTagCollectionView: UIView, NibOwnerLoadable {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    public let contentSize = PublishRelay<CGSize>()
    
    private let viewModels = BehaviorRelay<[DSTagViewModel]>(value: [])
    private lazy var dataSource = makeDataSource()
    private let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
}

extension DSTagCollectionView {
    private func setupViews() {
        self.loadNibContent()
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        setupLayout()
        registerCells()
        setupDelegate()
        bindingCollectionViewData()
    }
    
    private func setupDelegate() {
        collectionView.rx.setDelegate(self).disposed(by: disposeBag)
    }

    private func setupLayout() {
        let layout = DSTagCollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
    
        collectionView.collectionViewLayout = layout
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.isScrollEnabled = false
    
        layout
            .contentSizeObservable
            .bind(to: contentSize)
            .disposed(by: disposeBag)
    }
    
    private func registerCells() {
        collectionView.register(cellType: DSTagCell.self)
    }
    
    private func bindingCollectionViewData() {
        
        viewModels
            .map {
                [Section(model: .zero, items: $0)]
            }
            .asDriver(onErrorDriveWith: .never())
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}

extension DSTagCollectionView {
    private typealias Section = AnimatableSectionModel<Int, DSTagViewModel>
    private typealias DataSource = RxCollectionViewSectionedAnimatedDataSource<Section>
}

extension DSTagCollectionView {
    private func makeDataSource() -> DataSource {
        return .init(
            animationConfiguration: AnimationConfiguration(insertAnimation: .fade,
                                                           reloadAnimation: .fade,
                                                           deleteAnimation: .fade),
            configureCell:  { (_, collectionView, indexPath, viewModel) -> UICollectionViewCell in
                let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: DSTagCell.self)
                cell.configureCell(viewModel: viewModel)
                return cell
            }
        )
    }
}

extension DSTagCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard indexPath.count < viewModels.value.count else { return .zero }
        
        let cellTitleFont = collectionView.dequeueReusableCell(for: indexPath, cellType: DSTagCell.self).titleFont
        let cellViewModel = viewModels.value[indexPath.row]
        
        let itemWidth = ceil(cellViewModel.title.boundingWidth(withConstrainedHeight: 16, font: cellTitleFont, additionalOptions: [])) + 32 + 8
        return CGSize(width: itemWidth, height: 32.0)
    }
}

extension DSTagCollectionView {
    
    @discardableResult
    public func setTagItems(_ items: [DSTagItemType]) -> DSTagCollectionView {
        viewModels.accept(items.map { DSTagViewModel(model: $0) })
        return self
    }
}

extension String {

    public func boundingHeight(withConstrainedWidth width: CGFloat,
                               font: UIFont,
                               additionalOptions: NSStringDrawingOptions = []) -> CGFloat {

        let size = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        var options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
        options = options.union(additionalOptions)
        let boundingBox = self.boundingRect(
            with: size,
            options: options,
            attributes: [.font: font],
            context: nil
        )
        return boundingBox.height
    }

    public func boundingWidth(withConstrainedHeight height: CGFloat,
                              font: UIFont,
                              additionalOptions: NSStringDrawingOptions = []) -> CGFloat {

        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        var options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
        options = options.union(additionalOptions)
        let boundingBox = self.boundingRect(
            with: size,
            options: options,
            attributes: [.font: font],
            context: nil
        )
        return boundingBox.width
    }

    public func boundingHeight(withFont font: UIFont) -> CGFloat {
        return boundingHeight(withConstrainedWidth: CGFloat.greatestFiniteMagnitude, font: font)
    }

    public func boundingWidth(withFont font: UIFont) -> CGFloat {
        return boundingWidth(withConstrainedHeight: CGFloat.greatestFiniteMagnitude, font: font)
    }
}

extension Reactive where Base: DSTagCollectionView {
    var contentSize: Observable<CGSize?> {
        base.collectionView.rx.observe(CGSize.self, #keyPath(UIScrollView.contentSize))
            .map { contentSize in
                guard let size = contentSize else { return contentSize }
                return CGSize(width: size.width + 16,
                              height: size.height + 16)
            }
    }

}
