//
//  DSTagCollectionViewFlowLayout.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//

import UIKit
import RxSwift
import RxCocoa

final class DSTagCollectionViewFlowLayout: UICollectionViewFlowLayout {

    let contentSizeObservable = PublishRelay<CGSize>()
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributesForElements = super.layoutAttributesForElements(in: rect)
        
        if scrollDirection == .horizontal {
            return attributesForElements
        } else {
            return super.layoutAttributesForElements(in: rect)?.map { layoutAttributesForItem(at: $0.indexPath) ?? UICollectionViewLayoutAttributes() }
        }
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributesForItem = super.layoutAttributesForItem(at: indexPath)
        guard scrollDirection == .vertical else { return attributesForItem }

        guard let currentItemAttributes = attributesForItem,
            let collectionView = self.collectionView else {
            return attributesForItem
        }

        let sectionInset = evaluatedSectionInsetForSection(at: indexPath.section)
        guard indexPath.item != 0 else {
            currentItemAttributes.frame.origin.x = sectionInset.left
            return currentItemAttributes
        }

        guard let previousFrame = layoutAttributesForItem(at: IndexPath(item: indexPath.item - 1, section: indexPath.section))?.frame else {
            return attributesForItem
        }

        let rect = CGRect(x: sectionInset.left,
                          y: currentItemAttributes.frame.origin.y,
                          width: collectionView.frame.width - sectionInset.left - sectionInset.right,
                          height: currentItemAttributes.frame.size.height)
        guard previousFrame.intersects(rect) else {
            currentItemAttributes.frame.origin.x = sectionInset.left
            return currentItemAttributes
        }

        currentItemAttributes.frame.origin.x = previousFrame.origin.x + previousFrame.size.width + evaluatedMinimumInteritemSpacingForSection(at: indexPath.section)
        return currentItemAttributes
    }

    private func evaluatedMinimumInteritemSpacingForSection(at section: NSInteger) -> CGFloat {
        guard let collectionView = collectionView,
            let delegate = collectionView.delegate as? UICollectionViewDelegateFlowLayout,
            let interitemSpacing = delegate.collectionView?(collectionView, layout: self, minimumInteritemSpacingForSectionAt: section) else {
            return minimumInteritemSpacing
        }
        return interitemSpacing
    }

    private func evaluatedSectionInsetForSection(at section: NSInteger) -> UIEdgeInsets {
        guard let collectionView = collectionView,
            let delegate = collectionView.delegate as? UICollectionViewDelegateFlowLayout,
            let insetForSection = delegate.collectionView?(collectionView, layout: self, insetForSectionAt: section) else {
            return sectionInset
        }
        return insetForSection
    }
    
    
    override var collectionViewContentSize: CGSize {
        let contentSize = super.collectionViewContentSize
        contentSizeObservable.accept(contentSize)

        return contentSize
    }
}
