//
//  DSTagViewModel.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//

import Foundation
import Differentiator

struct DSTagViewModel {
    
    let model: DSTagItemType
    
    var id: String {
        return model.id
    }
    
    var title: String {
        return model.title
    }
    
    var isSelected: Bool {
        return model.isSelected
    }
    
    var isEnabled: Bool {
        return model.isEnabled
    }
}

extension DSTagViewModel: Equatable {
    static func == (lhs: DSTagViewModel, rhs: DSTagViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}

extension DSTagViewModel: IdentifiableType {
    var identity: String {
        return id
    }
}
