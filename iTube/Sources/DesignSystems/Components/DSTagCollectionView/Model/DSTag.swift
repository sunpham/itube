//
//  DSTag.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//

import Foundation

protocol DSTagItemType {
    var id: String { get }
    var title: String { get }
    var isSelected: Bool { get }
    var isEnabled: Bool { get }
}

struct DSTagItem: DSTagItemType, Equatable {
    let id: String
    let title: String
    let isSelected: Bool
    let isEnabled: Bool
    
    init(id: String, title: String = "", isSelected: Bool = false, isEnabled: Bool = false) {
        self.id = id
        self.title = title
        self.isSelected = isSelected
        self.isEnabled = isEnabled
    }
}
