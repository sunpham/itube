//
//  DSTagCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//

import UIKit
import Reusable

class DSTagCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet private weak var removeTagWrapView: UIView!
    @IBOutlet weak var removeTagImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        titleLabel.setStyle(DS.T14R(color: Colors.ink500))
        setupContainerView()
        
        removeTagWrapView.backgroundColor = Colors.ink300s
        removeTagImageView.image = Asset.Icon.Common.icIosCloseAudio.image.template.color(Colors.white500)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        removeTagWrapView.layer.masksToBounds = true
        removeTagWrapView.layer.cornerRadius = removeTagWrapView.bounds.height / 2
    }
    
    private func setupContainerView() {
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 8.0
        containerView.layer.borderWidth = 0.2
        containerView.layer.borderColor = Colors.ink400.cgColor
        containerView.backgroundColor = Colors.backgroundDark
    }
    
    func configureCell(viewModel: DSTagViewModel) {
        titleLabel.text = viewModel.title
        layoutIfNeeded()
    }
    
    var titleFont: UIFont {
        return titleLabel.font
    }
}
