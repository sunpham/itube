//
//  DSInfoTextField.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/13/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class DSInfoTextField: UIView, NibOwnerLoadable {
    
    enum Style {
        case normal
        case error
        
        var borderColor: UIColor {
            switch self {
            case .error:
                return Colors.red500
                
            case .normal:
                return Colors.ink400
                
            }
        }
    }
    
    @IBOutlet private weak var mainContainerWrapView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet weak var infoTextField: UITextField!
    
    private var style: Style = .normal {
        didSet {
            containerView.layer.borderColor = style.borderColor.cgColor
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }

    private func setupViews() {
        self.loadNibContent()
        
        mainContainerWrapView.backgroundColor = UIColor.clear
        mainContainerWrapView.setShadowStyle(.shadow8)
        
        containerView.layer.cornerRadius = 8.0
        containerView.layer.masksToBounds = true
        containerView.layer.borderWidth = 0.25
        
        infoTextField.placeholder = "Enter your text"
        infoTextField.borderStyle = .none
    }
}

extension DSInfoTextField {
    @discardableResult
    func setPlaceholder(_ value: String) -> DSInfoTextField {
        infoTextField.placeholder = value
        return self
    }
    
    @discardableResult
    func setNeedSecure(_ value: Bool) -> DSInfoTextField {
        infoTextField.isSecureTextEntry = value
        return self
    }
    
    @discardableResult
    func setTag(_ value: Int) -> DSInfoTextField {
        infoTextField.tag = value
        return self
    }
    
    @discardableResult
    func setText(_ value: String) -> DSInfoTextField {
        infoTextField.text = value
        return self
    }
    
    @discardableResult
    func setStyle(_ style: Style) -> DSInfoTextField {
        self.style = style
        return self
    }
}
