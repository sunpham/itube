//
//  DSChooseImageView.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/21/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class DSChooseImageView: UIView, NibOwnerLoadable {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var containerImageWrapView: UIView!
    @IBOutlet private weak var imageView: UIImageView!
    
    var presentViewController: UIViewController!
    var phothoPickerViewController: UIImagePickerController!
    
    var didChangeImage = PublishRelay<UIImage>()
    private let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        
        titleLabel.setStyle(DS.T16M(color: Colors.ink500))
        
        containerImageWrapView.layer.cornerRadius = 8
        containerImageWrapView.layer.masksToBounds = true
        containerImageWrapView.backgroundColor = Colors.ds2BackgroundDark
        
        phothoPickerViewController = UIImagePickerController()
        phothoPickerViewController.delegate = self
        phothoPickerViewController.allowsEditing = true
        phothoPickerViewController.sourceType = .photoLibrary
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapContainerImageView))
        containerImageWrapView.addGestureRecognizer(tapGesture)
    }
    
    @objc
    private func onTapContainerImageView() {
        presentViewController?.present(phothoPickerViewController, animated: true, completion: nil)
    }
}

extension DSChooseImageView {
    
    @discardableResult
    func setTitle(_ title: String) -> DSChooseImageView {
        titleLabel.text = title
        return self
    }
    
    @discardableResult
    func setPresentViewController(_ viewController: UIViewController) -> DSChooseImageView {
        self.presentViewController = viewController
        return self
    }
    
    @discardableResult
    func setImage(by assetId: String) -> DSChooseImageView {
        PhotoReader
            .load(savedAssetId: assetId)
            .asDriver(onErrorDriveWith: .never())
            .driveNext { [weak self] image in
                self?.imageView.image = image
            }
            .disposed(by: disposeBag)
        
        return self
    }
}

extension DSChooseImageView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        imageView.image = image
        
        if let image = image {
            didChangeImage.accept(image)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage
        else {
            return pickerController(picker, didSelect: nil)
        }
        
        self.pickerController(picker, didSelect: image)
    }
}

extension Reactive where Base: DSChooseImageView {
    var didChangeImage: ControlEvent<UIImage> {
        return ControlEvent(events: base.didChangeImage)
    }
}
