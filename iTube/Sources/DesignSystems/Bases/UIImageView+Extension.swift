//
//  UIImageView+Extension.swift
//  DesignSystem
//
//  Created by Tam Pham on 8/16/20.
//

//import Kingfisher
//import UIKit
//
//extension UIImageView {
//
//    func setImage(assetOrImageURL: URL) {
//        guard let imageName = assetOrImageURL.absoluteString.components(separatedBy: "/").last,
//            let image = UIImage(named: imageName) else {
//
//            KingfisherManager.shared.retrieveImage(with: assetOrImageURL) { [weak self] result in
//                switch result {
//                case let .success(receiveImage):
//                    self?.image = receiveImage.image
//                case .failure:
//                    break
//                }
//            }
//            return
//        }
//        self.image = image
//    }
//}
