//
//  GoogleAdmob+Rx.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/26/21.
//

import Foundation
import RxSwift
import GoogleMobileAds

class AdmobbBaseViewController: UIViewController {
    
    private var interstitialAd: GADInterstitialAd?
    enum AdmobType {
        case gadInterstitialAd
    }
    
    func loadAd(_ type: AdmobType) {
        switch type {
        case .gadInterstitialAd:
            loadGADInterstitialAd()
        }
    }
    
    private func loadGADInterstitialAd() {
        
        let request = GADRequest()
        GADInterstitialAd.load(
            withAdUnitID: Natrium.admobConfiguration.interstitialAdId,
            request: request
        ) { [weak self] (ad, error) in
            if let ad = ad {
                self?.interstitialAd = ad
            }
            
            if let error = error {
                debugPrint("Error when request ad id: \(Natrium.admobConfiguration.interstitialAdId) with error: \(error)")
            }
        }
    }
}

extension AdmobbBaseViewController: GADFullScreenContentDelegate {}

