//
//  MiniPlayerView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/13/21.
//

import UIKit
import Reusable
import AVKit
import RxSwift
import RxCocoa

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

class MiniPlayerView: UIView, NibOwnerLoadable {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet private weak var miniThumbnailView: UIView!
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var playImageView: UIImageView!
    @IBOutlet private weak var closeImageView: UIImageView!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var tapAreaContainerView: UIView!
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    private var isPlaying: Bool = false {
        didSet {
            if isPlaying {
                playImageView.image = Asset.Icon.Common.icIosPauseAudio.image
                return
            }
            
            playImageView.image = Asset.Icon.Common.icIosPlayAudio.image
        }
    }
    
    var onTapShowVideo: VoidCallBack?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
        
    }
    
    private func setupViews() {
        self.loadNibContent()
        
        
        if Keeper.shared.currentYoutubeVideo == nil {
            self.isHidden = true
        } else {
            self.isHidden = false
        }
        
        miniThumbnailView.layer.cornerRadius = 8.0
        miniThumbnailView.layer.masksToBounds = true
        
        containerView.setShadowStyle(.shadow4Revert)
        titleLabel.setStyle(DS.T12R(color: Colors.ink400s))
        closeImageView.image = Asset.Icon.Common.icIosCloseAudio.image
        
        closeImageView.setImageColor(color: Colors.red400)
        playImageView.setImageColor(color: Colors.red400)
        
        let showVideoTapGeture = UITapGestureRecognizer(target: self, action: #selector(didTapShowVideo))
        tapAreaContainerView.addGestureRecognizer(showVideoTapGeture)
        
        playButton
            .rx
            .tap
            .subscribeNext { _ in
                if (AVPlayerViewControllerManager.shared.player?.isPlaying).or(false) {
                    self.isPlaying = false
                    AVPlayerViewControllerManager.shared.player?.pause()
                } else {
                    self.isPlaying = true
                    AVPlayerViewControllerManager.shared.player?.play()
                }
            }
            .disposed(by: disposeBag)
        
        closeButton
            .rx
            .tap
            .subscribeNext { [weak self] in
                self?.isHidden = true
                Keeper.shared.currentYoutubeVideo = nil
                AVPlayerViewControllerManager.shared.player?.pause()
                AVPlayerViewControllerManager.shared.disconnectPlayer()
            }
            .disposed(by: disposeBag)
        
    }
    
    @objc func didTapShowVideo(_: UITapGestureRecognizer) {
        onTapShowVideo?()
    }
    
    func reload() {
        
        guard let player = AVPlayerViewControllerManager.shared.player else {
            self.isHidden = true
            return
        }
        
        self.isHidden = false
        isPlaying = player.isPlaying
        
        if let video = Keeper.shared.currentYoutubeVideo {
            self.isHidden = false
            thumbnailImageView.kf.setImage(with: URL(string: (video.snippet?.thumbnails?.getThumbnailUrlString()).or("")),
                                           placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
            titleLabel.text = video.snippet?.title
        } else {
            self.isHidden = true
        }
    }
    
}
