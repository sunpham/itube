//
//  Typealias+Const.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/4/21.
//

import Foundation

typealias VoidCallBack = () -> Void
