//
//  CarouselTemplateWireframe.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

enum CarouselTemplateStyle: String {
    case medium
    case large
    case largeSingle
    
    var mapToCellWidth: CGFloat {
        
        let cellPadding: CGFloat = 16
        let screenWidth = UIScreen.main.bounds.width
        switch self {
        case .medium:
            return (screenWidth * 0.95 - (cellPadding * 2) - 16) / 2
        case .largeSingle:
            return screenWidth - cellPadding * 2
            
        case .large:
            return screenWidth * 0.95 - cellPadding * 2
        }
    }
    
    var mapToCellHeight: CGFloat {
        return self.mapToCellWidth * 9 / 16 + 48
    }
}

final class CarouselTemplateWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "CarouselTemplate", bundle: nil)

    // MARK: - Module setup -

    init(style: CarouselTemplateStyle = .medium, extraData: [String: Any], service: TemplateServiceType, listener: CaroulseTemplateListener) {
        let moduleViewController = storyboard.instantiateViewController(ofType: CarouselTemplateViewController.self)
        moduleViewController.style = style
        super.init(viewController: moduleViewController)
        
        let categoryId: String = (extraData["category_id"] as? String).or("0")

        let interactor = CarouselTemplateInteractor(carouseService: service, categoryId: categoryId)
        let presenter = CarouselTemplatePresenter(view: moduleViewController,
                                                  interactor: interactor,
                                                  wireframe: self,
                                                  style: style,
                                                  extraData: extraData,
                                                  listener: listener)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension CarouselTemplateWireframe: CarouselTemplateWireframeInterface {
}
