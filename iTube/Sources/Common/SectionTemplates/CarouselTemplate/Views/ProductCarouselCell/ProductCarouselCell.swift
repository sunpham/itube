//
//  ProductCarouselCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/19/21.
//

import UIKit
import Reusable
import Kingfisher

struct TemplateItem {
    
    let model: YoutubeVideo
    
    var id: String {
        return model.id
    }
    
    var title: String {
        return (model.snippet?.title).or("")
    }
    
    var thumbnailUrlString: String {
        return (model.snippet?.thumbnails?.getThumbnailUrlString()).or("")
    }
    
    var moreInfo: String {
        return (model.snippet?.channelTitle).or("")
    }
}

struct TemplateCellViewModel {
    
    let model: TemplateItem
    
    var id: String {
        return model.id
    }
    
    var title: String {
        return model.title
    }
    
    var thumbnailUrlString: String {
        return model.thumbnailUrlString
    }
    
    var moreInfo: String {
        return model.moreInfo
    }
}

class ProductCarouselCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var productImageContainerView: UIView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var productNameLabel: UILabel!
    
    @IBOutlet private weak var moreInfoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }

    private func setupViews() {
        productImageContainerView.layer.cornerRadius = 8.0
        productImageContainerView.layer.masksToBounds = true
        
        productNameLabel.setStyle(DS.T12M(color: Colors.ink500))
        moreInfoLabel.setStyle(DS.T12R(color: Colors.ink400s))
    }
    
    func configureCell(viewModel: TemplateCellViewModel, style: CarouselTemplateStyle) {
        switch  style {
        case .medium:
            moreInfoLabel.isHidden = true
        case .large, .largeSingle:
            moreInfoLabel.isHidden = false
        }
        
        thumbnailImageView.kf.setImage(with: URL(string: viewModel.thumbnailUrlString), placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
        productNameLabel.text = viewModel.title
        moreInfoLabel.text = viewModel.moreInfo
    }
}
