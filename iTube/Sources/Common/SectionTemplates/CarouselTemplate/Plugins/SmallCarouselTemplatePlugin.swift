//
//  SmallCarouselTemplatePlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import Foundation

class MediumCarouselTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    let service: TemplateServiceType
    init(service: TemplateServiceType) {
        self.service = service
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return CarouselTemplateWireframe(style: .medium, extraData: extraData, service: service, listener: self)
    }
}

extension MediumCarouselTemplatePlugin: CaroulseTemplateListener {
    func carouselTemplateDidGetErorr(_ error: Error) {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidGetEmptyData() {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidSelected(_ model: YoutubeVideo) {
        listener?.selecteItem(selectedModel: model, self)
    }
    
    func carouselTemplateGotData() {
        
    }
    
    func carouselTemplateDidViewAll(categoryId: String) {
        listener?.didViewAll(self, categoryId: categoryId)
    }
}

class LargeCarouselTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    
    let service: TemplateServiceType
    init(service: TemplateServiceType) {
        self.service = service
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return CarouselTemplateWireframe(style: .large, extraData: extraData, service: service, listener: self)
    }
}


extension LargeCarouselTemplatePlugin: CaroulseTemplateListener {
    func carouselTemplateDidGetErorr(_ error: Error) {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidGetEmptyData() {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidSelected(_ model: YoutubeVideo) {
        listener?.selecteItem(selectedModel: model, self)
    }
    
    func carouselTemplateGotData() {
        
    }
    
    func carouselTemplateDidViewAll(categoryId: String) {
        listener?.didViewAll(self, categoryId: categoryId)
    }
}

class LargeSingleCarouselTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    
    let service: TemplateServiceType
    init(service: TemplateServiceType) {
        self.service = service
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return CarouselTemplateWireframe(style: .largeSingle, extraData: extraData, service: service, listener: self)
    }
}

extension LargeSingleCarouselTemplatePlugin: CaroulseTemplateListener {
    func carouselTemplateDidGetErorr(_ error: Error) {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidGetEmptyData() {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidSelected(_ model: YoutubeVideo) {
        listener?.selecteItem(selectedModel: model, self)
    }
    
    func carouselTemplateGotData() {
        
    }
    
    func carouselTemplateDidViewAll(categoryId: String) {
        listener?.didViewAll(self, categoryId: categoryId)
    }
}
