//
//  ProductGridCell.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/19/21.
//

import UIKit
import Reusable

protocol GridItemable {
    var id: Int { get set }
    var title: String { get  set }
    var moreInfo: String { get set }
}

class ProductGridCell: UICollectionViewCell, NibReusable {

    @IBOutlet private weak var productImageContainerView: UIView!
    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var brandNameLabel: UILabel!
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }

    private func setupViews() {
        productImageContainerView.backgroundColor = Colors.red300
        productImageContainerView.layer.cornerRadius = 8.0
        productImageContainerView.layer.masksToBounds = true
        
        productNameLabel.setStyle(DS.T12M(color: Colors.ink500))
        brandNameLabel.setStyle(DS.T12R(color: Colors.ink400s))
    }
    
    func configureCell(viewModel: TemplateCellViewModel) {
        thumbnailImageView.kf.setImage(with: URL(string: viewModel.thumbnailUrlString), placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
        productNameLabel.text = viewModel.title
        brandNameLabel.text = viewModel.moreInfo
    }
}
