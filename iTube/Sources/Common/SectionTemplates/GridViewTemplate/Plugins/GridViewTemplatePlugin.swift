//
//  GridViewTemplatePlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import Foundation

class GridViewTemplatePlugin: HomeSectionPlugin {
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    let service: TemplateServiceType
    init(service: TemplateServiceType) {
        self.service = service
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return GridViewTemplateWireframe(extraData: extraData, service: service, listener: self)
    }
}

extension GridViewTemplatePlugin: GridViewTemplateListener {
    func carouselTemplateDidGetErorr(_ error: Error) {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidGetEmptyData() {
        listener?.hidePlugin(self)
    }
    
    func carouselTemplateDidSelected(_ model: YoutubeVideo) {
        listener?.selecteItem(selectedModel: model, self)
    }
    
    func carouselTemplateGotData() {
        
    }
    
    func carouselTemplateDidViewAll(categoryId: String) {
        listener?.didViewAll(self, categoryId: categoryId)
    }
}
