//
//  CircleCarouselTemplatePlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/19/21.
//

import Foundation
import Resolver

class CircleCarouselTemplatePlugin: HomeSectionPlugin {
    
    @LazyInjected var logger: Loggable
    
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return CircleCarouselTemplateWireframe(listener: self)
    }
}

extension CircleCarouselTemplatePlugin: CircleCarouselTemplateListener {
    func circleCarouselTemplateDidSelected(_ model: CircleCarouselItemable) {
        logger.succees("Did selected item id: \(model.id)", file: "", line: nil)
    }
    
    func cirlceCarouselTemplateDidGetErorr(_ error: SuperSpaManagementError) {
        logger.error("circle carousle template get error \(error.localizedDescription)", file: "", line: nil)
    }
    
    func circleCarouselTemplateDidGetEmptyData() {
        
    }
    
    func cirlceCaroulseTemplateGotData() {
        
    }
}
