//
//  BannerTemplatePlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import Foundation

class BannerTemplatePlugin: HomeSectionPlugin {
    
    var listener: HomeSectionPluginListener?
    
    var extraData: [String : Any] = [:]
    
    let bannerService: BannerServiceType
    init(bannerService: BannerServiceType) {
        self.bannerService = bannerService
    }
    
    func build(listener: HomeSectionPluginListener) -> BaseWireframe {
        self.listener = listener
        return BannerTemplateWireframe(bannerService: bannerService, listener: self)
    }
}

extension BannerTemplatePlugin: BannerTemplateListener {
    func bannerTemplateDidSelected(model: YoutubeVideo) {
        listener?.selecteItem(selectedModel: model, self)
    }
    
    func bannerTemplateDidGetErorr(_ error: SuperSpaManagementError) {
        listener?.hidePlugin(self)
    }
    
    func bannerTemplateSelected(categoryId: String) {
        listener?.didViewAll(self, categoryId: categoryId)
    }
}
