//
//  ITubePagerItemCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import UIKit
import FSPagerView
import Kingfisher

class ITubePagerItemCell: FSPagerViewCell {

    @IBOutlet private weak var thumnailImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(viewModel: ITubePagerItemViewModel) {
        thumnailImageView.kf.setImage(with: viewModel.thumbnailURL, placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
    }

}
