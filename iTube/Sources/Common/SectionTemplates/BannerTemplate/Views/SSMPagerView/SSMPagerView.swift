//
//  SuperSpaManagementPagerView.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/18/21.
//

import UIKit
import Reusable
import FSPagerView
import RxSwift
import RxCocoa

private let pageBackgroundColors: [UIColor] = [
    Colors.yellow.withAlphaComponent(0.7),
    Colors.green300,
    Colors.ds2Warning1.withAlphaComponent(0.8),
    Colors.red300
]

struct ITubePagerViewModel {
    let items: [Banner]
}

struct ITubePagerItemViewModel {
    let model: Banner
    
    var thumbnailURL: URL? {
        return URL(string: model.urlString)
    }
}


class SSMPagerView: UIView, NibOwnerLoadable {
    private var pagerItems: [ITubePagerItemViewModel] = []
    
    var didSelectedItem: ((Banner) -> Void)?
    
    @IBOutlet weak var pagerView: FSPagerView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    private func setupViews() {
        self.loadNibContent()
        pagerView.automaticSlidingInterval = 3.0
        pagerView.isInfinite = true
        pagerView.register(UINib(nibName: "ITubePagerItemCell", bundle: nil), forCellWithReuseIdentifier: "iTube_Pager_item_cell")
        pagerView.dataSource = self
        pagerView.delegate = self
    }
    
    func configureView(viewModel: ITubePagerViewModel) {
        pagerItems = viewModel.items.map { ITubePagerItemViewModel(model: $0) }
        pagerView.reloadData()
    }
}

extension SSMPagerView: FSPagerViewDelegate {
    
}

extension SSMPagerView: FSPagerViewDataSource {
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        guard  let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "iTube_Pager_item_cell", at: index) as? ITubePagerItemCell else {
            return FSPagerViewCell()
        }
        cell.configureCell(viewModel: pagerItems[index])
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        self.didSelectedItem?(pagerItems[index].model)
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return pagerItems.count
    }
}
