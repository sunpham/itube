//
//  SettingItemCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/19/21.
//

import UIKit
import Reusable

class SettingItemCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dividerView: UIView!
    @IBOutlet private weak var toggleWrapView: UIView!
    @IBOutlet private weak var stateSwitch: UISwitch!
    @IBOutlet private weak var iconImageView: UIImageView!
    
    var onToggleState: VoidCallBack?
    
    var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    var displayToggle: BoolEnum = .off {
        didSet {
            if displayToggle.isOn {
                toggleWrapView.isHidden = false
            } else {
                toggleWrapView.isHidden = true
            }
        }
    }
    
    var isToggle: BoolEnum = .off {
        didSet {
            if isToggle.isOn {
                stateSwitch.setOn(true, animated: true)
            } else {
                stateSwitch.setOn(false, animated: true)
            }
        }
    }
    
    @IBAction func onSwitchState(_ sender: Any) {
        onToggleState?()
    }
    
    var icon: UIImage? {
        didSet {
            iconImageView.image = icon?.template.color(Colors.ink400)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        dividerView.backgroundColor = Colors.blue200
        titleLabel.setStyle(DS.T14R(color: Colors.ink500))
        stateSwitch.onTintColor = Colors.red400
    
    }
}
