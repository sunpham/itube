//
//  CollectionWireframe.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/22/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

struct CollectionsTabManElement {
    let type: CollectionsTabmanElementType
    let viewController: UIViewController
}

enum CollectionsTabmanElementType {
    case histories
    case following
    case downloaded
    
    var title: String {
        switch self {
        case .histories:
            return "Đã xem gần đây"
            
        case .following:
            return "Đang theo dõi"
            
        case .downloaded:
            return "Đã tải xuống"
        }
    }
}

typealias CollectionsTabManData = [CollectionsTabManElement]


final class CollectionWireframe: BaseWireframe {

    // MARK: - Private properties -
    var tabmanData: CollectionsTabManData = []
    private let storyboard = UIStoryboard(name: "Collection", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: CollectionViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = CollectionInteractor()
        let presenter = CollectionPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension CollectionWireframe: CollectionWireframeInterface {
    func configurePageBoyViewControllers() {
        guard let collectionViewController = viewController as? CollectionViewInterface else { return }
        
        let historiesPageWirefarme = CollectionPageWireframe(service: HistoriesCollectionPageService())
        let followingPageWireframe = CollectionPageWireframe(service: FollowingCollectionPageService.default)
        let downloadedPageWireframe = CollectionPageWireframe(service: DownloadedCollectionPageService.default)
        
        tabmanData.append(CollectionsTabManElement(type: .histories, viewController: historiesPageWirefarme.viewController))
        tabmanData.append(CollectionsTabManElement(type: .following, viewController: followingPageWireframe.viewController))
        tabmanData.append(CollectionsTabManElement(type: .downloaded, viewController: downloadedPageWireframe.viewController))
        
        collectionViewController.setTabmanData(tabmanData)
    }
    
    func navigate(to option: CollectionNavigationOption) {
        switch option {
        case .videoPlayer(let video):
            let wireframe = VideoPlayerWireframe(youtubeVideo: video)
            let presentedViewController = TubeNavigationController(rootViewController: wireframe.viewController)
            presentedViewController.modalPresentationStyle = .fullScreen
            navigationController?.present(presentedViewController, animated: true, completion: nil)
        }
    }
}
