//
//  HighlightSmallVideoCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/22/21.
//

import UIKit
import Reusable
import Differentiator

struct CollectionItemViewModel {
    let model: CollectionItem
    
    var thumbnailURL: URL? {
        return URL(string: model.thumbnailUrlString)
    }

    var title: String {
        return model.title
    }
    
    var moreInfo: String {
        return model.moreInfo
    }
    
    var publishedTimeString: String {
        return "24/04/2021"
    }
    
    var style: CollectionItem.ItemType {
        return model.type
    }
    
    var tapInstructionTitle: String {
        switch style {
        case .channel:
            return "Đi tới channel"
            
        case .video:
            return "Bấm để xem video"
        }
    }
}

extension CollectionItemViewModel: Equatable {
    static func == (lhs: CollectionItemViewModel, rhs: CollectionItemViewModel) -> Bool {
        return lhs.identity == rhs.identity
    }
}

extension CollectionItemViewModel: IdentifiableType {
    typealias Identity = String
    var identity: String {
        return model.id
    }
}

class HighlightSmallVideoCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var thumbnailWrapView: UIView!
    @IBOutlet private weak var thumnailImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var moreInfoLabel: UILabel!
    
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var withConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        mainContainerView.layer.cornerRadius = 8.0
        mainContainerView.layer.masksToBounds = true
        
        thumbnailWrapView.layer.cornerRadius = 8.0
        thumbnailWrapView.layer.masksToBounds = true
        
        titleLabel.setStyle(DS.T12M(color: Colors.ink500))
        moreInfoLabel.setStyle(DS.T12R(color: Colors.ink400s))
    }
    
    func configureCell(viewModel: CollectionItemViewModel) {
        thumnailImageView.kf.setImage(with: viewModel.thumbnailURL, placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
        titleLabel.text = viewModel.title
        moreInfoLabel.text = viewModel.moreInfo
        
        switch viewModel.style {
        case .video:
            heightConstraint.constant = UIConfig.normalHeight
            withConstraint.constant = UIConfig.normalHeight * UIConfig.normalRatio
            
        case .channel:
            heightConstraint.constant = UIConfig.specialHeight
            withConstraint.constant = UIConfig.specialHeight * UIConfig.specialRatio
            thumbnailWrapView.layer.cornerRadius = UIConfig.specialHeight / 2
            thumbnailWrapView.layer.masksToBounds = true
        }
    }
    
    enum UIConfig {
        static let normalHeight: CGFloat = 104
        static let normalRatio: CGFloat = 3 / 4
        static let specialHeight: CGFloat = 64
        static let specialRatio: CGFloat = 1
    }
}
