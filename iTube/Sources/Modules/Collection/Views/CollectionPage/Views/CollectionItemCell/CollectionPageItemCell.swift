//
//  CollectionPageItemCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/22/21.
//

import UIKit
import Reusable

class CollectionPageItemCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var thumbnailWrapView: UIView!
    @IBOutlet private weak var thumnailImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var moreInfoLabel: UILabel!
    @IBOutlet private weak var viewAgainLlabel: UILabel!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var withConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        
        thumbnailWrapView.layer.cornerRadius = 8
        thumbnailWrapView.layer.masksToBounds = true
        
        titleLabel.setStyle(DS.T14M(color: Colors.ink500))
        moreInfoLabel.setStyle(DS.T12R(color: Colors.ink400s))
        
        viewAgainLlabel.setStyle(DS.T14R(color: Colors.red400))
    }
    
    func configureCell(viewModel: CollectionItemViewModel) {
        thumnailImageView.kf.setImage(with: viewModel.thumbnailURL, placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
        titleLabel.text = viewModel.title
        moreInfoLabel.text = viewModel.moreInfo
        
        switch viewModel.style {
        case .video:
            heightConstraint.constant = UIConfig.normalHeight
            withConstraint.constant = UIConfig.normalHeight * UIConfig.normalRatio
            moreInfoLabel.isHidden = false
            
        case .channel:
            heightConstraint.constant = UIConfig.specialHeight
            withConstraint.constant = UIConfig.specialHeight * UIConfig.specialRatio
            thumbnailWrapView.layer.cornerRadius = UIConfig.specialHeight / 2
            thumbnailWrapView.layer.masksToBounds = true
            moreInfoLabel.isHidden = true
        }
        
        viewAgainLlabel.text = viewModel.tapInstructionTitle
    }
    
    enum UIConfig {
        static let normalHeight: CGFloat = 172
        static let normalRatio: CGFloat = 3 / 4
        static let specialHeight: CGFloat = 72
        static let specialRatio: CGFloat = 1
    }
}
