//
//  FavouriteListViewController.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/20/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

protocol FavouriteListPresenterInterface: PresenterInterface {
    var selectedItemTriger: PublishRelay<YoutubeVideo> { get }
}

final class FavouriteListViewController: UIViewController, FavouriteListViewInterface {

    let viewModels = BehaviorRelay<[YoutubeVideoViewModel]>(value: [])
    private let viewDidLoadRelay = BehaviorRelay(value: false)
    private let disposeBag = DisposeBag()
    
    lazy var dataSource = makeDataSource()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var miniPlayerView: MiniPlayerView!
    // MARK: - Public properties -

    var presenter: FavouriteListPresenterInterface!

    // MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        defer { viewDidLoadRelay.accept(true) }
        
        configureViews()
        configurePresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hideShadowLine()
        miniPlayerView.reload()
        
        navigationItem.title = "Danh sách yêu thích"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }

}

// MARK: - Extensions -

extension FavouriteListViewController {
    func configureViews() {
        configureTableViews()
        
        miniPlayerView.onTapShowVideo = { [weak presenter] in
            if let video = Keeper.shared.currentYoutubeVideo {
                presenter?.selectedItemTriger.accept(video)
            }
        }
    }
    private func configureTableViews() {
        tableView.register(cellType: YoutubeVideoCell.self)
        tableView.register(cellType: SmallYoutubeVideoCell.self)
        tableView.contentInset = UIEdgeInsets(top: 24, left: .zero, bottom: .zero, right: .zero)
        
        tableView
            .rx
            .modelSelected(YoutubeVideoViewModel.self)
            .map { $0.model}
            .bind(to: presenter.selectedItemTriger)
            .disposed(by: disposeBag)
    }
}

extension FavouriteListViewController {
    func configurePresenter() {
        let didLoad = viewDidLoadRelay.asObservable()
        viewModels
            .distinctUntilChanged()
            .filterEmpty()
            .takeLatestWhen(didLoad)
            .map {[Section(model: 0, items: $0)] }
            .asDriver(onErrorDriveWith: .never())
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}

extension FavouriteListViewController{
    typealias Section = SectionModel<Int, YoutubeVideoViewModel>
    typealias DataSource = RxTableViewSectionedReloadDataSource<Section>
}

extension FavouriteListViewController {
    func makeDataSource() -> DataSource {
        return DataSource(configureCell: { (_, tableView, indexPath, viewModel) -> UITableViewCell in
            let cell: SmallYoutubeVideoCell = tableView.dequeueReusableCell(for: indexPath)
            cell.configureCell(viewModel: viewModel)
            return cell
        })
    }
}
