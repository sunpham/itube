//
//  FavouriteListWireframe.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/20/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class FavouriteListWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "FavouriteList", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: FavouriteListViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = FavouriteListInteractor()
        let presenter = FavouriteListPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension FavouriteListWireframe: FavouriteListWireframeInterface {
    func navigate(to option: FavouriteListNavigationOption) {
        switch option {
        case .videoPlayer(let youtubeVideo):
            let wireframe = VideoPlayerWireframe(youtubeVideo: youtubeVideo)
            let presentedViewController = TubeNavigationController(rootViewController: wireframe.viewController)
            presentedViewController.modalPresentationStyle = .fullScreen
            navigationController?.present(presentedViewController, animated: true, completion: nil)
        }
    }
}
