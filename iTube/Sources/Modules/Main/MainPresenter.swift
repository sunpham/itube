//
//  MainPresenter.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/17/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation
import UIKit


protocol MainWireframeInterface: WireframeInterface {
    func addTabBarViewControllers()
}

protocol MainViewInterface: ViewInterface {
    func addTabBarViewControllers(_ viewControllers: [UIViewController])
}

final class MainPresenter {

    // MARK: - Private properties -

    private unowned let view: MainViewInterface
    private let interactor: MainInteractorInterface
    private let wireframe: MainWireframeInterface

    // MARK: - Lifecycle -

    init(view: MainViewInterface, interactor: MainInteractorInterface, wireframe: MainWireframeInterface) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
        
        wireframe.addTabBarViewControllers()
    }
}

// MARK: - Extensions -

extension MainPresenter: MainPresenterInterface {
}
