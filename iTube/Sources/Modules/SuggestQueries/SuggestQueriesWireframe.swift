//
//  SuggestQueriesWireframe.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/21/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class SuggestQueriesWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "SuggestQueries", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: SuggestQueriesViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = SuggestQueriesInteractor()
        let presenter = SuggestQueriesPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }

}

// MARK: - Extensions -

extension SuggestQueriesWireframe: SuggestQueriesWireframeInterface {
    
    func detachSuggestQueries() {
        navigationController?.popViewController(animated: true)
    }
    func navigate(to option: SuggestQueriesNavigationOption) {
        switch option {
        
        case .search(let searchText):
            let wireframe = SearchWireframe(searchText: searchText)
            navigationController?.pushWireframe(wireframe)
        }
    }
}
