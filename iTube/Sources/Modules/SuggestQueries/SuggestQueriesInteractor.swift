//
//  SuggestQueriesInteractor.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/21/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import Foundation
import Resolver
import RxSwift

protocol SuggestQueriesInteractorInterface: InteractorInterface {
    func getSuggestions(for searchText: String) -> Observable<[String]>
}


final class SuggestQueriesInteractor {
    @LazyInjected var sugestQueiresService: SuggestQueriesServiceType
}

// MARK: - Extensions -

extension SuggestQueriesInteractor: SuggestQueriesInteractorInterface {
    func getSuggestions(for searchText: String) -> Observable<[String]> {
        return sugestQueiresService.getSuggestions(for: searchText)
    }
}
