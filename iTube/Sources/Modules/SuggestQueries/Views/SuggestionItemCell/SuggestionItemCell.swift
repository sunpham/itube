//
//  SuggestionItemCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/21/21.
//

import UIKit
import Reusable

class SuggestionItemCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var suggestionTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        suggestionTitleLabel.setStyle(DS.T16R(color: Colors.ink500))
    }
    
    func configureCell(suggestionTitle: String) {
        suggestionTitleLabel.text = suggestionTitle
    }
}
