//
//  ChannelInfoPageIntroduceCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/28/21.
//

import UIKit
import Reusable

class ChannelInfoPageIntroduceCell: UITableViewCell, NibReusable {

    @IBOutlet weak var introduceTextView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        introduceTextView.setStyle(DS.T16R(color: Colors.ink500))
    }
    
    func configureCell(content: String) {
        introduceTextView.text = content
    }
}
