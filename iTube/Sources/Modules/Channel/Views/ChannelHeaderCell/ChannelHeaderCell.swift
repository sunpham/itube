//
//  ChannelHeaderCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/15/21.
//

import UIKit
import Reusable

class ChannelHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet private weak var gradientView: DSGradientView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        gradientView.gradientConfig = .init(colors: [Colors.black200.withAlphaComponent(0), Colors.black500], direction: .topToBottom)
    }
}
