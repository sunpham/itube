//
//  ChannelContentCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/15/21.
//

import UIKit
import Reusable

class ChannelContentCell: UITableViewCell, NibReusable {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerHeightConstraint.constant = UIScreen.main.bounds.height + 120.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
