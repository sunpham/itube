//
//  HomeSectionPlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/17/21.
//

import Foundation

protocol HomeSectionPlugin: AnyObject {
    
    var listener: HomeSectionPluginListener? { get set }
    var extraData: [String: Any] { get  set }
    func build(listener: HomeSectionPluginListener) -> BaseWireframe
}

extension HomeSectionPlugin {
    var id: String {
        return "\(ObjectIdentifier(self).hashValue)"
    }
}

enum HomeSectionCodeType: String {
    case none = ""
    case bannerSection = "beepe_banner_section"
    case circleCarouselSection = "beepe_circle_carousel_section"
    case mediumCarouselSection = "beepe_medium_carousel_section"
    case largeCarouselSection = "beepe_large_carousel_section"
    case largeSingleCarouselSection = "beepe_large_single_carousel_section"
    case gridViewSection = "beepe_grid_view_section"
    case singleViewSection = "SuperSpaManagement_single_view_section"
    case favouriteViewSection = "tube_favourite_view_section"
}
enum HomeSectionPlugins {
    static var registeredPluginsMapping: [HomeSectionCodeType: HomeSectionPlugin] {
        return [
            .bannerSection : BannerTemplatePlugin(bannerService: HomeBannerSerivce()),
            .circleCarouselSection: CircleCarouselTemplatePlugin(),
            .mediumCarouselSection: MediumCarouselTemplatePlugin(service: TemplateService.default),
            .largeCarouselSection: LargeCarouselTemplatePlugin(service: TemplateService.default),
            .largeSingleCarouselSection: LargeSingleCarouselTemplatePlugin(service: TemplateService.default),
            .gridViewSection: GridViewTemplatePlugin(service: TemplateService.default),
            .favouriteViewSection: LargeCarouselTemplatePlugin(service: FavouriteTemplateService.default)
        ]
    }
}

