//
//  HomeSectionPluginListener.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/17/21.
//

import Foundation

/// Plugin defination
protocol HomeSectionPluginListener: AnyObject {
    func showPlugin(_ plugin: HomeSectionPlugin)
    func hidePlugin(_ plugin: HomeSectionPlugin)
    func detachPlugin(_ plugin: HomeSectionPlugin)
    func reloadPlugin(_ plugin: HomeSectionPlugin)
    func selecteItem(selectedModel: YoutubeVideo, _ plugin: HomeSectionPlugin)
    func didViewAll(_ plugin: HomeSectionPlugin, categoryId: String)
}

extension HomeSectionPluginListener {
    func selecteItem(itemId: String, _ plugin: HomeSectionPlugin) {}
    func selecteItem(selectedModel: YoutubeVideo, _ plugin: HomeSectionPlugin){}
    func didViewAll(_ plugin: HomeSectionPlugin, categoryId: String) {}
}
