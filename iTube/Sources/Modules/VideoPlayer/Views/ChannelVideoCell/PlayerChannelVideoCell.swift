//
//  PlayerChannelVideoCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/5/21.
//

import UIKit
import Reusable

class PlayerChannelVideoCell: UITableViewCell, NibReusable {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(viewModel: YoutubeVideoViewModel) {}
}
