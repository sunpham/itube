//
//  PlayerRelatedVideosSectionCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/17/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa
import RxDataSources

class PlayerRelatedVideosSectionCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    private lazy var dataSource = makeDataSource()
    private let disposeBag = DisposeBag()
    
    var onLoadMore: VoidCallBack?
    var onSelectedVideo: ((YoutubeVideo) -> Void)?
    
    let viewModels = BehaviorRelay<[TemplateCellViewModel]>(value: [])
    var style: CarouselTemplateStyle = .large
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCollectionView()
        configureDataSource()
    }
    
    func configureCell(viewModel: RelatedVideosViewModel) {
        viewModels.accept(viewModel.items.map { TemplateCellViewModel(model: $0.convertToCarouselItem()) })
    }
}


extension PlayerRelatedVideosSectionCell {
    private func configureCollectionView() {
        collectionViewHeightConstraint.constant = style.mapToCellHeight
        let collectionViewLayout = CarouselCollectionViewFlowLayout()
            .setItemSize(
                CGSize(
                    width: style.mapToCellWidth,
                    height: style.mapToCellHeight
                )
            )
            .setPadding(16)
            .setScrollDirection(.horizontal)
        collectionView.collectionViewLayout = collectionViewLayout
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsets(top: .zero, left: UIConfig.cellPadding, bottom: .zero, right: .zero)
        
        collectionView
            .rx
            .loadMoreTrigger
            .subscribeNext { [weak self] in
                self?.onLoadMore?()
            }
            .disposed(by: disposeBag)
        
        collectionView
            .rx.modelSelected(TemplateCellViewModel.self)
            .map { $0.model.model }
            .subscribeNext { [weak self] video in
                self?.onSelectedVideo?(video)
            }
            .disposed(by: disposeBag)
        
        registerCells()
    }
    
    private func registerCells() {
        collectionView.register(cellType: ProductCarouselCell.self)
    }
    
    enum UIConfig {
        static let cellPadding: CGFloat = 16
        static let smallCellHeight: CGFloat = smallCellWidth * 9 / 16 + 56
        static var smallCellWidth: CGFloat = (UIScreen.main.bounds.width * 0.95 - (cellPadding  * 2) - 16) / 2
    }
}

extension PlayerRelatedVideosSectionCell {
    private func configureDataSource() {
        
        viewModels
            .filterEmpty()
            .map { [Section(model: 0, items: $0)] }
            .asDriver(onErrorDriveWith: .never())
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}

extension PlayerRelatedVideosSectionCell {
    typealias Section = SectionModel<Int, TemplateCellViewModel>
    typealias DataSource = RxCollectionViewSectionedReloadDataSource<Section>
}

extension PlayerRelatedVideosSectionCell {
    func makeDataSource() -> DataSource {
        return DataSource { [weak self] (_, collectionView, indexPath, model) -> UICollectionViewCell in
            guard let self = self else { fatalError("Self not available")}
            let cell: ProductCarouselCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.configureCell(viewModel: model, style: self.style)
            return cell
        }
    }
}

