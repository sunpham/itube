//
//  PlayerVideoInfoCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/5/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

struct PlayerVideoInfoViewModel: Equatable {
    let model: YoutubeVideo
    let isFollowing: Bool
    
    static func == (lhs: PlayerVideoInfoViewModel, rhs: PlayerVideoInfoViewModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    var id: String {
        return model.id
    }
    
    var videoTitle: String {
        return (model.snippet?.title).or("")
    }
    
    var channelTitle: String {
        return (model.snippet?.channelTitle).or("")
    }
}

class PlayerVideoInfoCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var channelShortcutLabel: UILabel!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var channelTitleLabel: UILabel!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var channelLogoWrapView: UIView!
    @IBOutlet weak var recommendationContainerView: UIView!
    @IBOutlet weak var channelInfoContainerView: UIView!
    @IBOutlet weak var followButton: FollowButton!
    
    private let channelBackgroundColors: [UIColor] = [#colorLiteral(red: 0.8511460977, green: 0.1191920082, blue: 0.06651309384, alpha: 1), #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1), #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)]
    
    var onTapChannelInfo: VoidCallBack?
    var onFollowingChannel: VoidCallBack?
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        videoTitleLabel.setStyle(DS.T20R(color: Colors.ink500))
        channelTitleLabel.setStyle(DS.T16M(color: Colors.ink500))
        channelShortcutLabel.setStyle(DS.T30M(color: Colors.white500))
        dividerView.backgroundColor = Colors.blue200
        
        let channelTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapChannelInfo))
        channelInfoContainerView.addGestureRecognizer(channelTapGesture)
        
        followButton.onTapFollow = { [weak self] in
            self?.onFollowingChannel?()
        }
    }
    
    @objc func didTapChannelInfo() {
        onTapChannelInfo?()
    }
    
    func configureCell(viewModel: PlayerVideoInfoViewModel) {
        videoTitleLabel.text = viewModel.videoTitle
        channelTitleLabel.text = viewModel.channelTitle
        
        channelLogoWrapView.layer.cornerRadius = 28
        channelLogoWrapView.layer.masksToBounds = true
        channelLogoWrapView.backgroundColor = channelBackgroundColor()
        
        if viewModel.channelTitle.isNotEmpty,  let character = viewModel.channelTitle.trimmingCharacters(in: .whitespacesAndNewlines).first {
            channelShortcutLabel.text = String(character)
        }
        
        followButton.style = viewModel.isFollowing ? .following : .follow
    }
    
    func channelBackgroundColor() -> UIColor {
        let index = Int.random(in: 0..<channelBackgroundColors.count)
        return channelBackgroundColors[index]
    }
}
