//
//  FollowButton.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/24/21.
//

import UIKit
import Reusable

class FollowButton: UIView, NibOwnerLoadable {
    
    var style: Style = .follow {
        didSet {
            updateViewSttyle()
        }
    }
    
    @IBOutlet private weak var mainContainerView: UIView!
    @IBOutlet private weak var rightImageView: UIImageView!
    @IBOutlet private weak var rightImageContainerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    var onTapFollow: VoidCallBack?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        mainContainerView.backgroundColor = Colors.white500
        
        mainContainerView.layer.cornerRadius = 8.0
        mainContainerView.layer.masksToBounds = true
        
        mainContainerView.layer.borderWidth = 0.2
        mainContainerView.layer.borderColor = Colors.ink400s.cgColor
        
        style = .following
    }
    
    @IBAction func onTap(_ sender: Any) {
        onTapFollow?()
    }
    
    func updateViewSttyle() {
        titleLabel.text = style.title
        rightImageView.image = style.icon
        mainContainerView.backgroundColor = style.backgroudColor
        titleLabel.setStyle(DS.T14M(color: style.titleColor))
        rightImageContainerView.isHidden = style.shouldHiddenIcon
    }
}

extension FollowButton {
    enum Style {
        case follow
        case following
        
        var title: String {
            switch self {
            case .follow:
                return "Theo dõi".uppercased()
                
            case .following:
                return "Bỏ theo dõi".uppercased()
            }
        }
        
        var icon: UIImage? {
            switch self {
            case .follow:
                return nil
                
            case .following:
                return Asset.Icon.Common.icIosSystemNotification.image.color(titleColor)
            }
        }
        
        var backgroudColor: UIColor {
            switch self {
            case .follow:
                return Colors.white500
                
            case .following:
                return Colors.white500
            }
        }
        
        var titleColor: UIColor {
            switch self {
            case .follow:
                return #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                
            case .following:
                return Colors.ink400s
            }
        }
        
        var shouldHiddenIcon: Bool {
            if icon == nil {
                return true
            }
            
            return false
        }
    }
}
