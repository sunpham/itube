//
//  SectionHeaderCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/17/21.
//

import UIKit
import Reusable

class SectionHeaderCell: UITableViewCell, NibReusable {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews() {
        titleLabel.setStyle(DS.T16M(color: Colors.ink500))
    }
    
    func configureCell(title: String) {
        titleLabel.text = title
    }
}
