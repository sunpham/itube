//
//  PlayerControlsView.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/19/21.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class PlayerControlsView: UIView, NibOwnerLoadable {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var prevVideoImageView: UIImageView!
    @IBOutlet private weak var pauseImageView: UIImageView!
    @IBOutlet private weak var nextVideoImageView: UIImageView!
    @IBOutlet private weak var repeateVideoImageView: UIImageView!
    @IBOutlet private weak var favouriteImageView: UIImageView!
    
    var onTapRandomMode: VoidCallBack?
    var onTapPreviousVideo: VoidCallBack?
    var onTapPauseVideo: VoidCallBack?
    var onTapNextVideo: VoidCallBack?
    var onTapRepeatedVideoMode: VoidCallBack?
    var onTapFavouriteVideo: VoidCallBack?
    
    private let isShuffle = BehaviorRelay<BoolEnum>(value: .off)
    private let isRepeated = BehaviorRelay<BoolEnum>(value: .off)
    private let isPlaying = BehaviorRelay<BoolEnum>(value: .off)
    private let disposeBag = DisposeBag()
    
    private var isFavourteVideo: Bool {
        return (Keeper.shared.currentYoutubeVideo?.isFavourite).or(false)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupViews()
    }
    
    private func setupViews() {
        self.loadNibContent()
        
        containerView.setShadowStyle(.shadow4Revert)
        
        let controlImageAssets = [
            Asset.Icon.PlayerControls.icRepeatSongMode,
            Asset.Icon.PlayerControls.icPreviousSong,
            Asset.Icon.PlayerControls.icPauseSong,
            Asset.Icon.PlayerControls.icNextSong,
            Asset.Icon.PlayerControls.icFavorite
        ]
        
        let controlImageViews = [
            repeateVideoImageView,
            prevVideoImageView,
            pauseImageView,
            nextVideoImageView,
            favouriteImageView
        ]
        
        zip(controlImageAssets, controlImageViews)
            .forEach { imageAsset, imageView in
                imageView?.image = imageAsset.image
                imageView?.setImageColor(color: Colors.red400)
            }
        
        isShuffle
            .asDriver()
            .driveNext { _ in
            }
            .disposed(by: disposeBag)
        
        isRepeated
            .asDriver()
            .driveNext { [weak self] state in
                self?.setImageColor(by: state, for: self?.repeateVideoImageView)
            }
            .disposed(by: disposeBag)
        
        isPlaying
            .asDriver()
            .driveNext { [weak self] state in
                if state.isOn {
                    self?.pauseImageView.image = Asset.Icon.PlayerControls.icPauseSong.image
                } else {
                    self?.pauseImageView.image = Asset.Icon.PlayerControls.icPlaySong.image
                }
            }
            .disposed(by: disposeBag)
        
        let _isPlaying: BoolEnum = (AVPlayerViewControllerManager.shared.player?.isPlaying).or(false) ? .on : .off
        isPlaying.accept(_isPlaying)
        
        AVPlayerViewControllerManager.shared.onPaused = { [weak self] in
            self?.isPlaying.accept(.off)
        }
        
        AVPlayerViewControllerManager.shared.onPlaying = { [weak self] in
            self?.isPlaying.accept(.on)
        }
        updateListeners()
        updateFavouriteStatus()
    }
    
    private func updateFavouriteStatus() {
        if isFavourteVideo {
            favouriteImageView.image = Asset.Icon.PlayerControls.icFavorite.image
            favouriteImageView.tintColor = Colors.red400
        } else {
            favouriteImageView.image = Asset.Icon.PlayerControls.icUnFavorite.image
            favouriteImageView.tintColor = Colors.ink400
        }
    }
    
    private func setImageColor(by state: BoolEnum, for imageView: UIImageView?) {
        if state.isOn {
            imageView?.tintColor = Colors.red400
            return
        }
        
        imageView?.tintColor = Colors.ink400
    }
    
    private func updateListeners() {
        isShuffle.accept(SettingsService.shared.playerSettings.isShuffle)
        isRepeated.accept(SettingsService.shared.playerSettings.isRepeated)
    }
    
    @IBAction func onTapShuffleVideo(_ sender: Any) {
        SettingsService.shared.playerSettings.toggleIsShuffle()
        updateListeners()
        
        onTapRandomMode?()
    }
    
    @IBAction func onTapPrevVideo(_ sender: Any) {
        onTapPreviousVideo?()
    }
    
    @IBAction func onTapPause(_ sender: Any) {
        guard let player = AVPlayerViewControllerManager.shared.player else { return }
        if player.isPlaying {
            player.pause()
        } else {
            player.play()
        }
        onTapPauseVideo?()
    }
    
    @IBAction func onTapNextVideo(_ sender: Any) {
        onTapNextVideo?()
    }
    
    @IBAction func onTapRepeated(_ sender: Any) {
        SettingsService.shared.playerSettings.toggleIsRepeated()
        updateListeners()
        onTapRepeatedVideoMode?()
    }
    
    @IBAction func onTapFavorite(_ sender: Any) {
        var currentVideo = Keeper.shared.currentYoutubeVideo
        currentVideo?.isFavourite.toggle()
        Keeper.shared.currentYoutubeVideo = currentVideo
        
        updateFavouriteStatus()
        onTapFavouriteVideo?()
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
