//
//  SmallYoutubeVideoCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/15/21.
//

import UIKit
import Reusable

class SmallYoutubeVideoCell: UITableViewCell, NibReusable {
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    
    @IBOutlet private weak var videoTitleLabel: UILabel!
    @IBOutlet private weak var channelTitleLabel: UILabel!

    @IBOutlet private weak var channelLogoWrapView: UIView!
    @IBOutlet private weak var imageContainerWrapView: UIView!
    
    @IBOutlet private weak var playListInfoView: UIView!
    @IBOutlet private weak var playListInfoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }

    private func setupViews() {
        videoTitleLabel.setStyle(DS.T14R(color: Colors.ink500))
        
        channelTitleLabel.setStyle(DS.T12R(color: Colors.ink400s))
        
        imageContainerWrapView.layer.cornerRadius = 8
        imageContainerWrapView.layer.masksToBounds = true
            
        channelLogoWrapView.layer.cornerRadius = 12
        channelLogoWrapView.layer.masksToBounds = true
        channelLogoWrapView.backgroundColor = Colors.red300
        
        playListInfoView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        playListInfoLabel.setStyle(DS.T14M(color: Colors.white300))
        playListInfoLabel.text = "Xem tất cả video"
        
    }
    
    func configureCell(viewModel: YoutubeVideoViewModel) {
        thumbnailImageView.kf.setImage(with: viewModel.thumbnailURL, placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
        videoTitleLabel.text = viewModel.title
        
        channelTitleLabel.text = viewModel.channelTitle
        
        playListInfoView.isHidden = !viewModel.isPlayList
    }
}
