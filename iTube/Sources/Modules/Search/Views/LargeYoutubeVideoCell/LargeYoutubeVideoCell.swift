//
//  LargeYoutubeVideoCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//

import UIKit
import Reusable

class LargeYoutubeVideoCell: UITableViewCell, NibReusable {
    
    @IBOutlet private weak var thumbnailWraperView: UIView!
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    
    @IBOutlet private weak var channelWraperView: UIView!
    @IBOutlet private weak var subscribeButton: UIButton!
    
    @IBOutlet private weak var videoTitleLabel: UILabel!
    @IBOutlet private weak var channelTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        setupThumbnail()
        setupChannel()
    }
    
    private func setupThumbnail() {
        thumbnailWraperView.layer.borderWidth = 0.1
        thumbnailWraperView.layer.borderColor = Colors.ink400s.cgColor
        
        thumbnailWraperView.layer.masksToBounds = true
        thumbnailWraperView.layer.cornerRadius = 16.0
    }
    
    private func setupChannel() {
        channelWraperView.layer.masksToBounds = true
        channelWraperView.layer.cornerRadius = channelWraperView.bounds.height / 2
        
        subscribeButton.layer.masksToBounds = true
        subscribeButton.layer.cornerRadius = subscribeButton.bounds.height / 2
        subscribeButton.backgroundColor = Colors.red500
        subscribeButton.tintColor = .white
        subscribeButton.setTitle("Đăng kí", for: .normal)
        
        self.layoutIfNeeded()
    }
}

extension LargeYoutubeVideoCell {
    func configureCell(viewModel: YoutubeVideoViewModel) {
        thumbnailImageView.kf.setImage(with: viewModel.thumbnailURL, placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
        
        videoTitleLabel.text = viewModel.title
        
        channelTitleLabel.text = viewModel.channelTitle
    }
}
