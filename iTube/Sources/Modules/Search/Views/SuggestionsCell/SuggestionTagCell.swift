//
//  SuggestionTagCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 04/06/2021.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class SuggestionTagCell: UITableViewCell, NibReusable {

    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var collectionView: DSTagCollectionView!
    
    private let disposeBag = DisposeBag()
    
    public var didUpdateLayout: VoidCallBack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        collectionView
            .rx
            .contentSize
            .asDriver(onErrorDriveWith: .never())
            .map { $0.or(.zero)}
            .map { $0.height }
            .distinctUntilChanged()
            .driveNext { [weak self] height in
                self?.containerViewHeightConstraint.constant = height
                self?.didUpdateLayout?()
            }
            .disposed(by: disposeBag)
    }
 
    func configureCell(tagItems: [DSTagItemType]) {
        collectionView.setTagItems(tagItems)
    }
}
