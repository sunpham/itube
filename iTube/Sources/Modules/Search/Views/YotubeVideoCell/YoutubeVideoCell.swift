//
//  YoutubeVideoCell.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/4/21.
//

import UIKit
import Reusable
import Differentiator


struct YoutubeVideoViewModel {
    let model: YoutubeVideo
    
    var thumbnailURL: URL? {
        return URL(string: (model.snippet?.thumbnails?.getThumbnailUrlString()).or(""))
    }

    var title: String {
        return (model.snippet?.title).or("")
    }
    
    var channelTitle: String {
        return (model.snippet?.channelTitle).or("")
    }
    
    var publishedTimeString: String {
        return "24/04/2021"
    }
    
    var isPlayList: Bool {
        return model.objectKind == .playList
    }
}

extension YoutubeVideoViewModel: Equatable {
    static func == (lhs: YoutubeVideoViewModel, rhs: YoutubeVideoViewModel) -> Bool {
        return lhs.identity == rhs.identity
    }
}

extension YoutubeVideoViewModel: IdentifiableType {
    typealias Identity = String
    var identity: String {
        return model.id
    }
}

class YoutubeVideoCell: UITableViewCell, NibReusable {
    @IBOutlet private weak var thumbnailImageView: UIImageView!
    
    @IBOutlet private weak var videoTitleLabel: UILabel!
    @IBOutlet private weak var channelTitleLabel: UILabel!

    @IBOutlet private weak var channelLogoWrapView: UIView!
    @IBOutlet private weak var imageContainerWrapView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    @IBOutlet private weak var playListInfoView: UIView!
    @IBOutlet private weak var playListInfoLabel: UILabel!
    private func setupViews() {
        videoTitleLabel.setStyle(DS.T16M(color: Colors.ink500))
        
        channelTitleLabel.setStyle(DS.T14R(color: Colors.ink400s))
        
        imageContainerWrapView.layer.cornerRadius = 8.0
        imageContainerWrapView.layer.masksToBounds = true
            
        channelLogoWrapView.layer.cornerRadius = 16
        channelLogoWrapView.layer.masksToBounds = true
        channelLogoWrapView.backgroundColor = Colors.red300
        
        playListInfoView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        playListInfoLabel.setStyle(DS.T16M(color: Colors.white300))
        playListInfoLabel.text = "Xem tất cả video"
        
    }
    
    func configureCell(viewModel: YoutubeVideoViewModel) {
        thumbnailImageView.kf.setImage(with: viewModel.thumbnailURL, placeholder: Asset.Image.Common.icIosCutePaceholder.image, options: [.transition(.fade(0.25))])
        videoTitleLabel.text = viewModel.title
        
        channelTitleLabel.text = viewModel.channelTitle
        playListInfoView.isHidden = !viewModel.isPlayList
    }
}

extension BinaryInteger {
    func speratorFormatedNumberAndConcatPefix(prefixString: String) -> String {
        return prefixString + "\(self.formattedWithSeparator)"
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension BinaryFloatingPoint {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

extension Int {
    var abbreviated: String {
        let abbrev = "KMBTPE"
        return abbrev.enumerated().reversed().reduce(nil as String?) { accum, tuple in
            let factor = Double(self) / pow(10, Double(tuple.0 + 1) * 3)
            let format = (factor.truncatingRemainder(dividingBy: 1) == 0 ? "%.0f%@" : "%.1f%@")
            return accum ?? (factor > 1 ? String(format: format, factor, String(tuple.1)) : nil)
        } ?? String(self)
    }
}
