//
//  SuggestQueriesService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/20/21.
//

import Foundation
import RxSwift
import Moya
import Resolver

protocol SuggestQueriesServiceType {
    func getSuggestions(for searchText: String) -> Observable<[String]>
}

class SuggestQueriesService {
    @LazyInjected var api: Networkable
    static let `default` = SuggestQueriesService()
}

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
enum SuggestionsElement: Codable {
    case string(String)
    case stringArray([String])
    case welcomeClass(SuggestionsMoreInfo)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let _suggestions = try? container.decode([String].self) {
            self = .stringArray(_suggestions)
            return
        }
        if let searchText = try? container.decode(String.self) {
            self = .string(searchText)
            return
        }
        if let moreInfo = try? container.decode(SuggestionsMoreInfo.self) {
            self = .welcomeClass(moreInfo)
            return
        }
        throw DecodingError.typeMismatch(SuggestionsElement.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for WelcomeElement"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .stringArray(let x):
            try container.encode(x)
        case .welcomeClass(let x):
            try container.encode(x)
        }
    }
    
    var suggestions: [String] {
        switch self {
        case .stringArray(let _suggestions):
            return _suggestions
            
        default:
            return []
        }
    }
}

struct SuggestionsMoreInfo: Codable {
    let googleSuggestsubtypes: [[Int]]

    enum CodingKeys: String, CodingKey {
        case googleSuggestsubtypes = "google:suggestsubtypes"
    }
}

extension SuggestQueriesService: SuggestQueriesServiceType {
    func getSuggestions(for searchText: String) -> Observable<[String]> {
        let target = SuggestQueriesTargets.Suggestions(query: searchText)
        return api.request(target: target)
            .observe(on: MainScheduler.instance)
            .asObservable()
            .map{ [weak self] response -> [String] in
                guard let self = self else { return [] }
                return self.parsedSuggestQuereis(from: response)
            }
            .doOnNext{ suggestions in
                UserDefaults.standard.suggestedQuereis = suggestions
            }
            .asObservable()
    }
    
    private func parsedSuggestQuereis(from response: Response) -> [String] {
        let contentJSONString = String(data: response.data, encoding: .ascii)
        do {
            if let convertedData = contentJSONString?.data(using: .utf8) {
                let parsedObjects = try JSONDecoder().decode([SuggestionsElement].self, from: convertedData)
                let suggestions =  parsedObjects.map { $0.suggestions }.filter { $0.isNotEmpty }.first
                return (suggestions).or([])
            }
        } catch {
            debugPrint("Error when parsing suggestions:  \(error)")
        }
        
        return []
    }
}

protocol SuggestQueriesTargetType: TargetType {}


extension SuggestQueriesTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiSuggestQueries) else {
            fatalError("Invalid API Base : \(Natrium.apiSuggestQueries)")
        }
        
        return url
    }
}

enum SuggestQueriesTargets {
    struct Suggestions: SuggestQueriesTargetType {
        
        let query: String
        
        var path: String {
            return ""
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        
        var parameters: [String: Any] {
            return [
                "client": "firefox",
                "ds": "yt",
                "q": query
            ]
        }
    }

}
