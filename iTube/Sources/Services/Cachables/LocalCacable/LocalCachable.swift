//
//  LocalCachable.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/10/21.
//

import Foundation

struct UserDefaulAPIKey: Codable {
    var currentIndex: Int
    var count: Int
    
    enum CodingKeys: String, CodingKey {
        case currentIndex = "current_index"
        case count
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        currentIndex = (try? container.decode(Int.self, forKey: .currentIndex)).or(.zero)
        count = (try? container.decode(Int.self, forKey: .count)).or(.zero)
    }
    
    mutating func refreshAPIKey() {
        currentIndex = 0
    }
    
    mutating func goToNextApiKey() {
        if currentIndex < count - 1 {
            currentIndex = currentIndex + 1
        } else {
            refreshAPIKey()
        }
    }
    
    init(currentIndex: Int, count: Int) {
        self.currentIndex = currentIndex
        self.count = count
    }
}

extension UserDefaults {
    
    private enum Keys: String {
        case apiKeyInfoStorage
        case isAutoUpdate
        case playerSettings
        case currentTotalVideoPlayed
        case pushNotification
        case suggestedQuereis
    }
    
    var pushNotification: BoolEnum {
        get {
            return BoolEnum(from: bool(forKey: Keys.pushNotification.rawValue))
        }
        
        set {
            setValue(newValue.value, forKey: Keys.pushNotification.rawValue)
        }
    }
    
    var apiKeyInfo: UserDefaulAPIKey? {
        get {
            guard let data = data(forKey: Keys.apiKeyInfoStorage.rawValue),
                  let info = try? JSONDecoder().decode(UserDefaulAPIKey.self, from: data) else {
                return nil
            }
            return info
        }
        
        set {
            guard let data = try? JSONEncoder().encode(newValue) else { return }
            setValue(data, forKey: Keys.apiKeyInfoStorage.rawValue)
        }
    }
    
    var isAutoUpdate: Bool {
        get {
            return bool(forKey: Keys.isAutoUpdate.rawValue)
        }
        
        set {
            setValue(newValue, forKey: Keys.isAutoUpdate.rawValue)
        }
    }
    
    var playerSettings: UserDefaultPlayerSettings? {
        get {
            guard let data = data(forKey: Keys.playerSettings.rawValue),
                  let settings = try? JSONDecoder().decode(UserDefaultPlayerSettings.self, from: data) else {
                return nil
            }
            return settings
        }
        
        set {
            guard let data = try? JSONEncoder().encode(newValue) else { return }
            setValue(data, forKey: Keys.playerSettings.rawValue)
        }
    }
    
    private var currentTotalVideoPlayed: Int {
        get {
            return integer(forKey: Keys.currentTotalVideoPlayed.rawValue)
        }
        
        set {
            setValue(newValue, forKey: Keys.currentTotalVideoPlayed.rawValue)
        }
    }
    
    var isExplicitFreeVideoPlayTimes: Bool {
        return currentTotalVideoPlayed > 0 && currentTotalVideoPlayed % maximumTotalVideoDisplayedBeforeShowAdmob == 0
    }
    
    var isShouldShowRating: Bool {
        return currentTotalVideoPlayed > maximumTotalVideoDisplayedBeforeShowRating
    }
    
    func updateVideoPlayedTimes() {
        
        currentTotalVideoPlayed = currentTotalVideoPlayed + 1
    }
    
    func resetVideoPlayedTimes() {
        currentTotalVideoPlayed = 1
    }
    
    var suggestedQuereis: [String] {
        get {
            guard let data = data(forKey: Keys.suggestedQuereis.rawValue),
                  let suggestions = try? JSONDecoder().decode([String].self, from: data) else {
                return []
            }
            return suggestions
        }
        
        set {
            guard let data = try? JSONEncoder().encode(newValue) else { return }
            setValue(data, forKey: Keys.suggestedQuereis.rawValue)
        }
    }
    
    
    func refresCurrenthAPIKey() {
        guard let currentAPIKey = apiKeyInfo else { return }
        var newAPIKey = currentAPIKey
        newAPIKey.refreshAPIKey()
        apiKeyInfo = newAPIKey
    }
    
    func goToNextAPIKey() {
        var newAPIKey = apiKeyInfo
        newAPIKey?.goToNextApiKey()
        apiKeyInfo = newAPIKey
    }
}
private let maximumTotalVideoDisplayedBeforeShowAdmob: Int = 5
private let maximumTotalVideoDisplayedBeforeShowRating: Int = 25

struct UserDefaultPlayerSettings: Codable {
    
    var isShuffle: BoolEnum = .off
    var isRepeated: BoolEnum = .off
    var isAutoPlay: BoolEnum = .off
    
    init() {}
    
    private enum CodingKeys: String, CodingKey {
        case isShuffle
        case isRepeated
        case isAutoPlay
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(isShuffle.value, forKey: .isShuffle)
        try container.encode(isRepeated.value, forKey: .isRepeated)
        try container.encode(isAutoPlay.value, forKey: .isAutoPlay)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        isShuffle = BoolEnum(from: (try? container.decode(Bool.self, forKey: .isShuffle)).or(false))
        isRepeated = BoolEnum(from: (try? container.decode(Bool.self, forKey: .isRepeated)).or(false))
        isAutoPlay = BoolEnum(from: (try? container.decode(Bool.self, forKey: .isAutoPlay)).or(false))
    }
    
    mutating func toggleIsShuffle() {
        
        if isShuffle.isOff, isRepeated.isOn {
            isRepeated = .off
        }
        isShuffle.switch()
    }
    
    mutating func toggleIsRepeated() {
        if isRepeated.isOff, isShuffle.isOn {
            isShuffle = .off
        }
        isRepeated.switch()
    }
    
    mutating func toggleAutoPlay() {
        isAutoPlay.switch()
    }
}
