//
//  Logger.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Foundation


protocol Loggable {
    func error(_ message: String, file: String, line: Int?)
    func succees(_ message: String, file: String, line: Int?)
    func warning(_ message: String, file: String, Line: Int?)
}

class ColorfulLoger: Loggable {
    
    static let logger = ColorfulLoger()
    
    func error(_ message: String, file: String, line: Int?) {
    }
    
    func succees(_ message: String, file: String, line: Int?) {
        debugPrint("Message :\(message)")
    }
    
    func warning(_ message: String, file: String, Line: Int?) {
    }
}

class FireBaseEventLogger: Loggable {
    func error(_ message: String, file: String, line: Int?) {
    }
    
    func succees(_ message: String, file: String, line: Int?) {
    }
    
    func warning(_ message: String, file: String, Line: Int?) {
    }
}
