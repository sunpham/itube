//
//  Thumbnails.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift

class CodableThumbnails: Object, Codable, IdentifiableObject {
    
    @objc dynamic var id: String = ""
    
    @objc dynamic var defaultThumbnail: CodableThumbnail?
    @objc dynamic var medium: CodableThumbnail?
    @objc dynamic var high: CodableThumbnail?
    @objc dynamic var standard: CodableThumbnail?
    @objc dynamic var maxres: CodableThumbnail?
    
    enum CodingKeys: String, CodingKey {
        case defaultThumbnail = "default"
        case  medium, high, standard, maxres, id
    }
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        defaultThumbnail = (try? container.decode(CodableThumbnail.self, forKey: .defaultThumbnail)).or(nil)
        medium = (try? container.decode(CodableThumbnail.self, forKey: .medium)).or(nil)
        high = (try? container.decode(CodableThumbnail.self, forKey: .high)).or(nil)
        standard = (try? container.decode(CodableThumbnail.self, forKey: .standard)).or(nil)
        maxres = (try? container.decode(CodableThumbnail.self, forKey: .maxres)).or(nil)
    }
    
    func map() -> YoutubeVideo.Thumbnails {
        return .init(
            id: id,
            default: defaultThumbnail?.map(),
            medium: medium?.map(),
            high: high?.map(),
            standard: standard?.map(),
            maxres: maxres?.map())
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CodableThumbnails: CodableObjectCovertable {
    
    typealias SourceElementType = YoutubeVideo.Thumbnails
    typealias DestinationElementType = CodableThumbnails
    
    func from(_ model: SourceElementType?) -> DestinationElementType? {
        guard let model = model else { return nil }
        self.id = model.id
        self.defaultThumbnail = CodableThumbnail().from(model.default)
        self.medium = CodableThumbnail().from(model.medium)
        self.high = CodableThumbnail().from(model.high)
        self.standard = CodableThumbnail().from(model.standard)
        self.maxres = CodableThumbnail().from(model.maxres)
        return self
    }
}

extension YoutubeVideo {
    struct Thumbnails {
        let id: String
        let `default`: Thumbnail?
        let medium: Thumbnail?
        let high: Thumbnail?
        let standard: Thumbnail?
        let maxres: Thumbnail?
        
        func getThumbnailUrlString() -> String {
            let urlString = [
                high, `default`, medium, standard, maxres
            ]
            .compactMap { $0 }
            .first?
            .url
            
            guard let url = urlString else { return "" }
            return url
        }
    }
}
