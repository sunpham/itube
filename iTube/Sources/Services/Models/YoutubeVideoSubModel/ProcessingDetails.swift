//
//  ProcessingDetails.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableProcessingDetails: Codable {
        let processingStatus: String
        let processingProgress: CodableProcessingProgress?
        let processingFailureReason: String
        let fileDetailsAvailability: String
        let processingIssuesAvailability: String
        let tagSuggestionsAvailability: String
        let editorSuggestionsAvailability: String
        let thumbnailsAvailability: String
        
        enum CodingKeys: String, CodingKey {
            case processingStatus, processingProgress, processingFailureReason
            case fileDetailsAvailability, processingIssuesAvailability, tagSuggestionsAvailability
            case editorSuggestionsAvailability, thumbnailsAvailability
        }
        
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            processingStatus = (try? conatiner.decode(String.self, forKey: .processingStatus)).or("")
            processingProgress = (try? conatiner.decode(CodableProcessingProgress.self, forKey: .processingProgress)).or(nil)
            processingFailureReason = (try? conatiner.decode(String.self, forKey: .processingFailureReason)).or("")
            fileDetailsAvailability = (try? conatiner.decode(String.self, forKey: .fileDetailsAvailability)).or("")
            processingIssuesAvailability = (try? conatiner.decode(String.self, forKey: .processingIssuesAvailability)).or("")
            tagSuggestionsAvailability = (try? conatiner.decode(String.self, forKey: .tagSuggestionsAvailability)).or("")
            editorSuggestionsAvailability = (try? conatiner.decode(String.self, forKey: .editorSuggestionsAvailability)).or("")
            thumbnailsAvailability = (try? conatiner.decode(String.self, forKey: .thumbnailsAvailability)).or("")
        }
        
        func map() -> YoutubeVideo.ProcessingDetails {
            return .init(processingStatus: processingStatus,
                         processingProgress: processingProgress?.map(),
                         processingFailureReason: processingFailureReason,
                         fileDetailsAvailability: fileDetailsAvailability,
                         processingIssuesAvailability: processingIssuesAvailability,
                         tagSuggestionsAvailability: tagSuggestionsAvailability,
                         editorSuggestionsAvailability: editorSuggestionsAvailability,
                         thumbnailsAvailability: thumbnailsAvailability)
        }
    }
    
    struct CodableProcessingProgress: Codable {
        let partsTotal: UInt64
        let partsProcessed: UInt64
        let timeLeftMs: UInt64
        
        enum CodingKeys: String, CodingKey {
            case partsTotal, partsProcessed, timeLeftMs
        }
        
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            partsTotal = (try? conatiner.decode(UInt64.self, forKey: .partsTotal)).or(.zero)
            partsProcessed = (try? conatiner.decode(UInt64.self, forKey: .partsProcessed)).or(.zero)
            timeLeftMs = (try? conatiner.decode(UInt64.self, forKey: .timeLeftMs)).or(.zero)
        }
        
        func map() -> YoutubeVideo.ProcessingProgress {
            return .init(partsTotal: partsTotal, partsProcessed: partsProcessed, timeLeftMs: timeLeftMs)
        }
    }
}

extension YoutubeVideo {
    struct ProcessingDetails {
        let processingStatus: String
        let processingProgress: ProcessingProgress?
        let processingFailureReason: String
        let fileDetailsAvailability: String
        let processingIssuesAvailability: String
        let tagSuggestionsAvailability: String
        let editorSuggestionsAvailability: String
        let thumbnailsAvailability: String
    }
    
    struct ProcessingProgress {
        let partsTotal: UInt64
        let partsProcessed: UInt64
        let timeLeftMs: UInt64
    }
}
