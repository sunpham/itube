//
//  FileDetails.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableFileDetails: Codable {
        let fileName: String
        let fileSize: Int64
        let fileType: String
        let container: String
        let videoStreams: [CodableVideoStream]
        let audioStreams: [CodableAudioStream]
        let durationMs: UInt64
        let bitrateBps: UInt64
        let creationTime: String
        
        enum CodingKeys: String, CodingKey {
            case fileName, fileSize, fileType
            case container, videoStreams, audioStreams
            case durationMs, bitrateBps, creationTime
        }
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            fileName = (try? conatiner.decode(String.self, forKey: .fileName)).or("")
            fileSize = (try? conatiner.decode(Int64.self, forKey: .fileSize)).or(.zero)
            fileType = (try? conatiner.decode(String.self, forKey: .fileType)).or("")
            container = (try? conatiner.decode(String.self, forKey: .container)).or("")
            videoStreams = (try? conatiner.decode([CodableVideoStream].self, forKey: .videoStreams)).or([])
            audioStreams = (try? conatiner.decode([CodableAudioStream].self, forKey: .audioStreams)).or([])
            durationMs = (try? conatiner.decode(UInt64.self, forKey: .durationMs)).or(.zero)
            bitrateBps = (try? conatiner.decode(UInt64.self, forKey: .bitrateBps)).or(.zero)
            creationTime = (try? conatiner.decode(String.self, forKey: .creationTime)).or("")
        }
        
        func map() -> YoutubeVideo.FileDetails {
            return .init(fileName: fileName,
                         fileSize: fileSize,
                         fileType: fileType,
                         container: container,
                         videoStreams: videoStreams.map { $0.map() },
                         audioStreams: audioStreams.map { $0.map() },
                         durationMs: durationMs,
                         bitrateBps: bitrateBps,
                         creationTime: creationTime)
        }
    }
    
    struct CodableVideoStream: Codable {
        let widthPixedls: UInt
        let heightPixels: UInt
        let frameRateFps: Double
        let aspectRatio: Double
        let codec: String
        let bitrateBPs: String
        let rotation: String
        let vendor: String
        
        enum CodingKeys: String, CodingKey {
            case widthPixedls, heightPixels, frameRateFps
            case aspectRatio, codec, bitrateBPs
            case rotation, vendor
        }
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            widthPixedls = (try? conatiner.decode(UInt.self, forKey: .widthPixedls)).or(.zero)
            heightPixels = (try? conatiner.decode(UInt.self, forKey: .heightPixels)).or(.zero)
            frameRateFps = (try? conatiner.decode(Double.self, forKey: .frameRateFps)).or(.zero)
            aspectRatio = (try? conatiner.decode(Double.self, forKey: .frameRateFps)).or(.zero)
            codec = (try? conatiner.decode(String.self, forKey: .codec)).or("")
            bitrateBPs = (try? conatiner.decode(String.self, forKey: .codec)).or("")
            rotation = (try? conatiner.decode(String.self, forKey: .codec)).or("")
            vendor = (try? conatiner.decode(String.self, forKey: .codec)).or("")
        }
        
        func map() -> YoutubeVideo.VideoStream {
            return .init(widthPixedls: widthPixedls,
                         heightPixels: heightPixels,
                         frameRateFps: frameRateFps,
                         aspectRatio: aspectRatio,
                         codec: codec,
                         bitrateBPs: bitrateBPs,
                         rotation: rotation,
                         vendor: vendor)
        }
        
    }
    
    struct CodableAudioStream: Codable {
        let channelCount: UInt
        let codec: String
        let bitratBps: UInt64
        let vendor: String
        
        enum CodingKeys: String, CodingKey {
            case channelCount, codec, bitratBps
            case vendor
        }
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            channelCount = (try? conatiner.decode(UInt.self, forKey: .channelCount)).or(.zero)
            codec = (try? conatiner.decode(String.self, forKey: .codec)).or("")
            bitratBps = (try? conatiner.decode(UInt64.self, forKey: .bitratBps)).or(.zero)
            vendor = (try? conatiner.decode(String.self, forKey: .vendor)).or("")
        }
        
        func map() -> YoutubeVideo.AudioStream {
            return .init(channelCount: channelCount,
                         codec: codec,
                         bitratBps: bitratBps,
                         vendor: vendor)
        }
    }
}
extension YoutubeVideo {
    struct FileDetails {
        let fileName: String
        let fileSize: Int64
        let fileType: String
        let container: String
        let videoStreams: [VideoStream]
        let audioStreams: [AudioStream]
        let durationMs: UInt64
        let bitrateBps: UInt64
        let creationTime: String
    }
}

extension YoutubeVideo {
    struct VideoStream {
        let widthPixedls: UInt
        let heightPixels: UInt
        let frameRateFps: Double
        let aspectRatio: Double
        let codec: String
        let bitrateBPs: String
        let rotation: String
        let vendor: String
    }
    
    struct AudioStream {
        let channelCount: UInt
        let codec: String
        let bitratBps: UInt64
        let vendor: String
    }
}
