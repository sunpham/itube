//
//  Suggestions.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableSuggestions: Codable {
        let processingErrors: [String]
        let processingWarnings: [String]
        let processingHints: [String]
        let tagSuggestions: [CodableTagSuggestion]
        let editorSuggestions: [String]
        
        enum CodingKeys: String, CodingKey {
            case processingErrors, processingWarnings, processingHints
            case tagSuggestions, editorSuggestions
        }
        
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            processingErrors = (try? conatiner.decode([String].self, forKey: .processingErrors)).or([])
            processingWarnings = (try? conatiner.decode([String].self, forKey: .processingWarnings)).or([])
            processingHints = (try? conatiner.decode([String].self, forKey: .processingHints)).or([])
            tagSuggestions = (try? conatiner.decode([CodableTagSuggestion].self, forKey: .tagSuggestions)).or([])
            editorSuggestions = (try? conatiner.decode([String].self, forKey: .editorSuggestions)).or([])
        }
        
        func map() -> YoutubeVideo.Suggestions {
            return .init(processingErrors: processingErrors,
                         processingWarnings: processingWarnings,
                         processingHints: processingHints,
                         tagSuggestions: tagSuggestions.map { $0.map() },
                         editorSuggestions: editorSuggestions)
        }
     }
    
    struct CodableTagSuggestion: Codable {
        let tag: String
        let categoryRestricts: [String]
        
        enum CodingKeys: String, CodingKey {
            case tag, categoryRestricts
        }
        
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            tag = (try? conatiner.decode(String.self, forKey: .tag)).or("")
            categoryRestricts = (try? conatiner.decode([String].self, forKey: .categoryRestricts)).or([])
        }
        
        func map() -> YoutubeVideo.TagSuggestion {
            return .init(tag: tag, categoryRestricts: categoryRestricts)
        }
    }
}
extension YoutubeVideo {
    
    struct Suggestions {
        let processingErrors: [String]
        let processingWarnings: [String]
        let processingHints: [String]
        let tagSuggestions: [TagSuggestion]
        let editorSuggestions: [String]
    }
    
    struct TagSuggestion {
        let tag: String
        let categoryRestricts: [String]
    }
}
