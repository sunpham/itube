//
//  RecordingDetails.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableRecordingDetails: Codable {
        let recordingDate: Date?
        
        enum CodingKeys: String, CodingKey {
            case recordingDate
        }
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            recordingDate = (try? conatiner.decode(Date.self, forKey: .recordingDate)).or(nil)
        }
        
        func map() -> YoutubeVideo.RecordingDetails {
            return .init(recordingDate: recordingDate)
        }
    }
}
extension YoutubeVideo {
    struct RecordingDetails {
        let recordingDate: Date?
    }
}
