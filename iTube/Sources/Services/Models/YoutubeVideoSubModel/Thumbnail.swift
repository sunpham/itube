//
//  Thumbnail.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift

protocol CodableObjectCovertable {
    
    associatedtype SourceElementType
    associatedtype DestinationElementType
    func from(_ model: SourceElementType?) -> DestinationElementType?
}

class CodableThumbnail: Object, Codable, DataModel{
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    @objc dynamic var id: String = ""
    
    @objc dynamic var url: String = ""
    @objc dynamic var width: Int = 0
    @objc dynamic var height: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case url, width, height, id
    }
    
    override init() {
        super.init()
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        url = (try? container.decode(String.self, forKey: .url)).or("")
        width = (try? container.decode(Int.self, forKey: .width)).or(.zero)
        height = (try? container.decode(Int.self, forKey: .height)).or(.zero)
    }
    
    func map() -> YoutubeVideo.Thumbnail {
        return .init(id: id, url: url, width: width, height: height)
    }
}

extension CodableThumbnail: CodableObjectCovertable {
    func from(_ model:  SourceElementType?) -> DestinationElementType? {
        guard let model = model else { return nil }
        id = model.id
        url = model.url
        width = model.width
        height = model.height
        return self
    }
    typealias SourceElementType = YoutubeVideo.Thumbnail
    typealias DestinationElementType = CodableThumbnail

}
extension YoutubeVideo {
    struct Thumbnail {
        let id: String
        let url: String
        let width: Int
        let height: Int
    }
}
