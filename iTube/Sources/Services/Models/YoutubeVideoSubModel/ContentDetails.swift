//
//  ContentDetails.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift

class CodableContentDetails: Object, Codable, DataModel {
    
    @objc dynamic var id: String = ""
    @objc dynamic var duration: String = ""
    @objc dynamic var dimension: String = ""
    @objc dynamic var definition: String = ""
    @objc dynamic var caption: String = ""
    @objc dynamic var licensedContent: String = ""
    @objc dynamic var regionRestriction: CodableRegoinRestriction?
    @objc dynamic var contentRating: CodableContentRating?
    @objc dynamic var projection: String = ""
    @objc dynamic var hasCustomThumbnail: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case id
        case duration, dimension, definition
        case caption, licensedContent, regionRestriction
        case contentRating, projection, hasCustomThumbnail
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        duration = (try? container.decode(String.self, forKey: .duration)).or("")
        dimension = (try? container.decode(String.self, forKey: .dimension)).or("")
        definition = (try? container.decode(String.self, forKey: .definition)).or("")
        caption = (try? container.decode(String.self, forKey: .caption)).or("")
        licensedContent = (try? container.decode(String.self, forKey: .licensedContent)).or("")
        regionRestriction = (try? container.decode(CodableRegoinRestriction.self, forKey: .regionRestriction)).or(nil)
        contentRating = (try? container.decode(CodableContentRating.self, forKey: .contentRating)).or(nil)
        projection = (try? container.decode(String.self, forKey: .projection)).or("")
        hasCustomThumbnail = (try? container.decode(Bool.self, forKey: .hasCustomThumbnail)).or(false)
    }
    
    func map() -> YoutubeVideo.ContentDetails {
        return .init(
            id: id,
            duration: duration,
            dimension: dimension,
            definition: definition,
            caption: caption,
            licensedContent: licensedContent,
            regionRestriction: regionRestriction?.map(),
            contentRating: contentRating?.map(),
            projection: projection,
            hasCustomThumbnail: hasCustomThumbnail)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CodableContentDetails: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo.ContentDetails
    typealias DestinationElementType = CodableContentDetails
    
    func from(_ model: YoutubeVideo.ContentDetails?) -> CodableContentDetails? {
        guard let model = model else { return nil }
        self.id = model.id
        self.duration = model.duration
        self.dimension = model.definition
        self.caption = model.caption
        self.licensedContent = model.licensedContent
        self.regionRestriction = CodableRegoinRestriction().from(model.regionRestriction)
        self.contentRating = CodableContentRating().from(model.contentRating)
        self.projection = model.projection
        self.hasCustomThumbnail = model.hasCustomThumbnail
        return self
    }
}

class CodableContentRating: Object, Codable, DataModel {
    @objc dynamic var id: String = ""
    @objc dynamic var content: String = ""
    
    enum CodingKeys: String, CodingKey {
        case content, id
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        content = (try? container.decode(String.self, forKey: .content)).or("")
    }
    
    func map() -> YoutubeVideo.ContentRating {
        return .init(id: id, content: content)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CodableContentRating: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo.ContentRating
    typealias DestinationElementType = CodableContentRating
    
    func from(_ model: YoutubeVideo.ContentRating?) -> CodableContentRating? {
        guard let model = model else { return nil }
        self.id = model.id
        self.content = model.content
        return self
    }
}

extension YoutubeVideo {
    struct ContentDetails {
        let id: String
        let duration: String
        let dimension: String
        let definition: String
        let caption: String
        let licensedContent: String
        let regionRestriction: RegoinRestriction?
        let contentRating: ContentRating?
        let projection: String
        let hasCustomThumbnail: Bool
    }
}

extension YoutubeVideo {
    struct ContentRating {
        let id: String
        let content: String
    }
}
