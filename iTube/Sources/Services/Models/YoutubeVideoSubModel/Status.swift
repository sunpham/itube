//
//  Status.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableStatus: Codable {
        let uploadStatus: String
        let failureReason: String
        let rejectionReason: String
        let privacyStatus: String
        let license: String
        let publicStatsViewable: Bool
        let madeForKids: Bool
        let selfDeclareMadeForKids: Bool
        
        enum CodingKeys: String, CodingKey {
            case uploadStatus, failureReason, rejectionReason
            case privacyStatus, license, publicStatsViewable
            case madeForKids, selfDeclareMadeForKids
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            uploadStatus = (try? container.decode(String.self, forKey: .uploadStatus)).or("")
            failureReason = (try? container.decode(String.self, forKey: .failureReason)).or("")
            rejectionReason = (try? container.decode(String.self, forKey: .rejectionReason)).or("")
            privacyStatus = (try? container.decode(String.self, forKey: .privacyStatus)).or("")
            license = (try? container.decode(String.self, forKey: .license)).or("")
            publicStatsViewable = (try? container.decode(Bool.self, forKey: .publicStatsViewable)).or(false)
            madeForKids = (try? container.decode(Bool.self, forKey: .madeForKids)).or(false)
            selfDeclareMadeForKids = (try? container.decode(Bool.self, forKey: .selfDeclareMadeForKids)).or(false)
        }
        
        func map() -> YoutubeVideo.Status {
            return .init(uploadStatus: uploadStatus,
                         failureReason: failureReason,
                         rejectionReason: rejectionReason,
                         privacyStatus: privacyStatus,
                         license: license,
                         publicStatsViewable: publicStatsViewable,
                         madeForKids: madeForKids,
                         selfDeclareMadeForKids: selfDeclareMadeForKids)
        }
    }
}

extension YoutubeVideo {
    struct Status {
        
        let uploadStatus: String
        let failureReason: String
        let rejectionReason: String
        let privacyStatus: String
        let license: String
        let publicStatsViewable: Bool
        let madeForKids: Bool
        let selfDeclareMadeForKids: Bool
    }
}
