//
//  Snippet.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift

class CodableSnippetResourceId: Object, Codable, DataModel {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var kind: String = ""
    @objc dynamic var videoId: String = ""
    
    enum CodingKeys: String, CodingKey {
        case id
        case kind
        case videoId
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id  = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        kind  = (try? container.decode(String.self, forKey: .kind)).or("")
        videoId  = (try? container.decode(String.self, forKey: .videoId)).or("")
    }
    
    func map() -> YoutubeVideo.Snippet.ResourceId {
        return YoutubeVideo.Snippet.ResourceId(id: id, kind: kind, videoId: videoId)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CodableSnippetResourceId: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo.Snippet.ResourceId
    typealias DestinationElementType = CodableSnippetResourceId
    
    func from(_ model: YoutubeVideo.Snippet.ResourceId?) -> CodableSnippetResourceId? {
        guard let model = model else { return nil }
        id = model.id
        kind = model.kind
        videoId = model.videoId
        return self
    }
}

class CodableSnippet: Object, Codable, DataModel {
    @objc dynamic var id: String = ""
    
    @objc dynamic var publishedAt: Date = Date()
    @objc dynamic var channelId: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var snipetDescription: String = ""
    @objc dynamic var thumbnails: CodableThumbnails?
    @objc dynamic var channelTitle: String = ""
    var tags: List<String> = List()
    @objc dynamic var categoryId: String = ""
    @objc dynamic var liveBroadcastContent: String = ""
    @objc dynamic var defaultLanguge: String = ""
    var localized: CodableYotubeVideo.CodableLocalized?
    @objc dynamic var defaultAudioLanguage: String = ""
    @objc dynamic var resourceId: CodableSnippetResourceId?
    @objc dynamic var playlistId: String = ""
    
    enum CodingKeys: String, CodingKey {
        case publishedAt, channelId, title, id
        case thumbnails, channelTitle
        case tags, categoryId, liveBroadcastContent
        case defaultLanguge, localized, defaultAudioLanguage
        case snipetDescription = "description"
        case resourceId
        case playlistId
    }
    
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id  = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        publishedAt = (try? container.decode(Date.self, forKey: .publishedAt)).or(Date())
        channelId = (try? container.decode(String.self, forKey: .channelId)).or("")
        title = (try? container.decode(String.self, forKey: .title)).or("")
        snipetDescription = (try? container.decode(String.self, forKey: .snipetDescription)).or("")
        thumbnails = (try? container.decode(CodableThumbnails.self, forKey: .thumbnails)).or(nil)
        channelTitle = (try? container.decode(String.self, forKey: .channelTitle)).or("")
       
        let _tags = (try? container.decode([String].self, forKey: .tags)).or([])
        for tag in _tags {
            tags.append(tag)
        }
        
        categoryId = (try? container.decode(String.self, forKey: .liveBroadcastContent)).or("")
        liveBroadcastContent = (try? container.decode(String.self, forKey: .liveBroadcastContent)).or("")
        defaultLanguge = (try? container.decode(String.self, forKey: .defaultLanguge)).or("")
        localized = (try? container.decode(CodableYotubeVideo.CodableLocalized.self, forKey: .localized)).or(nil)
        defaultAudioLanguage = (try? container.decode(String.self, forKey: .defaultAudioLanguage)).or("")
        resourceId = (try? container.decode(CodableSnippetResourceId.self, forKey: .resourceId)).or(nil)
        playlistId = (try? container.decode(String.self, forKey: .playlistId)).or("")
    }
    
    func map() -> YoutubeVideo.Snippet {
        return .init(id: id,
                     publishedAt: publishedAt,
                     channelId: channelId,
                     title: title,
                     description: snipetDescription,
                     thumbnails: thumbnails?.map() ,
                     channelTitle: channelTitle,
                     tags: tags.map { $0 },
                     categoryId: categoryId,
                     liveBroadcastContent: liveBroadcastContent,
                     defaultLanguge: defaultLanguge,
                     localized: localized?.map(),
                     defaultAudioLanguage: defaultAudioLanguage,
                     resourceId: resourceId?.map(),
                     playlistId: playlistId)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
extension CodableSnippet: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo.Snippet
    typealias DestinationElementType = CodableSnippet
    
    func from(_ model: YoutubeVideo.Snippet?) -> CodableSnippet? {
        guard let model = model else { return nil }
        id = model.id
        publishedAt = model.publishedAt
        channelId = model.channelId
        title = model.title
        snipetDescription = model.description
        thumbnails = CodableThumbnails().from(model.thumbnails)
        channelTitle = model.channelTitle
        for tag in model.tags {
            tags.append(tag)
        }
        categoryId = model.categoryId
        liveBroadcastContent = model.liveBroadcastContent
        defaultLanguge = model.defaultLanguge
        localized = nil
        defaultLanguge = model.defaultLanguge
        resourceId = CodableSnippetResourceId().from(model.resourceId)
        playlistId = model.playlistId
        return self
    }
}

extension YoutubeVideo {
    struct Snippet {
        let id: String
        let publishedAt: Date
        let channelId: String
        let title: String
        let description: String
        let thumbnails: Thumbnails?
        let channelTitle: String
        let tags: [String]
        let categoryId: String
        let liveBroadcastContent: String
        let defaultLanguge: String
        let localized: Localized?
        let defaultAudioLanguage: String
        let resourceId: ResourceId?
        let playlistId: String
    }
}

extension YoutubeVideo.Snippet {
    struct ResourceId {
        let id: String
        let kind: String
        let videoId: String
    }
}
