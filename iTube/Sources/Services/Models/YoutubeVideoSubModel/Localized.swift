//
//  Localized.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableLocalized: Codable {
        let title: String
        let description: String
        
        enum CodingKeys: String, CodingKey {
            case title, description
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            title = (try? container.decode(String.self, forKey: .title)).or("")
            description = (try? container.decode(String.self, forKey: .description)).or("")
        }
        
        func map() -> YoutubeVideo.Localized {
            return .init(title: title, description: description)
        }
    }
}
extension YoutubeVideo {
    struct Localized {
        let title: String
        let description: String
    }
}
