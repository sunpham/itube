//
//  Statistics.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift

class CodableStatistics: Object, Codable, DataModel {
    
    @objc dynamic var  id: String = ""
    @objc dynamic var viewCount: Int = .zero
    @objc dynamic var likeCount: Int = .zero
    @objc dynamic var dislikeCount: Int = .zero
    @objc dynamic var favoriteCount: Int = .zero
    @objc dynamic var commentCount: Int = .zero
    
    enum CodingKeys: String, CodingKey {
        case id
        case viewCount, likeCount, dislikeCount
        case favoriteCount, commentCount
    }
    
    required convenience init(from decoder: Decoder) throws {
        
        self.init()
        
        let conatiner = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? conatiner.decode(String.self, forKey: .id)).or("")
        viewCount = (try? conatiner.decode(Int.self, forKey: .viewCount)).or(.zero)
        likeCount = (try? conatiner.decode(Int.self, forKey: .likeCount)).or(.zero)
        dislikeCount = (try? conatiner.decode(Int.self, forKey: .dislikeCount)).or(.zero)
        favoriteCount = (try? conatiner.decode(Int.self, forKey: .favoriteCount)).or(.zero)
        commentCount = (try? conatiner.decode(Int.self, forKey: .commentCount)).or(.zero)
    }
    
    func map() -> YoutubeVideo.Statistics {
        return .init(id: id,
                     viewCount: viewCount,
                     likeCount: likeCount,
                     dislikeCount: dislikeCount,
                     favoriteCount: favoriteCount,
                     commentCount: commentCount)
    }
}

extension CodableStatistics: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo.Statistics
    typealias DestinationElementType = CodableStatistics
    
    func from(_ model: YoutubeVideo.Statistics?) -> CodableStatistics? {
        guard let model = model else { return nil }
        id = model.id
        viewCount = model.viewCount
        likeCount = model.likeCount
        dislikeCount = model.dislikeCount
        favoriteCount = model.favoriteCount
        commentCount = model.commentCount
        return self
    }
}

extension YoutubeVideo {
    struct Statistics {
        
        let id: String
        let viewCount: Int
        let likeCount: Int
        let dislikeCount: Int
        let favoriteCount: Int
        let commentCount: Int
    }
}
