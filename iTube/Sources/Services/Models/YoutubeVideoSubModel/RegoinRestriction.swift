//
//  RegoinRestriction.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift

class CodableRegoinRestriction: Object, Codable, DataModel {
    
    @objc dynamic var id: String = ""
    var allowed: List<String> = List()
    var blocked: List<String> = List()
    
    enum CodingKeys: String, CodingKey {
        case allowed, blocked, id
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        let _allowed = (try? container.decode([String].self, forKey: .allowed)).or([])
        for item in _allowed {
            allowed.append(item)
        }
        
        let _blocked = (try? container.decode([String].self, forKey: .blocked)).or([])
        for item in _blocked {
            blocked.append(item)
        }
    }
    
    func map() -> YoutubeVideo.RegoinRestriction {
        return .init(id: id, allowed: allowed.map { $0 }, blocked: blocked.map { $0 })
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CodableRegoinRestriction: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo.RegoinRestriction
    typealias DestinationElementType = CodableRegoinRestriction
    
    func from(_ model: YoutubeVideo.RegoinRestriction?) -> CodableRegoinRestriction? {
        guard let model = model else { return nil }
        id = model.id
        for item in model.allowed {
            allowed.append(item)
        }
        
        for item in model.blocked {
            blocked.append(item)
        }
        
        return self
    }
}

extension YoutubeVideo {
    struct RegoinRestriction {
        let id: String
        let allowed: [String]
        let blocked: [String]
    }
}
