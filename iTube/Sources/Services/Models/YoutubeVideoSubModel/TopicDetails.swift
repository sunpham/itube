//
//  TopicDetails.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableTopicDetails: Codable {
        let toppicIds: [ String]
        let relevantTopicIds: [String]
        let topicCategories: [String]
        
        enum CodingKeys: String, CodingKey {
            case toppicIds, relevantTopicIds, topicCategories
        }
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            toppicIds = (try? conatiner.decode([String].self, forKey: .toppicIds)).or([])
            relevantTopicIds = (try? conatiner.decode([String].self, forKey: .toppicIds)).or([])
            topicCategories = (try? conatiner.decode([String].self, forKey: .toppicIds)).or([])
        }
        
        func map() -> YoutubeVideo.TopicDetails {
            return .init(toppicIds: toppicIds,
                         relevantTopicIds: relevantTopicIds,
                         topicCategories: topicCategories)
        }
        
    }
}
extension YoutubeVideo {
    struct TopicDetails {
        let toppicIds: [ String]
        let relevantTopicIds: [String]
        let topicCategories: [String]
        
    }
}
