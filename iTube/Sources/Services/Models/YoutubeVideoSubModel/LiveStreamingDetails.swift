//
//  LiveStreamingDetails.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodableLiveStreamingDetails: Codable {
        let actualStartTime: Date
        let actualEndTime: Date
        let scheduledStartTime: Date
        let scheduledEndTime: Date
        let concurrentViewers: UInt64
        let activeLiveChatId: String
        
        enum CodingKeys: String, CodingKey {
            case actualStartTime, actualEndTime, scheduledStartTime, scheduledEndTime, concurrentViewers, activeLiveChatId
        }
        
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            actualStartTime = (try? conatiner.decode(Date.self, forKey: .actualStartTime)).or(Date())
            actualEndTime = (try? conatiner.decode(Date.self, forKey: .actualEndTime)).or(Date())
            scheduledStartTime = (try? conatiner.decode(Date.self, forKey: .scheduledStartTime)).or(Date())
            scheduledEndTime = (try? conatiner.decode(Date.self, forKey: .scheduledEndTime)).or(Date())
            concurrentViewers = (try? conatiner.decode(UInt64.self, forKey: .concurrentViewers)).or(.zero)
            activeLiveChatId = (try? conatiner.decode(String.self, forKey: .activeLiveChatId)).or("")
        }
        
        func map() -> YoutubeVideo.LiveStreamingDetails {
            return .init(actualStartTime: actualStartTime,
                         actualEndTime: actualEndTime,
                         scheduledStartTime: scheduledStartTime,
                         scheduledEndTime: scheduledEndTime,
                         concurrentViewers: concurrentViewers,
                         activeLiveChatId: activeLiveChatId)
        }
    }
}
extension YoutubeVideo {
    struct LiveStreamingDetails {
        let actualStartTime: Date
        let actualEndTime: Date
        let scheduledStartTime: Date
        let scheduledEndTime: Date
        let concurrentViewers: UInt64
        let activeLiveChatId: String
    }
}

