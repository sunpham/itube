//
//  Player.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

extension CodableYotubeVideo {
    struct CodablePlayer: Codable {
        let embedHtml: String
        let embedHeight: Int64
        let embedWidth: Int64
        
        enum CodingKeys: String, CodingKey {
            case embedHtml, embedHeight, embedWidth
        }
        
        init(from decoder: Decoder) throws {
            let conatiner = try decoder.container(keyedBy: CodingKeys.self)
            embedHtml = (try? conatiner.decode(String.self, forKey: .embedHtml)).or("")
            embedHeight = (try? conatiner.decode(Int64.self, forKey: .embedHeight)).or(.zero)
            embedWidth = (try? conatiner.decode(Int64.self, forKey: .embedWidth)).or(.zero)
        }
        
        func map() -> YoutubeVideo.Player {
            return .init(embedHtml: embedHtml,
                         embedHeight: embedHeight,
                         embedWidth: embedWidth)
        }
    }
}
extension YoutubeVideo {
    struct Player {
        let embedHtml: String
        let embedHeight: Int64
        let embedWidth: Int64
    }
}
