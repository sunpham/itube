//
//  YoutubePagination.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation

import RealmSwift
import Realm

struct YoutubeVideoPagination {
    let kind: String
    let etag: String
    let items: [YoutubeVideo]
    let nextPageToken: String
    let pageInfo: PageInfo?
}

class DeepCopier {
    //Used to expose generic
    static func Copy<T:Codable>(of object:T) -> T?{
       do{
           let json = try JSONEncoder().encode(object)
           return try JSONDecoder().decode(T.self, from: json)
       }
       catch let error{
           print(error)
           return nil
       }
    }
}

protocol YoutubeVideoMapping {
    func map() -> YoutubeVideo
}

protocol Pagable: DataModel {
    associatedtype ItemType: RealmSwift.Object
    
    var nextPageToken: String { get set }
    var id: String { get set }
    var pageInfo: CodablePageInfo? { get set }
    var items: List<ItemType> { get set }
    var kind: String { get set }
    var etag: String { get set }
}

class Pagination<T: DataModel & YoutubeVideoMapping>: Object, Pagable {
    
    typealias ItemType = T
    
    @objc dynamic var id: String = ""
    @objc dynamic var kind: String = ""
    @objc dynamic var etag: String = ""
    var items: List<ItemType> = List()
    @objc dynamic var nextPageToken: String = ""
    @objc dynamic var pageInfo: CodablePageInfo?
    
    enum CodingKeys: String, CodingKey {
        case kind, etag, items, id
        case nextPageToken, pageInfo
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or("")
        kind = (try? container.decode(String.self, forKey: .kind)).or("")
        etag = (try? container.decode(String.self, forKey: .etag)).or("")
        let _items = (try? container.decode([T].self, forKey: .items)).or([])
        for item in _items {
            items.append(item)
        }
        
        nextPageToken = (try? container.decode(String.self, forKey: .nextPageToken)).or("")
        pageInfo = (try? container.decode(CodablePageInfo.self, forKey: .pageInfo)).or(nil)
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func map() -> YoutubeVideoPagination {
        return .init(kind: kind,
                     etag: etag,
                     items: items.map { $0.map() },
                     nextPageToken: nextPageToken,
                     pageInfo: pageInfo?.map())
    }
}

class CodableYoutubeVideoSearchItemPagination: Pagination<CodableYoutubeVideoSearchItem> {}

class CodableYoutubeVideoPagination: Pagination<CodableYotubeVideo> {}
