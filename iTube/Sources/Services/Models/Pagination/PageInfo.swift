//
//  PageInfo.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift
import Realm

struct PageInfo {
    let totalResults: Int
    let resultsPerPage: Int
}


@objcMembers class CodablePageInfo: Object, Codable, DataModel {
    var id = UUID().uuidString
    
    var totalResults: Int = 0
    var resultsPerPage: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case totalResults
        case resultsPerPage
    }
    
    override init() {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        totalResults = (try? container.decode(Int.self, forKey: .totalResults)).or(.zero)
        resultsPerPage = (try? container.decode(Int.self, forKey: .resultsPerPage)).or(.zero)
    }
    
    func map() -> PageInfo {
        return .init(totalResults: totalResults,
                     resultsPerPage: resultsPerPage)
    }
}
