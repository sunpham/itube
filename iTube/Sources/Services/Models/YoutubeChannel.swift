//
//  YoutubeChannel.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/15/21.
//

import Foundation
import RealmSwift

struct YoutubeChannel {
    let kind: String
    let tag: String
    let id: String
    let snippet: YoutubeVideo.Snippet?
    let contentDetails: YoutubeVideo.ContentDetails?
    let statistics: YoutubeVideo.Statistics?
}

class CodableYoutubeChannel: Object, Codable, DataModel {
    @objc dynamic var kind: String = ""
    @objc dynamic var tag: String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var snippet: CodableSnippet?
    @objc dynamic var contentDetails: CodableContentDetails?
    @objc dynamic var statistics: CodableStatistics?
    
    enum CodingKeys: String, CodingKey {
        case kind
        case tag
        case id
        case snippet
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        kind = (try? container.decode(String.self, forKey: .kind)).or("")
        tag = (try? container.decode(String.self, forKey: .tag)).or("")
        id = (try? container.decode(String.self, forKey: .id) ).or("")
        
        snippet = (try? container.decode(CodableSnippet.self, forKey: .snippet)).or(nil)
    }
}

extension CodableYoutubeChannel {
    func map() -> YoutubeChannel {
        return .init(kind: kind,
                     tag: tag,
                     id: id,
                     snippet: snippet?.map(),
                     contentDetails: contentDetails?.map(),
                     statistics: statistics?.map())
        
    }
}

extension CodableYoutubeChannel: CodableObjectCovertable {
    
    typealias SourceElementType = YoutubeChannel
    typealias DestinationElementType = CodableYoutubeChannel
    
    func from(_ model: YoutubeChannel?) -> CodableYoutubeChannel? {
        guard let model = model else { return nil }
        id = model.id
        kind = model.kind
        tag = model.tag
        snippet = CodableSnippet().from(model.snippet)
        return self
    }
    
}
