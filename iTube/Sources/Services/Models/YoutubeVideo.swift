//
//  YoutubeVideo.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import RealmSwift
import Realm

extension CodableYotubeVideo: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo
    typealias DestinationElementType = CodableYotubeVideo
    
    func from(_ model: YoutubeVideo?) -> CodableYotubeVideo? {
        guard let model = model else { return nil }
        kind = model.kind
        etag = model.etag
        id = model.id
        isFavourite = model.isFavourite
        snippet = CodableSnippet().from(model.snippet)
        contentDetails = CodableContentDetails().from(model.contentDetails)
        statistics = CodableStatistics().from(model.statistics)
        return self
    }
}

class CodableYotubeVideo: Object, Codable, DataModel, YoutubeVideoMapping {
    
    @objc dynamic var kind: String = ""
    @objc dynamic var etag: String = ""
    @objc dynamic var id: String = ""
    @objc dynamic var isFavourite: Bool = false
    
    @objc dynamic var snippet: CodableSnippet?
    @objc dynamic var contentDetails: CodableContentDetails?
    var status: CodableStatus?
    @objc dynamic var statistics: CodableStatistics?
    var player: CodablePlayer?
    var topicDetails: CodableTopicDetails?
    var recordingDetails:  CodableRecordingDetails?
    var fileDetails: CodableFileDetails?
    var processingDetails: CodableProcessingDetails?
    var suggestions: CodableSuggestions?
    var liveStreamingDetails: CodableLiveStreamingDetails?
    var localizations: [String: CodableLocalized] = [:]
    
    enum CodingKeys: String, CodingKey {
        case kind, etag, id
        case snippet, contentDetails, status
        case statistics, player, topicDetails
        case recordingDetails, fileDetails, processingDetails
        case suggestions, liveStreamingDetails, localizations
        case isFavourite
    }
    
    override init() {}
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        kind = (try? container.decode(String.self, forKey: .kind)).or("")
        etag = (try? container.decode(String.self, forKey: .etag)).or("")
        id = (try? container.decode(String.self, forKey: .id)).or("")
        
        snippet = (try? container.decode(CodableSnippet.self, forKey: .snippet)).or(nil)
        contentDetails = (try? container.decode(CodableContentDetails.self, forKey: .contentDetails)).or(nil)
        status = (try? container.decode(CodableStatus.self, forKey: .status)).or(nil)
        statistics = (try? container.decode(CodableStatistics.self, forKey: .statistics)).or(nil)
        player = (try? container.decode(CodablePlayer.self, forKey: .player)).or(nil)
        topicDetails = (try? container.decode(CodableTopicDetails.self, forKey: .topicDetails)).or(nil)
        recordingDetails = (try? container.decode(CodableRecordingDetails.self, forKey: .recordingDetails)).or(nil)
        fileDetails = (try? container.decode(CodableFileDetails.self, forKey: .fileDetails)).or(nil)
        processingDetails = (try? container.decode(CodableProcessingDetails.self, forKey: .processingDetails)).or(nil)
        suggestions = (try? container.decode(CodableSuggestions.self, forKey: .suggestions)).or(nil)
        liveStreamingDetails = (try? container.decode(CodableLiveStreamingDetails.self, forKey: .liveStreamingDetails)).or(nil)
        localizations = (try? container.decode([String: CodableLocalized].self, forKey: .localizations)).or([:])
        isFavourite = (try? container.decode(Bool.self, forKey: .isFavourite)).or(false)
    }
    
    func map() -> YoutubeVideo {
        return .init(kind: kind,
                     etag: etag,
                     id: id,
                     isFavourite: isFavourite,
                     snippet: snippet?.map(),
                     contentDetails: contentDetails?.map(),
                     status: status?.map(),
                     statistics: statistics?.map(),
                     player: player?.map(),
                     topicDetails: topicDetails?.map(),
                     recordingDetails: recordingDetails?.map(),
                     fileDetails: fileDetails?.map(),
                     processingDetails: processingDetails?.map(),
                     suggestions: suggestions?.map(),
                     liveStreamingDetails: liveStreamingDetails?.map(),
                     localizations: mapLocalizations())
    }
    
    func mapLocalizations() -> [String: YoutubeVideo.Localized] {
        var result = [String: YoutubeVideo.Localized]()
        localizations.forEach { key, value in
            result[key] = value.map()
        }
        
        return result
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}


struct YoutubeVideo {
    let kind: String
    let etag: String
    let id: String
    var isFavourite: Bool
    
    let snippet: Snippet?
    let contentDetails: ContentDetails?
    let status: Status?
    let statistics: Statistics?
    let player: Player?
    let topicDetails: TopicDetails?
    let recordingDetails:  RecordingDetails?
    let fileDetails: FileDetails?
    let processingDetails: ProcessingDetails?
    let suggestions: Suggestions?
    let liveStreamingDetails: LiveStreamingDetails?
    let localizations: [String: Localized]
    
    var videoId: String {
        if let videoId = self.snippet?.resourceId?.videoId {
            return videoId
        }
        
        return self.id
    }
    
    enum Kind: String {
        case videoListResponse = "youtube#videoListResponse"
        case videoPlaylistResponse = "youtube#playlistListResponse"
        case playList = "youtube#playlist"
        case playListItem = "youtube#playlistItem"
        case video = "youtube#video"
        case other
    }
    
    var objectKind: Kind {
        return (Kind(rawValue: kind)).or(.other)
    }
    
    var playListId: String {
        switch objectKind {
        case .playList:
            return id
            
        default:
            return (snippet?.playlistId).or("")
        }
    }
}

class CodableID: Object, Codable, DataModel {
    @objc dynamic var id: String = ""
    @objc dynamic var kind: String = ""
    @objc dynamic var videoId: String = ""
    @objc dynamic var channelId: String = ""
    @objc dynamic var playlistId: String = ""
    
    enum CodingKeys: String, CodingKey {
        case kind, videoId, channelId, playlistId, id
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        kind = (try? container.decode(String.self, forKey: .kind)).or("")
        videoId = (try? container.decode(String.self, forKey: .videoId)).or("")
        channelId = (try? container.decode(String.self, forKey: .channelId)).or("")
        playlistId = (try? container.decode(String.self, forKey: .playlistId)).or("")
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class CodableYoutubeVideoSearchItem: Object, Codable, DataModel, YoutubeVideoMapping {
    
    @objc dynamic var id: String = ""
    @objc dynamic var kind: String = ""
    @objc dynamic var etag: String = ""
    @objc dynamic var objectId: CodableID?
    @objc dynamic var snippet: CodableSnippet?
    @objc dynamic var isFavourite: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case kind, etag
        case snippet
        case objectId = "id"
        case id = "_id"
        case isFavourite
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        kind = (try? container.decode(String.self, forKey: .kind)).or("")
        etag = (try? container.decode(String.self, forKey: .etag)).or("")
        objectId = (try? container.decode(CodableID.self, forKey: .objectId)).or(nil)
        
        snippet = (try? container.decode(CodableSnippet.self, forKey: .snippet)).or(nil)
        isFavourite = (try? container.decode(Bool.self, forKey: .isFavourite)).or(false)
    }
    
    func map() -> YoutubeVideo {
        return  .init(kind: kind,
                      etag: etag,
                      id: (objectId?.videoId).or(""),
                      isFavourite: isFavourite,
                      snippet: snippet?.map(),
                      contentDetails: nil,
                      status: nil,
                      statistics: nil,
                      player: nil,
                      topicDetails: nil,
                      recordingDetails: nil,
                      fileDetails: nil,
                      processingDetails: nil,
                      suggestions: nil,
                      liveStreamingDetails: nil,
                      localizations: [:])
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
