//
//  CollectionItem.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/23/21.
//

import Foundation

struct CollectionItem {
    let id: String
    let thumbnailUrlString: String
    let title: String
    let moreInfo: String
    let type: ItemType
    let video: YoutubeVideo?
    
    enum ItemType {
        case video
        case channel
    }
}
