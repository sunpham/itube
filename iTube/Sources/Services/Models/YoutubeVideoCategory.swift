//
//  YoutubeVideoCategory.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/4/21.
//

import Foundation

enum YoutubeVideoCategory: String, CaseIterable {
    case filmAndAnimation = "1"
    case autosAndVehicles = "2"
    case music = "10"
    case petsAndAnimals = "15"
    case shortMovies = "18"
    case sports = "17"
    case travelAndEvents = "19"
    case gaming = "20"
    case videoLogging = "21"
    case peopleAndBlogs = "22"
    case comedy = "23"
    case entertainment = "24"
    case newsPolitics = "25"
    case howtoAndStyle = "26"
    case education = "27"
    case scienceAnTechnology = "28"
    case nonprofitsAndActivism = "29"
    case movies = "30"
    case animeOrAnimation = "31"
    case actionAndAdventure = "32"
    case classics = "33"
    case drama = "36"
    case family = "37"
    case horror = "39"
    case shows = "43"
    
    var title: String {
        switch self {
        case .filmAndAnimation:
            return "Phim và hoạt hình"
            
        case .autosAndVehicles:
            return "Ô tô và xe cộ"
            
        case .music:
            return "Âm nhạc"
            
        case .petsAndAnimals:
            return "Thú cưng và động vật"
            
        case .sports:
            return "Thể thao"
            
        case .shortMovies:
            return "Phim ngắn"
            
        case .travelAndEvents:
            return "Du lịch và Sự kiện"
            
        case .gaming:
            return "Gaming"
            
        case .videoLogging:
            return "Nhật kí ví deo"
        
        case .peopleAndBlogs:
            return "Mọi người và blog"
            
        case .comedy:
            return "Hài"
            
        case .entertainment:
            return "giải trí"
            
        case .newsPolitics:
            return "Tin tức chính trị"
            
        case .howtoAndStyle:
            return "Phong cách và giải pháp"
            
        case .education:
            return "Giáo dục"
            
        case .scienceAnTechnology:
            return "Khoa học và công nghệ"
            
        case .nonprofitsAndActivism:
            return "Monprofits And Activism"
            
        case .movies:
            return "Điện ảnh"
            
        case .animeOrAnimation:
            return "Anime và hoạt hình"
            
        case .actionAndAdventure:
            return "hành động và phiêu lưu"
            
        case .classics:
            return "Kinh điển"
            
        case .drama:
            return "Dramas"
            
        case .family:
            return "Gia đình"
            
        case .horror:
            return "Horror"
            
        case .shows:
            return "Show"
        }
    }
    
    var type: HomeSectionCodeType {
        switch self {
        case .filmAndAnimation:
            return .mediumCarouselSection
            
        case .autosAndVehicles:
            return .largeCarouselSection
            
        case .music:
            return .gridViewSection
            
        case .petsAndAnimals:
            return .singleViewSection
            
        case .sports:
            return .mediumCarouselSection
            
        case .shortMovies:
            return .mediumCarouselSection
            
        case .travelAndEvents:
            return .mediumCarouselSection
            
        case .gaming:
            return .largeCarouselSection
            
        case .videoLogging:
            return .gridViewSection
            
        default:
            return .largeCarouselSection
        }
    }
}
