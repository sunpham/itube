//
//  AppServices.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/30/21.
//

import Foundation
import StoreKit

class AppServices {
    public static let shared = AppServices()
    
    private init() {}
    
    func rateApp() {
        if #available(iOS 14.0, *) {
            if let scene = UIApplication.shared.connectedScenes.first(where: { $0.activationState == .foregroundActive }) as? UIWindowScene {
                SKStoreReviewController.requestReview(in: scene)
            }
        } else {
            SKStoreReviewController.requestReview()
        }
    }
}
