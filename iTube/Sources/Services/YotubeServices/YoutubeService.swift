//
//  YoutubeService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/3/21.
//

import Foundation
import Moya
import RxSwift
import Resolver

protocol YoutubeServiceType {
    func getTrendingVideos(regoinCode: String, nextPageToken: String, videoCategoryId: String) -> Observable<YoutubeVideoPagination>
    func getVideos(regoinCode: String, nextPageToken: String, channelId: String, searchText: String, maxResults: Int) -> Observable<YoutubeVideoPagination>
    func searchVideos(regoinCode: String, nextPageToken: String, searchText: String,  maxResults: Int) -> Observable<YoutubeVideoPagination>
    func searchVideosInChannel(regoinCode: String, nextPageToken: String, channelId: String, maxResults: Int) -> Observable<YoutubeVideoPagination>
    func getVideos(regoinCode: String, nextPageToken: String, playlistId: String, videoId: String, maxResult: Int) -> Observable<YoutubeVideoPagination>
    func getChannelInfo(regoinCode: String, channelId: String) -> Observable<YoutubeChannel?>
    func getVideos(regoinCode: String, relatedtovideoId: String, nextPageToken: String, maxResult: Int) -> Observable<YoutubeVideoPagination>
}

class YoutubeService: YoutubeServiceType {
    
    static let `default` = YoutubeService()
    private let repository: Repository
    
    init(repository: Repository = YoutubeRepository.default) {
        self.repository = repository
    }
    @LazyInjected var api: Networkable
}

extension YoutubeService {
    func getTrendingVideos(regoinCode: String, nextPageToken: String, videoCategoryId: String) -> Observable<YoutubeVideoPagination> {
        let target = YoutubeTargets.TrendingVideosTarget(nextPageToken: nextPageToken,
                                                         regoinCode: regoinCode,
                                                         videoCategoryId: videoCategoryId)
        
        let predicate = NSCompoundPredicate(
            type: .and,
            subpredicates: [
                NSPredicate(format: "id == %@", "tube#category#\(videoCategoryId)"),
                NSPredicate(format: "nextPageToken != %@", nextPageToken)
            ]
        )
        let localRequest = LocalRequest(.objectPredicate(predicate,id: "tube#category#\(videoCategoryId)"), updatePolicy: .replace)
        let remoteRequest = RemoteRequest(target: target)
        let results: Observable<CodableYoutubeVideoPagination> = repository.get(fetchPolicy: FetchPolicy.cacheFirst(remote: remoteRequest, local: localRequest))
        return results.map { $0.map() }
        //        return api.request(target: target)
        //            .observe(on: MainScheduler.init())
        //            .asObservable()
        //            .map(CodableYoutubeVideoPagination.self)
        //            .map { $0.map() }
        //            .doOnNext { videos in
        //                debugPrint("Top Trending Youtube Video: \(videos)")
        //            }
        //            .asObservable()
    }
    
    func searchVideos(regoinCode: String, nextPageToken: String, searchText: String, maxResults: Int) -> Observable<YoutubeVideoPagination> {
        let target = YoutubeTargets.SearchVideosTarget(nextPageToken: nextPageToken,
                                                       regoinCode: regoinCode,
                                                       searchText: searchText,
                                                       maxResults: maxResults,
                                                       channelId: "")
        return api.request(target: target)
            .observe(on: MainScheduler.instance)
            .asObservable()
            .map(CodableYoutubeVideoSearchItemPagination.self)
            .map { $0.map() }
            .asObservable()
        
    }
    
    func getVideos(regoinCode: String, nextPageToken: String, channelId: String, searchText: String, maxResults: Int) -> Observable<YoutubeVideoPagination> {
        let target = YoutubeTargets.ChannelVideosTarget(nextPageToken: nextPageToken,
                                                        regoinCode: regoinCode,
                                                        searchText: searchText,
                                                        maxResults: maxResults,
                                                        channelId: channelId)
        let predicate = NSCompoundPredicate(
            type: .and,
            subpredicates: [
                NSPredicate(format: "id == %@", "tube#playlists#\(channelId)"),
                NSPredicate(format: "nextPageToken != %@", nextPageToken)
            ]
        )
        let localRequest = LocalRequest(.objectPredicate(predicate,id: "tube#playlists#\(channelId)"), updatePolicy: .replace)
        let remoteRequest = RemoteRequest(target: target)
        let results: Observable<CodableYoutubeVideoPagination> = repository.get(fetchPolicy: FetchPolicy.cacheFirst(remote: remoteRequest,
                                                                                                                    local: localRequest))
        return results.map { $0.map() }
        //
        //        return api.request(target: target)
        //            .observe(on: MainScheduler.instance)
        //            .asObservable()
        //            .map(CodableYoutubeVideoPagination.self)
        //            .map { $0.map() }
        //            .asObservable()
    }
    
    func getVideos(regoinCode: String, nextPageToken: String, playlistId: String, videoId: String, maxResult: Int) -> Observable<YoutubeVideoPagination> {
        
        let target = YoutubeTargets.PlayListVideosTarget(nextPageToken: nextPageToken,
                                                         regoinCode: regoinCode,
                                                         maxResults: maxResult,
                                                         playlistId: playlistId,
                                                         videoId: videoId)
        let predicate = NSCompoundPredicate(
            type: .and,
            subpredicates: [
                NSPredicate(format: "id == %@", "tube#playlistVideos#\(playlistId)"),
                NSPredicate(format: "nextPageToken != %@", nextPageToken)
            ]
        )
        let localRequest = LocalRequest(.objectPredicate(predicate,id: "tube#playlistVideos#\(playlistId)"), updatePolicy: .replace)
        let remoteRequest = RemoteRequest(target: target)
        let results: Observable<CodableYoutubeVideoPagination> = repository.get(fetchPolicy: FetchPolicy.cacheFirst(remote: remoteRequest, local: localRequest))
        return results.map {
            $0.map()
        }
        //        let target = YoutubeTargets.PlayListVideosTarget(nextPageToken: nextPageToken,
        //                                                         regoinCode: regoinCode,
        //                                                         maxResults: maxResult,
        //                                                         playlistId: playlistId)
        //
        //        return api.request(target: target)
        //            .observe(on: MainScheduler.instance)
        //            .asObservable()
        //            .map(CodableYoutubeVideoPagination.self)
        //            .map { $0.map() }
        //            .asObservable()
    }
    
    func getChannelInfo(regoinCode: String, channelId: String) -> Observable<YoutubeChannel?> {
        let target = YoutubeTargets.ChannelInfoTarget(regoinCode: regoinCode, id: channelId)
        
//        return api
//            .request(target: target)
//            .observe(on: MainScheduler.instance)
//            .asObservable()
//            .map([CodableYoutubeChannel].self, atKeyPath: "items")
//            .map { $0.first?.map() }
//            .asObservable()
        
        let predicate =  NSPredicate(format: "id == %@", "tube#channelinfo#\(channelId)")
        let localRequest = LocalRequest(.objectPredicate(predicate,id: "tube#channelinfo#\(channelId)"), updatePolicy: .replace)
        let remoteRequest = RemoteRequest(target: target, keyPath: "items")
        let results: Observable<[CodableYoutubeChannel]> = repository.get(fetchPolicy: FetchPolicy.cacheFirst(remote: remoteRequest,
                                                                                                                              local: localRequest))
        return results.map { $0.first?.map() }
    }
    
    func getVideos(regoinCode: String, relatedtovideoId: String,nextPageToken: String, maxResult: Int) -> Observable<YoutubeVideoPagination> {
        let target = YoutubeTargets.RelatedVideosTarget(regoinCode: regoinCode,
                                                        relatedToVideoId: relatedtovideoId,
                                                        maxResults: maxResult,
                                                        nextPageToken: nextPageToken)
        //        return api
        //            .request(target: target)
        //            .observe(on: MainScheduler.instance)
        //            .asObservable()
        //            .map(CodableYoutubeVideoPagination.self)
        //            .map { $0.map() }
        //            .asObservable()
        let predicate = NSCompoundPredicate(
            type: .and,
            subpredicates: [
                NSPredicate(format: "id == %@", "tube#relatedToVideos#\(relatedtovideoId)"),
                NSPredicate(format: "nextPageToken != %@", nextPageToken)
            ]
        )
        let localRequest = LocalRequest(.objectPredicate(predicate,id: "tube#relatedToVideos#\(relatedtovideoId)"), updatePolicy: .replace)
        let remoteRequest = RemoteRequest(target: target)
        let results: Observable<CodableYoutubeVideoSearchItemPagination> = repository.get(fetchPolicy: FetchPolicy.cacheFirst(remote: remoteRequest,
                                                                                                                              local: localRequest))
        return results.map { $0.map() }
    }
    
    func searchVideosInChannel(regoinCode: String, nextPageToken: String, channelId: String, maxResults: Int) -> Observable<YoutubeVideoPagination> {
        let target = YoutubeTargets.SearchVideosTarget(nextPageToken: nextPageToken,
                                                       regoinCode: regoinCode,
                                                       searchText: "",
                                                       maxResults: maxResults,
                                                       channelId: channelId)
        
        let predicate = NSCompoundPredicate(
            type: .and,
            subpredicates: [
                NSPredicate(format: "id == %@", "tube#channelvideo#\(channelId)"),
                NSPredicate(format: "nextPageToken != %@", nextPageToken)
            ]
        )
        let localRequest = LocalRequest(.objectPredicate(predicate,id: "tube#channelvideo#\(channelId)"), updatePolicy: .replace)
        let remoteRequest = RemoteRequest(target: target)
        let results: Observable<CodableYoutubeVideoSearchItemPagination> = repository.get(fetchPolicy: FetchPolicy.cacheFirst(remote: remoteRequest,
                                                                                                                              local: localRequest))
        return results.map { $0.map() }
    }
    
}

protocol TubeVideoListServiceType {
    func getVideos(nextPageToken: String) -> Observable<YoutubeVideoPagination>
}

class CategoryVideoListService: TubeVideoListServiceType {
    
    @LazyInjected var youtbeService: YoutubeServiceType
    let categoryId: String
    
    init(categoryId: String) {
        self.categoryId = categoryId
    }
    func getVideos(nextPageToken: String) -> Observable<YoutubeVideoPagination> {
        return youtbeService.getTrendingVideos(regoinCode: "VN", nextPageToken: nextPageToken, videoCategoryId: categoryId)
    }
}

class PlayListItemsVideoService: TubeVideoListServiceType {
    @LazyInjected var youtbeService: YoutubeServiceType
    let playlistId: String
    
    init(playlistId: String) {
        self.playlistId = playlistId
    }
    
    func getVideos(nextPageToken: String) -> Observable<YoutubeVideoPagination> {
        return youtbeService.getVideos(regoinCode: "", nextPageToken: nextPageToken, playlistId: playlistId, videoId: "", maxResult: 10)
    }
}

class ChannelVideosService: TubeVideoListServiceType {
    @LazyInjected var youtbeService: YoutubeServiceType
    let channelId: String
    
    init(channelId: String) {
        self.channelId = channelId
    }
    
    func getVideos(nextPageToken: String) -> Observable<YoutubeVideoPagination> {
        return youtbeService.searchVideosInChannel(regoinCode: "VN", nextPageToken: nextPageToken, channelId: channelId, maxResults: 10)
    }
}
