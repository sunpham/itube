//
//  ChannelPlaysListService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/16/21.
//

import Resolver
import RxSwift

class ChannelPlayListService: TubeVideoListServiceType {
    
    @LazyInjected var youtubeService: YoutubeService
    let channelId: String
    
    init(channelId: String) {
        self.channelId = channelId
    }
    
    func getVideos(nextPageToken: String) -> Observable<YoutubeVideoPagination> {
        return youtubeService.getVideos(regoinCode: "VN",
                                        nextPageToken: nextPageToken,
                                        channelId: channelId,
                                        searchText: "",
                                        maxResults: 10)
    }
}
