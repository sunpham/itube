//
//  FavouriteService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/20/21.
//

import Foundation
import RxSwift
import RxCocoa

protocol FavouriteServiceType {
    func subscribeVideo(videoId: String) -> Observable<Bool>
    func unSubscribeVideo(videoId: String) -> Observable<Bool>
    func getFavouriteVideos() -> Observable<[YoutubeVideo]>
}

class FavouriteService {
    let realmManager: RealmManagerType
    let disposeBag = DisposeBag()
    
    public static var `default`: FavouriteService {
        return FavouriteService(realmManager: RealmManager.default)
    }
    
    init(realmManager: RealmManagerType) {
        self.realmManager = realmManager
    }
}

extension FavouriteService: FavouriteServiceType {
    func subscribeVideo(videoId: String) -> Observable<Bool> {
        DispatchQueue.main.async {
            self.realmManager.update(CodableYotubeVideo.self, filter: NSPredicate(format: "id == %@", videoId)) { (object) in
                object.isFavourite = true
            }
        }
        
        DispatchQueue.main.async {
            self.realmManager.update(CodableYoutubeVideoSearchItem.self, filter: NSPredicate(format: "id == %@", videoId)) { (object) in
                object.isFavourite = true
            }
        }
        return Observable.just(true).observe(on: MainScheduler.instance).asObservable()
    }
    
    func unSubscribeVideo(videoId: String) -> Observable<Bool> {
        
        DispatchQueue.main.async {
            self.realmManager.update(CodableYotubeVideo.self, filter: NSPredicate(format: "id == %@", videoId)) { (object) in
                object.isFavourite = false
            }
        }
        
        DispatchQueue.main.async {
            self.realmManager.update(CodableYoutubeVideoSearchItem.self, filter: NSPredicate(format: "id == %@", videoId)) { (object) in
                object.isFavourite = false
            }
        }
        return Observable.just(true).observe(on: MainScheduler.instance).asObservable()
    }
    
    func getFavouriteVideos() -> Observable<[YoutubeVideo]> {
        let videosInYoutubeVideoTable = Observable.just(realmManager
                                                            .getAll(CodableYotubeVideo.self)
                                                            .toArray().filter { $0.isFavourite == true }
                                                            .map { $0.map() })
            .observe(on: MainScheduler.instance)
        let videosInSearchVideoTable = Observable.just(realmManager
                                                        .getAll(CodableYoutubeVideoSearchItem.self)
                                                        .toArray()
                                                        .filter { $0.isFavourite }
                                                        .map { $0.map() })
            .observe(on: MainScheduler.instance)
        
        return Observable.zip(
            videosInYoutubeVideoTable,
            videosInSearchVideoTable
        )
        .map { $0.0 + $0.1 }
        .observe(on: MainScheduler.instance)
        .asObservable()
    }
}
