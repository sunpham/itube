//
//  CollectionVideosService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/23/21.
//

import Foundation
import Resolver
import RxSwift


/// `Create Collection Strategy By Caching`
/**
 - Saving to userdefaults or realm database
 */

protocol CollectionVideosServiceType {
    func addHistoryItem(_ item: YoutubeVideo, createAt: Date) -> Observable<Bool>
    func addFollwingItem(_ item: FollowingItem,  isFollowing: Bool) -> Observable<Bool>
    func addDownloadedItem(_ item: YoutubeVideo) -> Observable<Bool>
    
    func delleteHistoryItem(_ itemId: String) -> Observable<Bool>
    func deleteFollwingItem(_ itemId: String) -> Observable<Bool>
    func deleteDownloadedItem(_ itemId: String) -> Observable<Bool>
    
    func getHistoryItems() -> Observable<[YoutubeVideo]>
    func getFollowingItems() -> Observable<[FollowingItem]>
    func getDowloandedItems() -> Observable<[YoutubeVideo]>
    
    func getIsFollowing(by channelId: String) -> Observable<Bool>
 }

class CollectionVideosService {
    
    static let `default` = CollectionVideosService(realmManager: LongTermStorageRealmManager.default)
    
    let realmManager: RealmManagerType
    let scheduler: SchedulerType
    
    init(realmManager: RealmManagerType) {
        
        self.realmManager = realmManager
        self.scheduler = ConcurrentDispatchQueueScheduler(queue: DispatchQueue.global())
    }
}

extension CollectionVideosService: CollectionVideosServiceType {
    
    // MARK: - `Add actions`
    func addHistoryItem(_ item: YoutubeVideo, createAt: Date) -> Observable<Bool> {
        guard let willAddObject = CodableHistoryItem().from(item) else { return .empty() }
        willAddObject.createAt = createAt
        return realmManager.create(object: willAddObject, update: true, writeBlock: { _ in })
            .subscribe(on: scheduler)
            .observe(on: MainScheduler.instance)
            .mapTo(true)
            .asObservable()
    }
    
    func addFollwingItem(_ item: FollowingItem, isFollowing: Bool) -> Observable<Bool> {
        guard let willAddObject = CodableFollowingItem().from(item) else { return .empty() }
        return realmManager.create(object: willAddObject, update: true, writeBlock: { _ in })
            .subscribe(on: scheduler)
            .observe(on: MainScheduler.instance)
            .mapTo(true)
            .asObservable()
    }
    
    func addDownloadedItem(_ item: YoutubeVideo) -> Observable<Bool> {
        return .just(true)
    }
    
    // MARK: - ` Delete actions`
    func delleteHistoryItem(_ itemId: String) -> Observable<Bool> {
        return .just(true)
    }
    
    func deleteFollwingItem(_ itemId: String) -> Observable<Bool> {
        
        return Observable.just(
            realmManager.delete(type: CodableFollowingItem.self, id: itemId)
        )
        .mapTo(true)
        .observe(on: MainScheduler.instance)
        .asObservable()
    }
    
    func deleteDownloadedItem(_ itemId: String) -> Observable<Bool> {
        return .just(true)
    }
    
    
    // MARK: - `Get Actions`
    func getHistoryItems() -> Observable<[YoutubeVideo]> {
        return Observable
            .just(
                realmManager.getAll(CodableHistoryItem.self)
                .sorted(byKeyPath: "createAt", ascending: false)
            )
            .map { $0.toArray() }
            .map { $0.compactMap { $0.video?.map() } }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getFollowingItems() -> Observable<[FollowingItem]> {
        return Observable
            .just(
                realmManager.getAll(CodableFollowingItem.self)
                .sorted(byKeyPath: "createAt", ascending: false)
            )
            .map { $0.toArray() }
            .map { $0.compactMap { $0.map() } }
            .observe(on: MainScheduler.instance)
            .asObservable()
    }
    
    func getDowloandedItems() -> Observable<[YoutubeVideo]> {
        return Observable.just([])
    }
    
    func getIsFollowing(by channelId: String) -> Observable<Bool> {
        return Observable.just(
            realmManager.get(CodableFollowingItem.self, filter: NSPredicate(format: "id == %@", channelId)).toArray().first
        )
        .map { $0 != nil }
    }
}

import RealmSwift
class CodableHistoryItem: Object, Codable, DataModel {
    @objc dynamic var id: String = ""
    @objc dynamic var video: CodableYotubeVideo?
    @objc dynamic var createAt: Date = Date()
    
    enum CodingKeys: String, CodingKey {
        case id
        case video
        case createAt
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        video = (try? container.decode(CodableYotubeVideo.self, forKey: .video)).or(nil)
        createAt = (try? container.decode(Date.self, forKey: .createAt)).or(Date())
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CodableHistoryItem: CodableObjectCovertable {
    typealias SourceElementType = YoutubeVideo
    typealias DestinationElementType = CodableHistoryItem
    
    func from(_ model: YoutubeVideo?) -> CodableHistoryItem? {
        guard let model = model else { return nil }
        id = "collection#histories#\(model.id)"
        video = CodableYotubeVideo().from(model)
        createAt = Date()
        return self
    }
}

struct FollowingItem {
    let id: String
    let title: String
    let urlString: String
}
class CodableFollowingItem: Object, Codable, DataModel {
    @objc dynamic var id: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var urlString: String = ""
    @objc dynamic var createAt: Date = Date()
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case urlString
        case createAt
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = (try? container.decode(String.self, forKey: .id)).or(UUID().uuidString)
        title = (try? container.decode(String.self, forKey: .title)).or("")
        urlString = (try? container.decode(String.self, forKey: .urlString)).or("")
        createAt = (try? container.decode(Date.self, forKey: .createAt)).or(Date())
    }
    
    func map() -> FollowingItem {
        return .init(id: id, title: title, urlString: urlString)
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CodableFollowingItem: CodableObjectCovertable {
    typealias SourceElementType = FollowingItem
    typealias DestinationElementType = CodableFollowingItem
    
    func from(_ model: FollowingItem?) -> CodableFollowingItem? {
        guard let model = model else { return nil }
        id = model.id
        title = model.title
        urlString = model.urlString
        createAt = Date()
        return self
    }
}
