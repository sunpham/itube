//
//  TemplateService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import Resolver
import RxSwift

protocol TemplateServiceType {
    func getItems(categoryId: String) -> Observable<[TemplateItem]>
}

class TemplateService: TemplateServiceType {
    
    static let `default` = TemplateService()
    
    @LazyInjected var youtubeService: YoutubeServiceType
    
    func getItems(categoryId: String) -> Observable<[TemplateItem]> {
        return youtubeService
            .getTrendingVideos(regoinCode: "VN",
                               nextPageToken: "",
                               videoCategoryId: categoryId)
            .map { $0.items.map { $0.convertToCarouselItem()} }
    }
}

extension YoutubeVideo {
    func convertToCarouselItem() -> TemplateItem {
        return .init(model: self)
    }
}

class FavouriteTemplateService: TemplateServiceType {
    
    @LazyInjected var favouriteService: FavouriteServiceType
    
    static let `default` = FavouriteTemplateService()
    
    func getItems(categoryId: String) -> Observable<[TemplateItem]> {
        return favouriteService.getFavouriteVideos().map { $0.map { $0.convertToCarouselItem() } }
    }
}
