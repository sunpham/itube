//
//  BannerService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import Resolver
import RxSwift


protocol BannerServiceType {
    func getBanners() -> Observable<[Banner]>
}

class HomeBannerSerivce: BannerServiceType {
    
    @LazyInjected var youtubeService: YoutubeServiceType
    
    func getBanners() -> Observable<[Banner]> {
        return youtubeService
            .getTrendingVideos(regoinCode: "VN",
                               nextPageToken: "",
                               videoCategoryId: "")
            .map { $0.items.map { $0.covertToBanner() } }
    }
}

extension YoutubeVideo {
    func covertToBanner() -> Banner {
        return Banner(model: self)
    }
}
