//
//  YoutubeTargets.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/12/21.
//

import Foundation
import Moya

protocol YoutubeTargetType: TargetType {}
protocol YoutubeAuthenticateTarget: AccessTokenAuthorizable {}


extension YoutubeTargetType {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
}

extension YoutubeAuthenticateTarget {
    var baseURL: URL {
        guard let url = URL(string: Natrium.apiBase) else {
            fatalError("Invalid API Base : \(Natrium.apiBase)")
        }
        
        return url
    }
}

enum YoutubeTargets {
    struct TrendingVideosTarget: YoutubeTargetType {
        
        let nextPageToken: String
        let regoinCode: String
        let videoCategoryId: String
        
        var path: String {
            return "/videos"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        
        var parameters: [String: Any] {
            var params: [String: Any] = [
                "part": "snippet,statistics",
                "chart": "mostPopular",
                "regionCode": regoinCode,
                "pageToken": nextPageToken,
                "key": Natrium.youtubeAPIKey,
                "maxResults": 10
            ]
            
            if videoCategoryId.isNotEmpty {
                params["videoCategoryId"] = videoCategoryId
            }
            
            return params
        }
    }
    
    struct SearchVideosTarget: YoutubeTargetType {
        
        let nextPageToken: String
        let regoinCode: String
        let searchText: String
        let maxResults: Int
        let channelId: String
        
        var path: String {
            return "/search"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        
        var parameters: [String: Any] {
            var params: [String: Any] = [
                "part": "snippet",
                "regionCode": regoinCode,
                "pageToken": nextPageToken,
                "key": Natrium.youtubeAPIKey,
                "maxResults": maxResults
            ]
            
            if searchText.isNotEmpty {
                params["q"] = searchText
            }
            
            if channelId.isNotEmpty {
                params["channelId"] = channelId
            }
            
            return params
        }
    }
    
    struct ChannelVideosTarget: YoutubeTargetType {
        
        let nextPageToken: String
        let regoinCode: String
        let searchText: String
        let maxResults: Int
        let channelId: String
        
        var path: String {
            return "/playlists"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        
        var parameters: [String: Any] {
            let params: [String: Any] = [
                "part": "snippet",
                "regionCode": regoinCode,
                "pageToken": nextPageToken,
                "key": Natrium.youtubeAPIKey,
                "maxResults": maxResults,
                "channelId": channelId
            ]
            return params
        }
    }
    
    struct PlayListVideosTarget: YoutubeTargetType {
        
        let nextPageToken: String
        let regoinCode: String
        let maxResults: Int
        let playlistId: String
        let videoId: String
        
        var path: String {
            return "/playlistItems"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        
        var parameters: [String: Any] {
            let params: [String: Any] = [
                "part": "snippet,contentDetails",
                "regionCode": regoinCode,
                "pageToken": nextPageToken,
                "key": Natrium.youtubeAPIKey,
                "maxResults": maxResults,
                "playlistId": playlistId,
                "videoId": videoId
            ]
            return params
        }
    }
    
    struct ChannelInfoTarget: YoutubeTargetType {
        
        let regoinCode: String
        let id: String
        
        var path: String {
            return "/channels"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        
        var parameters: [String: Any] {
            let params: [String: Any] = [
                "part": "snippet,contentDetails,statistics",
                "regionCode": regoinCode,
                "key": Natrium.youtubeAPIKey,
                "id": id
            ]
            return params
        }
    }
    
    struct RelatedVideosTarget: YoutubeTargetType {
        
        let regoinCode: String
        let relatedToVideoId: String
        let maxResults: Int
        let nextPageToken: String
        
        var path: String {
            return "/search"
        }
        
        var method: Moya.Method {
            return .get
        }
        
        var task: Task {
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
        
        var parameters: [String: Any] {
            let params: [String: Any] = [
                "part": "snippet",
                "regionCode": regoinCode,
                "key": Natrium.youtubeAPIKey,
                "relatedToVideoId": relatedToVideoId,
                "maxResults": maxResults,
                "pageToken": nextPageToken,
                "type": "video"
            ]
            return params
        }
    }
}
