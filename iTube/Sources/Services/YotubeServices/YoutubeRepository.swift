//
//  YoutubeRepository.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/11/21.
//

import Foundation

class YoutubeRepository: Repository {
    
    static let `default` = YoutubeRepository(remoteRepository: DefaultRemoteRepository.default,
                                             localRepository: DefaultLocalRepository(realmManager: RealmManager.default))
     let remoteRepository: RemoteRepository
     let localRepository: LocalRepository
    
    init(remoteRepository: RemoteRepository, localRepository: LocalRepository) {
        self.remoteRepository = remoteRepository
        self.localRepository = localRepository
    }
}
