//
//  CollectionPageServiceType.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/23/21.
//

import Foundation
import RxSwift
import Resolver

protocol CollectionPageServiceType {
    func getHiglightedItem() -> Observable<CollectionItem?>
    func getCollectionItems() -> Observable<[CollectionItem]>
}

class HistoriesCollectionPageService: CollectionPageServiceType {
    @LazyInjected var collectionVideosService: CollectionVideosServiceType
    
    func getHiglightedItem() -> Observable<CollectionItem?> {
        return collectionVideosService.getHistoryItems().map { $0.first?.asCollectionItem() }
    }
    
    func getCollectionItems() -> Observable<[CollectionItem]> {
        return collectionVideosService
            .getHistoryItems()
            .map { items -> [CollectionItem] in
                guard items.isNotEmpty else { return [] }
                var results = items
                results.removeFirst()
                return results.map { $0.asCollectionItem() }
            }
    }
}

extension YoutubeVideo {
    func asCollectionItem() -> CollectionItem {
        return .init(id: id,
                     thumbnailUrlString: (snippet?.thumbnails?.getThumbnailUrlString()).or(""),
                     title: (snippet?.title).or(""),
                     moreInfo: (snippet?.channelTitle).or(""),
                     type: .video, video: self)
    }
}

extension YoutubeChannel {
    func asCollectionItem() -> CollectionItem {
        return .init(id: id,
                     thumbnailUrlString: (snippet?.thumbnails?.getThumbnailUrlString()).or(""),
                     title: (snippet?.title).or(""),
                     moreInfo: "\((statistics?.viewCount).or(.zero)) view",
                     type: .channel, video: nil)
    }
}

extension FollowingItem {
    func asCollectionItem() -> CollectionItem {
        return .init(id: id, thumbnailUrlString: urlString, title: title, moreInfo: "", type: .channel, video: nil)
    }
}

class FollowingCollectionPageService: CollectionPageServiceType {
    
    static let `default` = FollowingCollectionPageService()
    
    @LazyInjected var collectionVideosService: CollectionVideosServiceType
    
    func getHiglightedItem() -> Observable<CollectionItem?> {
        return collectionVideosService.getFollowingItems().map { $0.first?.asCollectionItem() }
    }
    
    func getCollectionItems() -> Observable<[CollectionItem]> {
        return collectionVideosService
            .getFollowingItems()
            .map { items -> [CollectionItem] in
                guard items.isNotEmpty else { return [] }
                var results = items
                results.removeFirst()
                return results.map { $0.asCollectionItem() }
            }
    }
}

class DownloadedCollectionPageService: CollectionPageServiceType {
    
    static let `default` = DownloadedCollectionPageService()
    
    @LazyInjected var collectionVideosService: CollectionVideosServiceType
    
    func getHiglightedItem() -> Observable<CollectionItem?> {
        return collectionVideosService.getDowloandedItems().map { $0.first?.asCollectionItem() }
    }
    
    func getCollectionItems() -> Observable<[CollectionItem]> {
        return collectionVideosService
            .getDowloandedItems()
            .map { items -> [CollectionItem] in
                guard items.isNotEmpty else { return [] }
                var results = items
                results.removeFirst()
                return results.map { $0.asCollectionItem() }
            }
    }
}
