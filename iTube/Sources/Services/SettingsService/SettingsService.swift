//
//  SettingsService.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/17/21.
//

import Foundation

class SettingsService {
    
    public static let shared = SettingsService()
    private init() {}
    
    public var isAutoUpdate: Bool {
        get {
            return UserDefaults.standard.isAutoUpdate
        }
        
        set {
            UserDefaults.standard.isAutoUpdate = newValue
        }
    }
    
    var playerSettings: UserDefaultPlayerSettings {
        get {
            if let settings = UserDefaults.standard.playerSettings {
                return settings
            }
            
            let settings = UserDefaultPlayerSettings()
            UserDefaults.standard.playerSettings = settings
            return settings
        }
        
        set {
            UserDefaults.standard.playerSettings = newValue
        }
    }
    
    var pushNotification: BoolEnum {
        get {
            return UserDefaults.standard.pushNotification
        }
        
        set {
            UserDefaults.standard.pushNotification = newValue
        }
    }
    
    public func toglePushNotification() {
        pushNotification.switch()
    }
}
