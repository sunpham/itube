//
//  DeeplinkPlugin.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/19/21.
//

import Foundation

protocol DeeplinkPluginListener {}

protocol DeeplinkPlugin {
    func build(deeplinkComponent: URLComponents, listener: DeeplinkPluginListener) -> BaseWireframe
}

/**
 The SuperSpaManagement Deeplink will have a formater like this
 - The formated host://superapp/module_name/functional_name/more_info_part?prameter_1=value_1&parameter_2=value2
 */
class SuperSpaManagementDeeplinkPlugin: DeeplinkPlugin {
    var listener: DeeplinkPluginListener?
    
    func build(deeplinkComponent: URLComponents, listener: DeeplinkPluginListener) -> BaseWireframe {
        self.listener = listener
        
        let urlPathComponents = deeplinkComponent.path.mapToUrlPathComponents()
        
        let totalUrlPathComponents = 3
        guard urlPathComponents.count == totalUrlPathComponents else  {
            fatalError("Not correct url component")
        }
        fatalError("Function not implementation")
    }
}

class DeeplinkParser {
    static let availableHosts = ["SuperSpaManagement.com", "superapp"]
    
}

extension String {
    func mapToDeeplinkURLComponentsOrNil() -> URLComponents? {
        return URLComponents(string: self)
    }
    
    func mapToUrlPathComponents() -> [String] {
        return self.split(separator: "/").map { String($0) }
    }
}

protocol DeeplinkManagable: AnyObject {
    func attachDeeplinkPlugin(deeplink: String)
}

class DeeplinkManager: DeeplinkManagable {
    func attachDeeplinkPlugin(deeplink: String) {
        
    }
}
