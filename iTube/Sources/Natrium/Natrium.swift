//
//  BeepbeeNatrium.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/7/21.
//

import Foundation

enum AppEnvironment {
    case dev
    case staging
    case uat
    case production
}

protocol NatriumMappable {
    var evironmentParameters: [AppEnvironment: String] { get }
}

extension NatriumMappable {
    var value: String {
        return (evironmentParameters[AppEnvironment.current]).or("")
    }
}

enum ApiBase: NatriumMappable {
    case `current`
    
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev : "https://youtube.googleapis.com/youtube/v3",
            .staging: "https://youtube.googleapis.com/youtube/v3",
            .uat: "https://youtube.googleapis.com/youtube/v3",
            .production: "https://youtube.googleapis.com/youtube/v3"
        ]
    }
}

enum ApiSuggestQueries: NatriumMappable {
    case `current`
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev: "http://suggestqueries.google.com/complete/search",
            .staging: "http://suggestqueries.google.com/complete/search",
            .uat: "http://suggestqueries.google.com/complete/search",
            .production: "http://suggestqueries.google.com/complete/search"
        ]
    }
}

enum GoogleInfomation {
    enum ClientID: NatriumMappable {
        
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                
                .dev: "509701648420-t6qdkg3j77c9usf8ks7ekf8m9vqdikpq.apps.googleusercontent.com",
                .staging: "509701648420-t6qdkg3j77c9usf8ks7ekf8m9vqdikpq.apps.googleusercontent.com",
                .uat: "509701648420-t6qdkg3j77c9usf8ks7ekf8m9vqdikpq.apps.googleusercontent.com",
                .production: "509701648420-t6qdkg3j77c9usf8ks7ekf8m9vqdikpq.apps.googleusercontent.com"
            ]
        }
    }
    
    enum APIKey: NatriumMappable {
        case `current`
        
        var currentIndex: Int {
            if UserDefaults.standard.apiKeyInfo == nil {
                UserDefaults.standard.apiKeyInfo = UserDefaulAPIKey(currentIndex: 0, count: APIKey.apiKeys.count)
                return 0
            }
            
            return (UserDefaults.standard.apiKeyInfo?.currentIndex).or(.zero)
        }
        
        var evironmentParameters: [AppEnvironment : String] {
            return APIKey.apiKeys[currentIndex]
        }
        
        /// 50.000 units
        private static let apiKeys: [[AppEnvironment: String]] = [
            [
                .dev: "AIzaSyCDMYz4pdCfGJOzvm9SWLH6oFiw0SdSqrU",
                .staging: "AIzaSyCDMYz4pdCfGJOzvm9SWLH6oFiw0SdSqrU",
                .uat: "AIzaSyCDMYz4pdCfGJOzvm9SWLH6oFiw0SdSqrU",
                .production: "AIzaSyCDMYz4pdCfGJOzvm9SWLH6oFiw0SdSqrU"
            ],
            [
                .dev:"AIzaSyBGpSJqT7FnXUdk-Hk5UzFTCqCuKud7X1Q",
                .staging: "AIzaSyBGpSJqT7FnXUdk-Hk5UzFTCqCuKud7X1Q",
                .uat: "AIzaSyBGpSJqT7FnXUdk-Hk5UzFTCqCuKud7X1Q",
                .production: "AIzaSyBGpSJqT7FnXUdk-Hk5UzFTCqCuKud7X1Q"
            ],
            [
                .dev: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA",
                .staging: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA",
                .uat: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA",
                .production: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA"
            ],
            [
                .dev: "AIzaSyCVpDonazCAT62Ed3IQuPApNcjMhsPVVUw",
                .staging: "AIzaSyCVpDonazCAT62Ed3IQuPApNcjMhsPVVUw",
                .uat: "AIzaSyCVpDonazCAT62Ed3IQuPApNcjMhsPVVUw",
                .production: "AIzaSyCVpDonazCAT62Ed3IQuPApNcjMhsPVVUw"
            ],
            [
                .dev: "AIzaSyCjPQtYG6ToiuNWC6HzwCzG9KSh15p5SRY",
                .staging: "AIzaSyCjPQtYG6ToiuNWC6HzwCzG9KSh15p5SRY",
                .uat: "AIzaSyCjPQtYG6ToiuNWC6HzwCzG9KSh15p5SRY",
                .production: "AIzaSyCjPQtYG6ToiuNWC6HzwCzG9KSh15p5SRY"
            ],
            [
                .dev: "AIzaSyDRMtqS7qOG_Hz5bYwgFFWLPtcP1RfIcIw",
                .staging: "AIzaSyDRMtqS7qOG_Hz5bYwgFFWLPtcP1RfIcIw",
                .uat: "AIzaSyDRMtqS7qOG_Hz5bYwgFFWLPtcP1RfIcIw",
                .production: "AIzaSyDRMtqS7qOG_Hz5bYwgFFWLPtcP1RfIcIw"
            ],
            [
                .dev: "AIzaSyAqUlufnNAzwvhtfy0a8ulfZXV7kKA9a2w",
                .staging: "AIzaSyAqUlufnNAzwvhtfy0a8ulfZXV7kKA9a2w",
                .uat: "AIzaSyAqUlufnNAzwvhtfy0a8ulfZXV7kKA9a2w",
                .production: "AIzaSyAqUlufnNAzwvhtfy0a8ulfZXV7kKA9a2w"
            ],
            [
                .dev: "AIzaSyAtiPndpTNuyRmkZpu3aXon_BUGQuwZDQU",
                .staging: "AIzaSyAtiPndpTNuyRmkZpu3aXon_BUGQuwZDQU",
                .uat: "AIzaSyAtiPndpTNuyRmkZpu3aXon_BUGQuwZDQU",
                .production: "AIzaSyAtiPndpTNuyRmkZpu3aXon_BUGQuwZDQU"
            ],
            [
                .dev: "AIzaSyA5xgvG8d-FRhAAWULGt_qnq7Oal9_Jsqc",
                .staging: "AIzaSyA5xgvG8d-FRhAAWULGt_qnq7Oal9_Jsqc",
                .uat: "AIzaSyA5xgvG8d-FRhAAWULGt_qnq7Oal9_Jsqc",
                .production: "AIzaSyA5xgvG8d-FRhAAWULGt_qnq7Oal9_Jsqc"
            ],
            [
                .dev: "AIzaSyCQLk0z8hAP3ZWjVRYC1lSdLXyeUiwxXP4",
                .staging: "AIzaSyCQLk0z8hAP3ZWjVRYC1lSdLXyeUiwxXP4",
                .uat: "AIzaSyCQLk0z8hAP3ZWjVRYC1lSdLXyeUiwxXP4",
                .production: "AIzaSyCQLk0z8hAP3ZWjVRYC1lSdLXyeUiwxXP4"
            ],
            [
                .dev: "AIzaSyB3dFlSTMQ3N6j35HqnaN6oGarDO7vGJ4o",
                .staging: "AIzaSyB3dFlSTMQ3N6j35HqnaN6oGarDO7vGJ4o",
                .uat: "AIzaSyB3dFlSTMQ3N6j35HqnaN6oGarDO7vGJ4o",
                .production: "AIzaSyB3dFlSTMQ3N6j35HqnaN6oGarDO7vGJ4o"
            ],
            [
                .dev: "AIzaSyAZM4pacG6eAd24DCq-KgRP8WGdLxtWL0I",
                .staging: "AIzaSyAZM4pacG6eAd24DCq-KgRP8WGdLxtWL0I",
                .uat: "AIzaSyAZM4pacG6eAd24DCq-KgRP8WGdLxtWL0I",
                .production: "AIzaSyAZM4pacG6eAd24DCq-KgRP8WGdLxtWL0I"
            ],
            [
                .dev: "AIzaSyA5-lCe0DpnfWWIA3w6oRgkNmQxPB5DJlE",
                .staging: "AIzaSyA5-lCe0DpnfWWIA3w6oRgkNmQxPB5DJlE",
                .uat: "AIzaSyA5-lCe0DpnfWWIA3w6oRgkNmQxPB5DJlE",
                .production: "AIzaSyA5-lCe0DpnfWWIA3w6oRgkNmQxPB5DJlE"
            ],
            
            /// Duplicate key in iTube
            [
                .dev: "AIzaSyA7pVCb5cfv8iHOG12LT4W-2dMgPQrZzEA",
                .staging: "AIzaSyA7pVCb5cfv8iHOG12LT4W-2dMgPQrZzEA",
                .uat: "AIzaSyA7pVCb5cfv8iHOG12LT4W-2dMgPQrZzEA",
                .production: "AIzaSyA7pVCb5cfv8iHOG12LT4W-2dMgPQrZzEA"
            ],
            
            [
                .dev: "AIzaSyBCfmMR1reVbo-P_7N4XECXHLqdM3XUQB8",
                .staging:  "AIzaSyBCfmMR1reVbo-P_7N4XECXHLqdM3XUQB8",
                .uat: "AIzaSyBCfmMR1reVbo-P_7N4XECXHLqdM3XUQB8",
                .production: "AIzaSyBCfmMR1reVbo-P_7N4XECXHLqdM3XUQB8"
            ],
            [
                .dev: "AIzaSyAgCf6sByLM6A9RP7eVeEmyXbl69bE6GhA",
                .staging: "AIzaSyAgCf6sByLM6A9RP7eVeEmyXbl69bE6GhA",
                .uat: "AIzaSyAgCf6sByLM6A9RP7eVeEmyXbl69bE6GhA",
                .production: "AIzaSyAgCf6sByLM6A9RP7eVeEmyXbl69bE6GhA"
            ],
            
            /// Dulicate key
            [
                .dev: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA",
                .staging: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA",
                .uat: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA",
                .production: "AIzaSyD2zvMJz7xYUfbBg9zQ25KTZ3iNDMR8kBA"
            ]
        ]
    }
}


enum AdmobConfiguration {
    
    case `current`
    
    var appIdentifier: String {
        return ApplicationIdentifier.current.value
    }
    
    var bannerAdId: String {
        return BannerAdId.current.value
    }
    
    var interstitialAdId: String {
        return ApplicationIdentifier.current.value
    }
    
    var interstitialVideoAdId: String {
        return InterstitialVideoAdId.current.value
    }
    
    var rewardedAdId: String {
        return RewardedAdId.current.value
    }
    
    var rewardedInterstitialAdId: String {
        return RewardedInterstitialAdId.current.value
    }
    
    var nativeAdvancedAdId: String {
        return NativeAdvancedAdId.current.value
    }
    
    enum ApplicationIdentifier: NatriumMappable {
        
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                .dev: "ca-app-pub-3940256099942544~1458002511"
            ]
        }
    }
    
    enum BannerAdId: NatriumMappable {
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                .dev: "ca-app-pub-3940256099942544/6300978111",
                .production: ""
            ]
        }
    }
    
    enum InterstitialAdId: NatriumMappable {
        
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                .dev: "ca-app-pub-3940256099942544/1033173712"
            ]
        }
    }
    
    enum InterstitialVideoAdId: NatriumMappable {
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                .dev: "ca-app-pub-3940256099942544/8691691433"
            ]
        }
    }
    
    enum RewardedAdId: NatriumMappable {
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                .dev: "ca-app-pub-3940256099942544/1712485313"
            ]
        }
    }
    
    enum RewardedInterstitialAdId: NatriumMappable {
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                .dev: "ca-app-pub-3940256099942544/5354046379"
            ]
        }
    }
    
    enum NativeAdvancedAdId: NatriumMappable {
        case `current`
        var evironmentParameters: [AppEnvironment : String] {
            return [
                .dev: "ca-app-pub-3940256099942544/2247696110"
            ]
        }
    }
}

enum AppLink: NatriumMappable {
    case `current`
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev: "https://www.facebook.com/pham.tuan.507679"
        ]
    }
}

enum AppID: NatriumMappable {
    case `current`
    var evironmentParameters: [AppEnvironment : String] {
        return [
            .dev: "id959379869"
        ]
    }
}

enum AppStaticUrls {
    
    case `current`
    var rating: String {
        return "itms-apps://itunes.apple.com/app/" + AppID.current.value
    }
}


extension AppEnvironment {
    
    static let current: AppEnvironment = .dev
}

enum Natrium {
    public static var apiBase: String = ApiBase.current.value
    public static var apiSuggestQueries: String = ApiSuggestQueries.current.value
    public static var googleClientID: String = GoogleInfomation.ClientID.current.value
    public static var youtubeAPIKey: String {
        return GoogleInfomation.APIKey.current.value
    }
    
    public static var admobConfiguration = AdmobConfiguration.current
    public static var appLink: String {
        return AppLink.current.value
    }
    
    public static var appStaticUrls = AppStaticUrls.current
}
