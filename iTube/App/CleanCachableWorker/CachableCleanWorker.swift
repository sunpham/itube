//
//  CachableCleanWorker.swift
//  iTube
//
//  Created by PHAM ANH TUAN on 4/24/21.
//

import Foundation
import SwiftyTimer

class CachableCleanWorker {
    
    public static let shared = CachableCleanWorker()
    private let notLongtermSupportStorageRealm: RealmManagerType
    private let longtermSupportStorageRealm: RealmManagerType
    
    private init(notLongtermSupportStorageRealm: RealmManagerType = RealmManager.default,
                 longtermSupportStorageRealm: RealmManagerType = RealmManager.default) {
        self.notLongtermSupportStorageRealm = notLongtermSupportStorageRealm
        self.longtermSupportStorageRealm = longtermSupportStorageRealm
    }
    
    private func cleanupDataDoestNotSupportLongTermStorage() {
        notLongtermSupportStorageRealm.clearData()
    }
    private func minimizeDataSupportLongTermStorage() {
        debugPrint("Minimize data in database")
    }
    
    func registerWorker() {
        Timer.every(3.days) { [weak self] _ in
            self?.cleanupDataDoestNotSupportLongTermStorage()
            self?.minimizeDataSupportLongTermStorage()
        }
    }
}
