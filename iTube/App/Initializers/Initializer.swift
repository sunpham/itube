//
//  Initializer.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import Foundation

protocol Initializable: class {
    func performInitialization()
}
