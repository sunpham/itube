//
//  ThemeInitializer.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import UIKit

class ThemeInitializer: Initializable {

    func performInitialization() {
        UINavigationBar.appearance(whenContainedInInstancesOf: [TubeNavigationController.self]).tintColor = .black
        UINavigationBar.appearance(whenContainedInInstancesOf: [TubeNavigationController.self]).barTintColor = UIColor.white
        UINavigationBar.appearance(whenContainedInInstancesOf: [TubeNavigationController.self]).titleTextAttributes = [
            .foregroundColor: UIColor.black
        ]
    }
}
