//
//  MaiNgaBeatyMMNavigationController.swift
//  MaiNgaBeatyMembershipManagement
//
//  Created by PHAM ANH TUAN on 2/9/21.
//

import UIKit

class TubeNavigationController: UINavigationController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    override func viewDidLoad() {
          super.viewDidLoad()
        self.hero.isEnabled = true
          interactivePopGestureRecognizer?.delegate = self
          delegate = self
      }

      override func pushViewController(_ viewController: UIViewController, animated: Bool) {
          super.pushViewController(viewController, animated: animated)
          interactivePopGestureRecognizer?.isEnabled = false
      }

      func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
          interactivePopGestureRecognizer?.isEnabled = true
      }

      func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
          return viewControllers.count > 1
      }
}

extension UINavigationController {
    func hideShadowLine() {
        self.navigationBar.setValue(true, forKey: "hidesShadow")
    }
}
