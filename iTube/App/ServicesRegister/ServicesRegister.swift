//
//  ServicesRegister.swift
//  SuperSpaManagement
//
//  Created by PHAM ANH TUAN on 3/6/21.
//

import Resolver
//import GoogleSignIn
import GoogleMobileAds

extension Resolver {
    static func registerSSMServices() {
        register { ColorfulLoger() }
            .implements(Loggable.self)
        
        register { NetworkManager.default }
            .implements(Networkable.self)
        
        register { YoutubeService.default }
            .implements(YoutubeServiceType.self)
        
        register { FavouriteService.default }
            .implements(FavouriteServiceType.self)
        
        register { SuggestQueriesService.default }
            .implements(SuggestQueriesServiceType.self)
        
        register { CollectionVideosService.default }
            .implements(CollectionVideosServiceType.self)
        
        CachableCleanWorker.shared.registerWorker()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
    }
}
